//
//  comuniappUITests.swift
//  comuniappUITests
//
//  Created by Tomás Brezmes Llecha on 7/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import XCTest

class comuniappUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /*
    func logging() {   // Not working
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        app.alerts["“comuniapp” Would Like to Send You Notifications"].scrollViews.otherElements.buttons["Allow"].tap()
        app.buttons["Access"].tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["We use some quantitative analysis solutions to collect aggregate data on how users use the application. For example: \"This functionality has been used 10 times in the last 30 days\""].swipeLeft()/*[[".cells.staticTexts[\"We use some quantitative analysis solutions to collect aggregate data on how users use the application. For example: \\\"This functionality has been used 10 times in the last 30 days\\\"\"]",".swipeUp()",".swipeLeft()",".staticTexts[\"We use some quantitative analysis solutions to collect aggregate data on how users use the application. For example: \\\"This functionality has been used 10 times in the last 30 days\\\"\"]"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,0]]@END_MENU_TOKEN@*/
        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Continue"]/*[[".cells.buttons[\"Continue\"]",".buttons[\"Continue\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["If you wish to terminate this Agreement, you may delete your account at any time. You will find this option in the application. We also reserve the right if the user does not comply with any of the terms"]/*[[".cells.staticTexts[\"If you wish to terminate this Agreement, you may delete your account at any time. You will find this option in the application. We also reserve the right if the user does not comply with any of the terms\"]",".staticTexts[\"If you wish to terminate this Agreement, you may delete your account at any time. You will find this option in the application. We also reserve the right if the user does not comply with any of the terms\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        
        
    }*/
    
    func testCreateNewCommunication() {
        
        let app = XCUIApplication()
        
        //let tabBar = app.tabBars["Tab Bar"]
        //tabBar.buttons["Issues"].tap()
        //tabBar.buttons["Communications"].tap()
        app.navigationBars["Communications"].buttons["Add"].tap()
        
        let pleaseSelectCommunityForPublishingSheet = app.sheets["Please Select Community for publishing"]
        pleaseSelectCommunityForPublishingSheet/*@START_MENU_TOKEN@*/.pickerWheels["Sagrada Família 23"].tap()/*[[".pickers.pickerWheels[\"Sagrada Família 23\"]",".tap()",".press(forDuration: 0.9);",".pickerWheels[\"Sagrada Família 23\"]"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,1]]@END_MENU_TOKEN@*/
        pleaseSelectCommunityForPublishingSheet.scrollViews.otherElements.buttons["OK"].tap()
        
        
        
        let communicationTitleTextField = app.textFields["Communication title"]
        communicationTitleTextField.tap()
        communicationTitleTextField.typeText("Communication test")
        
        
        let communicationDescriptionTextView = app.textViews["communication_description"]
        communicationDescriptionTextView.tap()
        communicationDescriptionTextView.typeText("This is a test to check if communications are created correctly")
        
        app.navigationBars["Communication"].buttons["Done"].tap()
 
        //waitForExpectations(timeout: 1, handler: nil)
        app.tables.staticTexts["Communication test"].tap()
        
        
        app.navigationBars["Communication"].buttons["Edit"].tap()
        
        communicationTitleTextField.tap()
        communicationTitleTextField.typeText("Communication test 2")
        communicationDescriptionTextView.tap()
        communicationDescriptionTextView.typeText("This is a new text that I edited")
    
        
        
        // Adding a file
        app.buttons["icons8 add file"].tap()
        app.sheets["Select Document type"].scrollViews.otherElements.buttons["Photo"].tap()
        app.sheets.scrollViews.otherElements.buttons["Camera Roll"].tap()
        app/*@START_MENU_TOKEN@*/.scrollViews.otherElements.images["Photo, March 30, 2018, 9:14 PM"]/*[[".otherElements[\"Photos\"].scrollViews.otherElements",".otherElements[\"Photo, March 30, 2018, 9:14 PM, Photo, August 08, 2012, 11:55 PM, Photo, August 08, 2012, 11:29 PM, Photo, August 08, 2012, 8:52 PM, Photo, October 09, 2009, 11:09 PM, Photo, March 13, 2011, 1:17 AM\"].images[\"Photo, March 30, 2018, 9:14 PM\"]",".images[\"Photo, March 30, 2018, 9:14 PM\"]",".scrollViews.otherElements"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,0]]@END_MENU_TOKEN@*/.tap()

        app.navigationBars["Communication"].buttons["Done"].tap()
                
    }
    
}
