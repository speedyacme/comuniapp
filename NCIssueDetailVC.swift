//
//  NCIssueDetailVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 16/07/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import PhotosUI
import MobileCoreServices
import PDFKit
import JGProgressHUD

private let reuseIdentifier = "newCell"
fileprivate let itemsPerRow: CGFloat = 3

 final class NCIssueDetailVC: UIViewController, UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UITextViewDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    var isAdministrator : Bool = false
    var issue : NCIssue!
    let conversationViewController = ChatViewController ()
    @IBOutlet weak var issueTitleLabel: UITextField!
    @IBOutlet var issueSubtitleLabel: UILabel!
    @IBOutlet weak var containerVC: UIView!
    @IBOutlet weak var longDescriptionLabel: UITextView!
    @IBOutlet weak var statusLabel: UILabel! {
        didSet {
            if self.viewIfLoaded != nil { // The view is loaded
                self.updateUIbaseOnEditingStatus()
            }
        }
    }
    var temporalShortDescription : String?
    var temporalLongDescription : String?
    private var queryListener : ListenerRegistration?
    var completeDocumentsPath : String!
    override var isEditing: Bool {
        didSet {
            // Hacer todo lo que queramos en función si se ha seleccionado edit o no
            if self.viewIfLoaded != nil { // The view is loaded
                self.updateUIbaseOnEditingStatus()
            }
        }
    }
    var documents = [NCDocument]() {
        didSet
        {
            self.updateUIbaseOnEditingStatus()
            self.updateUI()
        }
    }
    var partialPath : String! { // The path of the communication item where the documents are included
        didSet
        {
            self.completeDocumentsPath = self.partialPath + "/" + constants.fireStore.issues.documentsCollectionName
        }
    }
    
        // MARK: ViewDidLoad()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.updateUI()
        self.setListener()
        NCModelAPI.sharedInstance.isUserAdmin(blockAfterAdminInfo: { isAdmin, error in
            if let error = error {
                print("[NC] Couldn't get user Admin info \(error.localizedDescription)")
            } else {
                guard self.issue.docRef != nil else {return}
                // We enable the edit option to all admins and to the creator of the issue since any user can create an issue
                if isAdmin || self.issue.creatorUserId == NCModelAPI.sharedInstance.getMyPhoneNumber() {
                    self.isAdministrator = true
                    self.isEditing = false
                }
            }
        })
        self.longDescriptionLabel.addDoneButton(title: NSLocalizedString("Done", comment: ""), target: self, selector: #selector(tapDone(sender:)))
        //  Labels
        self.statusLabel.layer.borderWidth = 1.0
        self.issueTitleLabel.delegate = self
        let communityName = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: self.issue.communityId)
        let dateString = DateFormatter.localizedString(from: issue.openingDate.dateValue(), dateStyle: .short, timeStyle: .short)
        self.issueSubtitleLabel.text = issue.creatorNickname + " - " + dateString
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.navigationItem.prompt = communityName
        // Defining Bar Buttons
        if var textAttributes = navigationController?.navigationBar.titleTextAttributes {
            textAttributes[NSAttributedString.Key.foregroundColor] = UIColor(named: "MainAppBlueColor")
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }
        // Status Label
        self.statusLabel.layer.borderWidth = 1.0
        self.issueTitleLabel.delegate = self
        self.updateUIbaseOnEditingStatus()
        if self.issue.docRef != nil {
            setConversationView()
        } else {// We are creating a new issue
            // self.createANewIssue() // We remove it, not neccessary
        }
    }
    
    /// Required for the chatviewcontroller to fit correctly
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let headerHeight: CGFloat = 10
        conversationViewController.view.frame = CGRect(x: 0, y: headerHeight, width: self.containerVC.bounds.width, height: self.containerVC.bounds.height - headerHeight)
    }
     
   
    
    func updateUIbaseOnEditingStatus () {
        self.statusLabel.layer.cornerRadius = 5
        switch issue.status {
        case 1:
            self.statusLabel.text = NSLocalizedString("PENDING", comment: "")
            self.statusLabel.textColor = UIColor.white
            self.statusLabel.backgroundColor = UIColor.red
            self.statusLabel.layer.borderColor = UIColor.red.cgColor
            //self.statusLabel.padding = UIEdgeInsets(top: 25, left: 5, bottom: 25, right: 5)
        case 2:
            self.statusLabel.text = NSLocalizedString("ON COURSE", comment: "")
            self.statusLabel.textColor = UIColor.white
            self.statusLabel.backgroundColor = UIColor.orange
            self.statusLabel.layer.borderColor = UIColor.orange.cgColor
        case 3:
            self.statusLabel.text = NSLocalizedString("SOLVED", comment: "")
            self.statusLabel.textColor = UIColor.white
            self.statusLabel.backgroundColor = UIColor.systemGreen
            self.statusLabel.layer.borderColor = UIColor.systemGreen.cgColor
        case 4:
            self.statusLabel.text = NSLocalizedString("CLOSED", comment: "")
            self.statusLabel.textColor = UIColor.white
            self.statusLabel.backgroundColor = UIColor.lightGray
            self.statusLabel.layer.borderColor = UIColor.lightGray.cgColor
        default:
            self.statusLabel.text = NSLocalizedString("PENDING", comment: "")
            self.statusLabel.textColor = UIColor.white
            self.statusLabel.backgroundColor = UIColor.red
            self.statusLabel.layer.borderColor = UIColor.red.cgColor
            //self.statusLabel.padding = UIEdgeInsets(top: 25, left: 5, bottom: 25, right: 5)
        }
        if isEditing {
            self.issueTitleLabel.isHidden = false
            statusLabel.isUserInteractionEnabled = true
            statusLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.statusLabelPushed)))
            //self.issueTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
            self.issueTitleLabel.setBottomBorder()
            self.longDescriptionLabel.layer.borderColor = UIColor.lightGray.cgColor
            self.longDescriptionLabel.layer.borderWidth = 1.0
            self.longDescriptionLabel.layer.cornerRadius = 3
            self.navigationItem.rightBarButtonItems?.removeAll()
   
            let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneButtonPushed))
            self.navigationItem.rightBarButtonItem = doneButton
            if self.issue.docRef != nil {
                let trashButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonItem.SystemItem.trash, target: self, action: #selector(self.deleteIssuePushed))
                self.navigationItem.leftBarButtonItem = trashButton
            } else {
                let cancelButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelButtonPushed))
                self.navigationItem.leftBarButtonItem = cancelButton
            }

            
            if self.isViewLoaded {
                self.longDescriptionLabel.isEditable = true
                self.issueTitleLabel.becomeFirstResponder()
            }
            
        } else { // We are not editing
            self.issueTitleLabel.removeBottomBorder()
            self.issueTitleLabel.endEditing(true)
            //self.issueTitleLabel.isHidden = true
            statusLabel.isUserInteractionEnabled = false
            self.issueTitleLabel.layer.borderColor = UIColor.clear.cgColor
            self.longDescriptionLabel.layer.borderColor = UIColor.clear.cgColor
            self.longDescriptionLabel.endEditing(true)
            // Bar button items
            self.navigationItem.leftBarButtonItems?.removeAll()
            self.navigationItem.rightBarButtonItems?.removeAll()
            if isAdministrator && self.isViewLoaded {
                //let fixedSpace = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
                let editButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(self.editButtonPushed))
                self.navigationItem.rightBarButtonItem = editButton
                //self.navigationItem.rightBarButtonItems?.append(fixedSpace)
                if self.isViewLoaded {
                    self.longDescriptionLabel.isEditable = false
                }
            }
            
            if self.documents.count > 0 {
                let fixedSpace = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
                self.navigationItem.rightBarButtonItems?.append(fixedSpace)
                let fileButton = UIBarButtonItem.init(image: UIImage(named: "icons8-documents-blue"), style: .done, target: self, action: #selector(self.showFiles))
                self.navigationItem.rightBarButtonItems?.append(fileButton)
            }
            
            if self.isViewLoaded {
                
                self.longDescriptionLabel.isEditable = false
                self.issueTitleLabel.layer.borderColor = UIColor.clear.cgColor
                self.longDescriptionLabel.layer.borderColor = UIColor.clear.cgColor;
            }
        }
    }
    
    func setConversationView() {
        if self.containerVC.subviews.count == 0 {
            conversationViewController.issue = self.issue
            /// Add the `ConversationViewController` as a child view controller
            conversationViewController.willMove(toParent: self)
            addChild(conversationViewController)
            self.containerVC.addSubview(conversationViewController.view)
            conversationViewController.didMove(toParent: self)
            // This is important in order to show input bar when in a subview
            self.becomeFirstResponder()
        }
    }
    
    func setListener() {
        if let currentIssuePath = self.issue?.docRef?.path {
            // Since the documents inside the communications are a different collection and is not loaded with the communications collection, we have to set a querylistener for this subcollection specifically
            self.queryListener = NCModelAPI.sharedInstance.getNCDocumentsListener(collectionPath: currentIssuePath + "/" + constants.fireStore.issues.documentsCollectionName, orderField: constants.fireStore.documents.uploadDate, descending: true, limit: 25, blockAfterGettingDocuments: { NCDocumentsArray, error in
            
            /*self.queryListener = NCModelAPI.sharedInstance.getDocumentsListener(collectionPath: currentIssuePath + "/" + constants.fireStore.issues.documentsCollectionName, orderField: constants.fireStore.documents.uploadDate, descending: true, limit: 25, blockAfterGettingDocuments: { documentsDictionaries, err in*/
                
                if let error = error {
                    print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                } else {
                    guard let NCDocumentsArray = NCDocumentsArray else {
                        return
                    }
                    self.documents = NCDocumentsArray.sorted(by: { $0.uploadDate.dateValue() < $1.uploadDate.dateValue() })
                    
                    print ("[NC] Listener obtained Snapshot and Mapped \(self.documents.count) documents from FireStore")
                    if self.documents.count != 0 {
                        for index in 0 ... (self.documents.count - 1) {
                            NCModelAPI.sharedInstance.getStorageFile(path: self.documents[index].fileStorageReferencePath, blockAfterGettingFile: { data, error in
                                if error != nil {
                                    print ("[NC!!] Error obtaining files: \(String(describing: error)) ")
                                } else {
                                    print ("[NC] File number \(index+1) obtained correctly")
                                    self.documents[index].fileData = data
                                }
                            })
                        }
                    }
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Now we load the documents of the communication (only the list of documents)
        self.updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isBeingDismissed {
            self.queryListener?.remove()
        }
    }
    
    func updateUI () {
        
        self.issueTitleLabel.text = self.issue.title
        self.longDescriptionLabel.text = self.issue.description
    }
    
    @objc func statusLabelPushed () {
        // We first show a action menu to choose the type of status
        let message = ""
        let alert = UIAlertController(title: NSLocalizedString("Select new status", comment: ""), message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        let startedAction = UIAlertAction(title: NSLocalizedString("ON COURSE", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.issue.status = 2 // The status is On course
            self.issue.startingDate = Timestamp.init(date: Date())
            self.issue.resolutionDate = nil
            self.issue.closingDate = nil
            self.updateUIbaseOnEditingStatus()
            // TODO: Add comment on the chat automatically indicating date and new status
        })
        
        let solvedAction = UIAlertAction(title: NSLocalizedString("SOLVED", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.issue.status = 3 // The status is Solved
            self.issue.resolutionDate = Timestamp.init(date: Date())
            self.issue.closingDate = nil
            self.updateUIbaseOnEditingStatus()
            // TODO: Add comment on the chat automatically indicating date and new status
        })
        let closedAction = UIAlertAction(title: NSLocalizedString("CLOSED", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.issue.status = 4 // The status is closed
            self.issue.closingDate = Timestamp.init(date: Date())
            self.updateUIbaseOnEditingStatus()
            // TODO: Add comment on the chat automatically indicating date and new status
        })
        switch self.issue.status {
        case 1:
            alert.addAction(startedAction)
            alert.addAction(solvedAction)
            alert.addAction(closedAction)
        case 2:
            alert.addAction(solvedAction)
            alert.addAction(closedAction)
        case 3:
            alert.addAction(closedAction)
        default: // If it is closed (=4) then whe allow to reopen
            alert.addAction(startedAction)
            alert.addAction(solvedAction)
            alert.addAction(closedAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.parent!.present(alert, animated: true, completion: nil)
    }

    
    // UITexfield delegate functions
       
   func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       if self.isEditing {
           textField.layer.borderColor = UIColor(named: "MainAppBlueColor")?.cgColor
           return true
       } else {
           textField.layer.borderColor = UIColor.white.cgColor
           return false
       }
   }
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder() // This will call texFieldDidEndEditing
       return true
   }
   
   func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
       return true
   }
   
   func textFieldDidEndEditing(_ sender: UITextField) {
       self.temporalShortDescription = sender.text
       if self.issueTitleLabel.isFirstResponder {
           self.issueTitleLabel.resignFirstResponder()
       }
   }
   
    // UITextview delegate functions
   
   func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
       if self.isEditing {
           return true
       } else {
           return false
       }
   }
 
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
       return true
    }
   
    func textViewDidEndEditing(_ textView: UITextView) {
       self.temporalLongDescription = textView.text
       if self.longDescriptionLabel.isFirstResponder {
           self.longDescriptionLabel.resignFirstResponder()
       }
    }
    
    @objc func showFiles() {
        self.performSegue(withIdentifier: "issueDetailToFileCollectionSegue", sender: self)
    }
   
    @objc func editButtonPushed() {
       //self.performSegue(withIdentifier: "segueToCommunicationEditor", sender: self)
       self.isEditing = !self.isEditing
    }
     
    @objc func tapDone(sender: Any) {
        self.view.endEditing(true)
    }
   
   @IBAction func addDocumentButtonPushed(_ sender: UIButton) {
       // We first show a action menu to choose photos or documents
       let message = ""
       let alert = UIAlertController(title: NSLocalizedString("Select Document type", comment: ""), message: message, preferredStyle: UIAlertController.Style.actionSheet)
       alert.isModalInPopover = true
       let photoAction = UIAlertAction(title: NSLocalizedString("Photo", comment: ""), style: .default, handler: {
           (alert: UIAlertAction!) -> Void in
           self.addPhoto()
       })
       alert.addAction(photoAction)
       let otherDocumentAction = UIAlertAction(title: NSLocalizedString("PDF Document", comment: ""), style: .default, handler: {
           (alert: UIAlertAction!) -> Void in
           self.pickDocument()
       })
       alert.addAction(otherDocumentAction)
       let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .destructive, handler: nil)
       alert.addAction(cancelAction)
       self.parent!.present(alert, animated: true, completion: nil)
   }
   
       
   
     func createANewIssue () {
         guard self.issue.title != "" else {
             let alert = UIAlertController(title: NSLocalizedString("Title text is empty", comment: ""), message: NSLocalizedString("In order to save an issue you must speficy a title", comment: ""), preferredStyle: UIAlertController.Style.actionSheet)
             alert.isModalInPopover = true
             let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
             alert.addAction(okAction)
             self.parent!.present(alert, animated: true, completion: nil)
             return
         }
         let collectionPathString = constants.fireStore.communities.collectionName + "/" + self.issue.communityId + "/" + constants.fireStore.issues.collectionName
         NCModelAPI.sharedInstance.addDocument(documentDictionary: self.issue.dictionary, collectionPath: collectionPathString,
             blockAfterCreatingDocument: { error, ref in
                 if error != nil {
                     NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Document creation Error", error: error!, vc: self)
                 } else {
                     print("[NC] Document added correctly in collection Path \(collectionPathString) and reference: \(String(describing: ref))")
                     if let reference = ref {
                         
                         self.issue.docRef = ref
                         self.partialPath = collectionPathString + "/" + reference.documentID
                         self.isEditing = true
                         self.setListener()
                         self.updateUIbaseOnEditingStatus()
                         self.setConversationView()
                         guard let currentIssue = self.issue,  let issueRef = currentIssue.docRef else {return}
                         
                         NCFirebaseCloudMessagingAPI.subscribeToTopic(issueRef.documentID)
                     }
                 }
         })
     }
     
     
     
   func addDocument () {
          self.pickDocument()
      }

   func pickDocument () {
   //launch document picker
       let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeJPEG)], in: .import)
       importMenu.delegate = self
       importMenu.modalPresentationStyle = .formSheet
       self.present(importMenu, animated: true, completion: nil)
   }

   func addPhoto () {
       // launch image picker
       let picker = NCPhotoPicker.init(delegate: self)
       picker.launchImagePicker()
   }
   
    
    @objc func cancelButtonPushed () {
        self.navigationController?.popViewController(animated: true)
    }
    
   
   @objc func deleteIssuePushed () {
      
       let alertController = UIAlertController(title: NSLocalizedString("Are you sure to delete this issue?", comment:""), message: NSLocalizedString("If you press delete the issue and all messages and files will be removed", comment: ""), preferredStyle: .alert)
       alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) in
           //self.dismiss(animated: true, completion: nil)
       }))
       alertController.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (action) in
           if let issuePath = self.issue.docRef?.path {
                self.deleteIssueDocuments(blockAfterDeletingAllDocuments: { error in
                    if error != nil  {
                        print ("[NC!!] Error deleting documents and files: \(String(describing: error))")
                        NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(error!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
                    } else {
                        self.deleteIssueMessages(blockAfterDeletingAllMessages: { error in
                            if error != nil  {
                                print ("[NC!!] Error deleting messages from the issue: \(String(describing: error))")
                                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(error!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
                            } else {
                                NCModelAPI.sharedInstance.deleteDocument(pathRef: issuePath, block: { error in
                                    if error != nil  {
                                        print ("[NC!!] Error deleting issue \(String(describing: self.issue.docRef)): \(String(describing: error?.localizedDescription))")
                                        NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(error!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
                                    } else {
                                        print ("[NC] Issue \(String(describing: self.issue.docRef)) deleted correctly")
                                        guard let issueId = self.issue.docRef?.documentID else {return}
                                        NCFirebaseCloudMessagingAPI.unsubscribeFromTopic(issueId)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                })
                            }
                        })
                    }
                
                })
           }
       }))
                
               
       if (self.isViewLoaded) && (self.view.window != nil) {
           DispatchQueue.main.async {
               self.present (alertController, animated: true, completion:nil)
           }
       }
   }
    
    func deleteIssueDocuments (blockAfterDeletingAllDocuments: @escaping (Error?) -> ()) {
        
        if self.documents.count > 0 {
            for doc in self.documents {
                if let docId = doc.id, let lastDocId = self.documents.last?.id {
                    
                    NCModelAPI.sharedInstance.deleteFirestoreDocumentWithStorageFile(docRef: Firestore.firestore().collection(completeDocumentsPath).document(docId), fileStorageReferencePath: doc.fileStorageReferencePath, block: { error in
                        if error != nil {
                            print ("[NC!!] Error deleting document \(doc.docName) : \(String(describing: error?.localizedDescription))")
                            blockAfterDeletingAllDocuments (error)
                        } else {
                            print ("[NC] Document \(String(describing: docId)) deleted correctly")
                            if docId == lastDocId { // This is the last doc of the array, so now we erase messages
                                blockAfterDeletingAllDocuments (nil)
                            }
                        }
                        
                    })
                } else {
                    print ("[NC!!] Error deleting documents, doc.docRef and/or self.documents.last?.docRef are nil")
                    blockAfterDeletingAllDocuments (NSError(domain:"nilValue", code:1992, userInfo:nil))
                }
            }
        } else {
            blockAfterDeletingAllDocuments(nil)
        }
        
    }
                    
    func deleteIssueMessages (blockAfterDeletingAllMessages: @escaping (Error?) -> ()) {
        
        if self.conversationViewController.messages.count > 0 {
            for message in self.conversationViewController.messages {
                if let messagePath = message.docRef?.path, let lastMessageDocRef = self.conversationViewController.messages.last?.docRef {
                    NCModelAPI.sharedInstance.deleteDocument(pathRef: messagePath, block: { error in
                        if error != nil {
                            print ("[NC!!] Error deleting message \(String(describing: message.docRef)) : \(String(describing: error?.localizedDescription))")
                            blockAfterDeletingAllMessages (error)
                        }
                        else {
                            print ("[NC] Message \(String(describing: message.docRef)) deleted correctly")
                            if message.docRef?.documentID == lastMessageDocRef.documentID { // This is the last doc of the array, so now we erase the issue
                                blockAfterDeletingAllMessages(nil)
                            }
                        }
                    })
                } else {
                    print ("[NC!!] Error deleting messages, message.docRef?.path and/or self.conversationViewController.messages.last?.docRef are nil")
                    blockAfterDeletingAllMessages (NSError(domain:"nilValue", code:1992, userInfo:nil))
                }
            }
        } else {
            blockAfterDeletingAllMessages(nil)
        }
    }
   
   
   @objc func doneButtonPushed () {
       // Checking if there is title and description
       guard self.issueTitleLabel.text != "", self.issueTitleLabel.text != nil, self.longDescriptionLabel.text != "",  self.longDescriptionLabel.text !=  nil else {
           NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Title and/or description empty", comment: ""), message: NSLocalizedString("Title and description texts are required", comment: ""), cancelText: NSLocalizedString("Ok", comment: ""), continueText: nil)
           return
       }
       self.checkAndSave()
   }
   
   
   
   func checkAndSave () {
         
       
       if let newIssueTitle = self.issueTitleLabel.text{
        self.issue.title = newIssueTitle
        self.title = self.issue.title
       }
       if let newLongDescription = self.longDescriptionLabel.text {
        self.issue.description = newLongDescription
       }
       self.issueEdited(issue: self.issue)
       self.isEditing = false
       self.updateUI()
   }
   
    func issueEdited (issue: NCIssue) {
        // TODO: Guardar comunicación actual
        self.issue = issue
        self.updateUI()
        let hud = JGProgressHUD()
        hud.show(in: self.view, animated: true)
        if let documentReference = issue.docRef { // If we already have an issue and we are editing it
            NCModelAPI.sharedInstance.updateDocument(documentDictionary: self.issue.dictionary, docRef: documentReference, blockAfterUpdating: { error in
                if let error = error{
                    print ("[NC!!] Error updating document in the Cloud: \(error.localizedDescription)")
                    hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                    hud.textLabel.text = NSLocalizedString("Error updating issue", comment: "")
                    hud.dismiss(afterDelay: 3, animated: true)
                    NCGUI.showErrorAlertOnCurrentVC(errorTitle: "We couldn't update the document in the Cloud. Try again later", error: error, vc: self)
                } else {
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                    hud.textLabel.text = NSLocalizedString("Issue updated", comment: "")
                    hud.dismiss(afterDelay: 3, animated: true)
                    //NCFirebaseCloudMessagingAPI.sendMessage(title: issue.title, message: issue.description, topic: issue.communityId, type: .community_new_issue, firebaseReferencePath: documentReference.path, senderPhoneNumber: issue.creatorUserId ,completion: nil)
                    self.conversationViewController.becomeFirstResponder()
                }
            })
        } else { // If we are creating a new issue
            let collectionPathStringPart1 = constants.fireStore.communities.collectionName + "/"
            let collectionPathStringPart2 = self.issue.communityId + "/" + constants.fireStore.issues.collectionName
            let collectionPathString = collectionPathStringPart1 + collectionPathStringPart2
           
            NCModelAPI.sharedInstance.addDocument(documentDictionary: self.issue.dictionary, collectionPath: collectionPathString,
               blockAfterCreatingDocument: { error, ref in
                   if error != nil {
                       hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                       hud.textLabel.text = NSLocalizedString("Error creating issue", comment: "")
                       hud.dismiss(afterDelay: 3, animated: true)
                       NCGUI.showErrorAlertOnCurrentVC(errorTitle: "File Storage Error", error: error!, vc: self)
                   } else {
                       print("[NC] Document added correctly in collection Path \(collectionPathString)")
                       hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                       hud.textLabel.text = NSLocalizedString("Issue created", comment: "")
                       hud.dismiss(afterDelay: 3, animated: true)
                       self.issue.docRef = ref
                       self.viewDidLoad()
                       guard let currentIssue = self.issue,  let issueRef = currentIssue.docRef else {return}
                       //NCFirebaseCloudMessagingAPI.sendMessage(title: NSLocalizedString("New Issue", comment: ""), message: currentIssue.title, topic: self.issue.communityId, type: .issue_update, firebaseReferencePath: issueRef.path, senderPhoneNumber: currentIssue.creatorUserId ,completion: nil)
                       NCFirebaseCloudMessagingAPI.subscribeToTopic(issueRef.documentID)
                       //self.dismiss(animated: true, completion: nil)
                       self.conversationViewController.becomeFirstResponder()
                   }
            })
       }
    }

   
    
 // MARK: UICollectionViewDataSource
   
   
   
   func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
   }
   
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return documents.count
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
       // Configure the cell
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NCDocumentCollectionViewCell
       
       let document = self.documents[indexPath.row]
       if let data = document.fileData {
           switch document.mimeType {
               case "image/jpeg":
                   cell.cellImageView.image = UIImage (data: data)
                   ?? UIImage (named: "icons8-image_file-1")
               case "application/pdf":
                   cell.cellImageView.image = UIImage (named: "icons8-pdf-1")
               default:
                   cell.cellImageView.image = UIImage (named: "icons8-document")
           }
       } else {
           if cell.isSelected {
               cell.cellImageView.image = UIImage (named: "icons8-image_file_filled")
           } else {
               cell.cellImageView.image = UIImage (named: "icons8-image_file")
           }
       }
   
       return cell
   }
    
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       guard let cell = collectionView.cellForItem(at: indexPath) as? NCDocumentCollectionViewCell
           else {return}
       if self.isEditing {
           let cell = collectionView.cellForItem(at: indexPath)
           cell?.layer.borderWidth = 2.0
           cell?.layer.borderColor = UIColor.systemBlue.cgColor
           
       } else {
           switch self.documents[indexPath.row].mimeType {
           case "image/jpeg":
               self.performSegue(withIdentifier: "issueDetailToPhoto", sender: cell)
           case "application/pdf":
               self.performSegue(withIdentifier: "issueDetailToPdfViewer", sender: cell)
           default:
               return
           }
               
           //deleteButton.isEnabled = false
       }
   }
   
   
   func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
       
       let cell = collectionView.cellForItem(at: indexPath)
       cell?.layer.borderWidth = 2.0
       cell?.layer.borderColor = UIColor.clear.cgColor
   }
   
   func imagePickerController( _ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
   {
       // Local variable inserted by Swift 4.2 migrator.
       var imageFileName : String? = nil
       let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
       
       guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
           else {
               print("[NC!!] Problem picking edited image")
               return
       }
       
       guard let imageResizedAndCompressed = image.resizeAndCompressImageToJpeg(image: image, quality: nil, reSizeTo: nil)
           else { return }
       
       if let url = info[UIImagePickerController.InfoKey.imageURL.rawValue] as? URL {
           imageFileName = url.lastPathComponent
       }
       let hud = JGProgressHUD()
       hud.show(in: self.view, animated: true)
       
       NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: imageResizedAndCompressed, metadataContentType: "image/jpeg", documentType: Int.init(5), fileName: imageFileName , collectionRef: Firestore.firestore().collection(completeDocumentsPath), communityId: self.issue.communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
           if error != nil {
               hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
               hud.dismiss(afterDelay: 0.5, animated: true)
               NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
           } else {
               print ("[NC] Document created correctly!")
               hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
               hud.dismiss(afterDelay: 0.5, animated: true)
           }
       })
       
       dismiss(animated: true, completion: nil)
   }
   
   func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       dismiss(animated: true, completion: nil)
       
   }
   

  
   
   // MARK: - Document picker functions
   
   public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
       guard let myURL = urls.first else {
           return
       }
       print("Document picker import URL : \(myURL)")
       do {
        // inUrl is the document's URL
           var documentData = try Data(contentsOf: myURL) // Getting file data here
           var documentMimeType = myURL.mimeType()
           let fileName = myURL.lastPathComponent
           // If it is an image, we compress it
           let fileExtension = myURL.pathExtension
           let imageFileExtensions = ["jpg", "png", "gif", "jpeg", "bmp"]
           if imageFileExtensions.contains(fileExtension) {
               guard let fileImage = UIImage(data: documentData)
                   else {
                       print ("[NC!!]: Error obtaining UIImage from file ")
                       return
               }
               if let compressedAndResizedImage = fileImage.resizeAndCompressImageToJpeg(image: fileImage, quality: nil, reSizeTo: nil) {
                   documentData = compressedAndResizedImage
                   documentMimeType = "image/jpeg"
               }
           }
           let hud = JGProgressHUD()
           hud.show(in: self.view, animated: true)
           NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: documentData, metadataContentType: documentMimeType, documentType: Int.init(5), fileName: fileName , collectionRef: Firestore.firestore().collection(completeDocumentsPath), communityId: self.issue.communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
                       if error != nil {
                           hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                           hud.dismiss(afterDelay: 0.5, animated: true)
                           NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
                       } else {
                           print ("[NC] Document created correctly!")
                           hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                           hud.dismiss(afterDelay: 0.5, animated: true)
                       }
               })
       } catch {
           print("[NC!!] Document data loading error")
       }
   }


   public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
           documentPicker.delegate = self
           present(documentPicker, animated: true, completion: nil)
   }


   func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
               print("[NC] Document picker view was cancelled by the user")
               dismiss(animated: true, completion: nil)
   }
    

    
    // MARK: - Navigation

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let identifier = segue.identifier
        {
            switch identifier
            {
 
            case "toChatViewController":
                let destinationVc = segue.destination as! ChatViewController
                destinationVc.issue = issue
            case "issueDetailToFileCollectionSegue":
                let destinationVc = segue.destination as! NCFileCollectionReadOnlyVC
                destinationVc.mainTitle = self.issue.title
                destinationVc.topTitle = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: self.issue.communityId)
                destinationVc.documents = self.documents
                
            case "issueDetailToPhoto":
                let destinationVC = segue.destination as! NCPhotoVC
                if let senderPhoto = sender as? UIImage {
                    destinationVC.imageToPresent = senderPhoto
                } else {
                    print ("[NC!!] Error obtaining image data before segue to detailPhotoVC")
                    return
                }
            
            
            case "issueDetailToPdfViewer":
                print("going to pdfviewer")
                
                let destinationVC = segue.destination as!NCPDFVC
                if let path = sender as? String {
                    NCModelAPI.sharedInstance.getStorageFile(path: path, blockAfterGettingFile: { data, err in
                        if let error = err {
                            print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                        } else {
                            if data != nil {
                                print ("[NC] Document obtained from firestorage")
                                NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key:
                                    path, data: data)
                                guard let pdf = PDFDocument.init(data: data!)
                                else {
                                    print ("[NC] Document doesn't conform to PDF format. Exiting")
                                    return
                                }
                                destinationVC.pdfDocument = pdf
                            } else {
                                print ("[NC!!] Error obtaining pdf data")
                                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting pdf from data", vc: self)
                            }
                        }
                    })
                } else {
                    print ("[NC!!] Error obtaining PDF data before segue to NCPDFVC")
                    return
                }
                
            default: return
            }
        }
    }


}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
