//
//  comuniappTests.swift
//  comuniappTests
//
//  Created by Tomás Brezmes Llecha on 7/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import XCTest
@testable import comuniapp

class comuniappTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let error = NCModelAPI.sharedInstance.updateMyUserName(name: "Tomasito")
        if error == nil {
            XCTAssertTrue(true)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMyName() {
        // This is an example of a functional test case.
        var name = NCModelAPI.sharedInstance.getMyUserName() // should be tomasin for +34999999999
        print("[NCtest] Testing getMyUserName function")
        print("[NCtest] The name is \(String(describing: name))")
        XCTAssertTrue(name == "Tomasito")
        print("[NCtest] testing updateMyUserName function")
        let error = NCModelAPI.sharedInstance.updateMyUserName(name: "Tomasin")
        if error == nil {
            name = NCModelAPI.sharedInstance.getMyUserName() // should be tomasin for +34999999999 
            print("[NCtest] The name is \(String(describing: name))")
            XCTAssertTrue(name == "Tomasin")
        }
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testImageResizing() {
        // This is an example of a functional test case.
        guard let testImage = UIImage (named: "testjpegimage.jpeg")
            else {return}
        let testingSize = CGSize(width: 800, height: 600)
        guard let newImage = testImage.resizeAndCompressImageToJpeg(image: testImage, quality: nil, reSizeTo: nil)
            else { return }
        guard let newImagePNG = UIImage.init(data: newImage)
            else { return }
            print("new image size : \(newImagePNG.size.width) x \(newImagePNG.size.height) ")
            XCTAssertTrue(newImagePNG.size == testingSize)
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testImageResizingWithCompression() {
        // This is an example of a functional test case.
        guard let testImage = UIImage (named: "testjpegimage.jpeg")
            else {return}
        let testingSize = CGSize(width: 800, height: 600)
        guard let newImageData = testImage.resizeAndCompressImageToJpeg(image: testImage, quality: nil, reSizeTo: nil)
            else {return}
        guard let newImage = UIImage (data: newImageData)
            else {return}
        print("new image size : \(newImage.size.width) x \(newImage.size.height) ")
        XCTAssertTrue(newImage.size == testingSize)
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
  /*
    func testGettingListenerOnCommunications () {
        let userCommunityId = "S9fA9lbXvj4mAI78ya6E"
        let collectionPath =  constants.fireStore.communities.collectionName+"/"+userCommunityId+"/"+constants.fireStore.communications.collectionName
        print ("[NC] Docpath = \(collectionPath)")
        let listener = NCModelAPI.sharedInstance.getDocumentsListener(collectionPath: collectionPath, orderField: constants.fireStore.communications.FieldNameDate, descending: false, limit: 25, blockAfterGettingDocuments: { dictionary, err in
            if let error = err {
                print ("[NCTest] error getting documents with listener: \(error.localizedDescription)")
            } else {
                guard let dict = dictionary
                    else { return}
                print ("[NCTest] dictionary get from snapshot: \(dict.description)")
            }
        })
        if listener != nil {
            print ("[NCTest] listener: \(listener!.description)")
        }
    }
 */
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
