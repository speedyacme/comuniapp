//
//  NCIssue.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 28/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import Foundation

import Foundation
import FirebaseFirestore

struct NCIssue {
    
    var docRef: DocumentReference?
    var communityId: String
    var title: String
    var description: String
    var creatorUserId: String
    var creatorNickname: String
    var openingDate: Timestamp
    var startingDate: Timestamp?
    var resolutionDate: Timestamp?
    var closingDate: Timestamp?
    var documents = [NCDocument]()
    var type : Int
    var status: Int
    var dictionary:[String:Any] {
        return [
            constants.fireStore.issues.title: title,
            constants.fireStore.issues.communityId: communityId,
            constants.fireStore.issues.description : description,
            constants.fireStore.issues.creatorUserId : creatorUserId,
            constants.fireStore.issues.creatorNickname: creatorNickname,
            constants.fireStore.issues.openingDate: openingDate,
            constants.fireStore.issues.startingDate: startingDate as Timestamp? as Any,
            constants.fireStore.issues.resolutionDate: resolutionDate as Timestamp? as Any,
            constants.fireStore.issues.closingDate: closingDate as Timestamp? as Any,
            constants.fireStore.issues.status: status,
            constants.fireStore.issues.type: type,
        ]
    }
}

extension NCIssue : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        
        // First we check the mandatory fields are not empty, if any of the mandatory fields in null we don't create the object
        guard let title = dictionary[constants.fireStore.issues.title] as? String,
            let communityId = dictionary[constants.fireStore.issues.communityId] as? String,
            let description = dictionary[constants.fireStore.issues.description] as? String,
            let creatorUserId = dictionary [constants.fireStore.issues.creatorUserId] as? String,
            let creatorNickname = dictionary [constants.fireStore.issues.creatorNickname] as? String,
            let openingDate = dictionary [constants.fireStore.issues.openingDate] as? Timestamp,
            let type = dictionary [constants.fireStore.issues.type] as? Int,
            let status = dictionary [constants.fireStore.issues.status] as? Int
            else {
                print ("[NC!!] Trying to create NCIssue from Firebase dictionary failed due to null values: \(dictionary)")
            return nil
            }
        
        // Now the non-mandatory fields that can be nil
        let docRef = dictionary [constants.fireStore.generic.documentReference] as? DocumentReference // It can be nil when you create the communication
        let documents = [NCDocument]()
        let startingDate = dictionary [constants.fireStore.issues.startingDate] as? Timestamp
        let resolutionDate = dictionary [constants.fireStore.issues.resolutionDate] as? Timestamp
        let closingDate = dictionary [constants.fireStore.issues.closingDate] as? Timestamp
        
        
        // Here we initiate the object
        
        
        self.init(docRef: docRef, communityId: communityId, title: title, description: description, creatorUserId: creatorUserId, creatorNickname: creatorNickname, openingDate: openingDate, startingDate: startingDate, resolutionDate: resolutionDate, closingDate: closingDate, documents: documents, type: type, status: status)
    }
}


