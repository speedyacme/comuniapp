//
//  NCInfo.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 12/09/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


// [START codable_struct]
public struct NCInfo: Codable, Identifiable {
    
    @DocumentID public var id: String?
    var description : String
    var phone : String
    var type : Int          // =1 Emergency, =2 Information
    var communityId: String

}
// [END codable_struct]
