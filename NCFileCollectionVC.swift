//
//  NCFileCollectionVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 01/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import PhotosUI
import MobileCoreServices
import QuickLook
import PDFKit


class NCFileCollectionVC: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegateFlowLayout {
   
    private var queryListeners = [ListenerRegistration]()
    private let reuseIdentifier = "newSectionCell"
    private let spacing:CGFloat = 16.0  // Inset spacing
    private let itemsPerRow: CGFloat = 3
    var isUsrAdmin = false
    let pullToRefreshControl = UIRefreshControl()
    var pickerViewRolled = false
    var filterButton = UIBarButtonItem ()
    var addButton = UIBarButtonItem ()
    var editButton = UIBarButtonItem ()
    var doneButton = UIBarButtonItem ()
    var trashButton = UIBarButtonItem ()
    var db:Firestore! = Firestore.firestore()
    var userCommunities = [NCCommunity]()
    var userCommunitiesIds = [String]()
    var userCommunitiesIdsFiltered = [String]() {
        didSet { // We change the name of the navigation top bar based on community filtered
            if userCommunitiesIdsFiltered.count == 1{
                if let userCommunitySelected =  self.userCommunities.first(where: { $0.communityId == self.userCommunitiesIdsFiltered.first!})  {
                    self.navigationItem.prompt = userCommunitySelected.communityName
                    let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
                }
            } else {
                self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
                let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
            }
        }
    }
    //@IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    var newDocumentCommunityId : String?
    var newDocumentCommunityName : String?
    var documentTypes = [NCDocumentType]()
    var documentTypeSelected : NCDocumentType? = nil
    //var documentsByType = [[NCDocument]]()
    var documents = [NCDocument]() {
        didSet
        {
            /*var temporalDocuments = [[NCDocument]]()
            for documentType in documentTypes {
                let typeArray = documents.filter({ $0.documentType == documentType.typeNumber })
                if typeArray.count != 0 {
                    temporalDocuments.append(typeArray)
                }
            }*/
            //self.documentsByType = temporalDocuments
            //print ("[NC] Documents got set: \(documents)")
            print ("[NC] Documents got set: \(self.documents.map{[String($0.type),$0.communityId]})")
            self.sections = Array(Set(self.documents.map{[String($0.type),$0.communityId]}))
            print ("[NC] Sections array was calculated: \(sections)")
            self.updateUI()
        }
    }
    var sections = [[String]]()  // Array with one row for each section and two strings, first the fileType and second the communityId
    
   override var isEditing: Bool {
        didSet {
            // we need to update the UI
            if self.isViewLoaded{ // The view is loaded
                self.updateUI()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.allowsMultipleSelection = true
        self.userCommunities = NCModelAPI.sharedInstance.getUserCommunitiesFromLocalMemory()
        self.userCommunitiesIds = self.userCommunities.compactMap ({$0.communityId})
        self.userCommunitiesIdsFiltered = userCommunitiesIds
        // UI Labeling and buttons
        self.title = NSLocalizedString(NSLocalizedString("Documents", comment: ""), comment: "")
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
        // Add Refresh Control to Table View
        pullToRefreshControl.tintColor = UIColor(named: "MainAppBlueColor")
        pullToRefreshControl.attributedTitle = NSAttributedString (string:NSLocalizedString("Loading documents...", comment: ""))
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = pullToRefreshControl
        } else {
            self.collectionView.addSubview(pullToRefreshControl)
        }
        // Configure Refresh Control
        pullToRefreshControl.addTarget(self, action: #selector(updateModel), for: .valueChanged)
        
        // If the user is an administrator we allow them to add documents
        NCModelAPI.sharedInstance.isUserAdmin(blockAfterAdminInfo: { isAdmin, error in
            if let error = error {
               print("[NC] Couldn't get user Admin info \(error.localizedDescription)")
            } else {
                if isAdmin {
                    self.isUsrAdmin = true
                    let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPushed))
                    self.navigationItem.leftBarButtonItem = addButton
                    let editButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(self.editButtonPushed))
                    self.navigationItem.rightBarButtonItem = editButton
                }
            }
        })
        NCModelAPI.sharedInstance.getDocumentTypesList(blockAfterGettingDocuments: { documentTypelist, err in
            self.documentTypes = documentTypelist.filter({$0.typeNumber != 5 && $0.typeNumber != 6 && $0.typeNumber != 0}) // We filter photo, issue and communication to be aligned with Android
        })
        
        // Defining Bar Buttons
        self.filterButton = UIBarButtonItem.init(image: UIImage(named: "icons8-filter"), landscapeImagePhone: UIImage(named: "icons8-filter"), style: .plain, target: self, action: #selector(self.filterButtonPushed))
        self.doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneButtonPushed))
        self.trashButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonItem.SystemItem.trash, target: self, action: #selector(self.deleteDocumentPushed))
        self.editButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(self.editButtonPushed))
        self.addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPushed))
        self.updateModel()
        self.updateUI()
        self.removeSelectionRectangleToAllSelectedItems()
    }
    
    func stopObserving () {
        for listener in self.queryListeners {
            listener.remove()
        }
        self.queryListeners.removeAll() // We reset the queryListeners
    }
    
    
    
    func updateUI () {
        self.navigationItem.leftBarButtonItems?.removeAll()
        self.navigationItem.rightBarButtonItems?.removeAll()
        if (self.userCommunitiesIds.count != self.userCommunitiesIdsFiltered.count) && self.userCommunitiesIds.count > 1 {
            self.filterButton.image = UIImage (named: "icons8-filter_filled")
        } else {
            self.filterButton.image = UIImage (named: "icons8-filter")
        }
        if self.userCommunitiesIds.count > 1 && !self.isEditing{ // If we have only one community, we don't need to show the filter button
            self.navigationItem.leftBarButtonItem = self.filterButton
        }
        
        self.collectionView.reloadData()
        
        switch isUsrAdmin {
        case true:
            if self.isEditing {
                self.navigationItem.rightBarButtonItem = self.doneButton
                self.navigationItem.leftBarButtonItems?.append(self.trashButton)
            } else {
                self.navigationItem.rightBarButtonItem = editButton
                self.navigationItem.rightBarButtonItems?.append(addButton)
            }
        default:
            return
        }
    }
    
    func removeSelectionRectangleToAllSelectedItems () {
        if let selectedIndexPaths = self.collectionView.indexPathsForSelectedItems {
            for indexPath in selectedIndexPaths {
                self.collectionView.deselectItem(at: indexPath, animated: true)
                let cell = self.collectionView.cellForItem(at: indexPath)
                //cell?.layer.borderWidth = 2.0
                cell?.layer.borderColor = UIColor.clear.cgColor
            }
        }
        self.collectionView.reloadData()
    }
    
    @objc func updateModel () {
        if self.userCommunitiesIdsFiltered.count != 0 {
            // Find communications in each community
            self.stopObserving()
            let newListener = NCModelAPI.sharedInstance.getCollectionGroupNCDocumentsListener(collectionGroupId: constants.fireStore.documents.documentsCollectionName, communityIds: self.userCommunitiesIdsFiltered, orderField: constants.fireStore.documents.uploadDate, descending: false, limit: 25, blockAfterGettingDocuments: { documentArray, err in
                
                if let err = err {
                    print("[NC!!] Error getting communications: \(err.localizedDescription)")
                    self.pullToRefreshControl.endRefreshing()
                    //self.activityIndicatorView.stopAnimating()
                    NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error downloading documents", comment: ""), error: err, vc: self)
                } else {
                    self.pullToRefreshControl.endRefreshing()
                    guard let documentArray = documentArray
                        else { return }
                    self.documents = documentArray.sorted(by: { $0.uploadDate.dateValue() > $1.uploadDate.dateValue() })
                }
            })
            if newListener != nil {
                self.queryListeners.append(newListener!)
            }
     
        } else {
                // User doesn't have any community set -> he must talk to administrator
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities found", comment: ""), message: NSLocalizedString("Contact the community administrator to include you in the community", comment:""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
        }
    }
    
    @objc func filterButtonPushed () {
        if self.userCommunitiesIds.count > 1 { // show picker
            self.pickerViewRolled = false
            let message = "\n\n\n\n\n\n"
            let alert = UIAlertController(title: "Please Select Community for filter", message: message, preferredStyle: UIAlertController.Style.actionSheet)
            alert.isModalInPopover = true
            let screenWidth = self.view.frame.size.width
            let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 30, width: screenWidth - 20, height: 150)) // CGRectMake(left, top, width, height) - left and top are like margins
            pickerFrame.tag = 0 // this will tell us if that this specific picker is to filter one specific community or select "all communities" option
            //set the pickers datasource and delegate
            pickerFrame.delegate = self
            
            
            //Add the picker to the alert controller
            alert.view.addSubview(pickerFrame)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if !self.pickerViewRolled {
                    self.userCommunitiesIdsFiltered = [self.userCommunitiesIds[0]]
                }
                self.updateModel()
            })
            alert.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(cancelAction)
            self.parent!.present(alert, animated: true, completion: nil)
            if self.userCommunitiesIdsFiltered.count == 1 { // We have selected already a community
                if let indexCommunity = self.userCommunitiesIds.firstIndex(of: self.userCommunitiesIdsFiltered[0]) {
                pickerFrame.selectRow(indexCommunity, inComponent: 0, animated: true)
                }
            } else {
                pickerFrame.selectRow(self.userCommunitiesIds.count, inComponent: 0, animated: true)
            }
        } else {
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Only one community assigned", comment: ""), message: NSLocalizedString("To filter communities you must be registered at least in two communities", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            return
        }
    }
    
    /*
    
    func runActivityIndicator () {
        // See if it is animating.
        if activityIndicatorView.isAnimating {
            // If currently animating, stop it.
            activityIndicatorView.stopAnimating()
        }
        else {
            // Begin.
            activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivityIndicator () {
        activityIndicatorView.stopAnimating()
    } */
    
    @objc func editButtonPushed () {
        self.isEditing = true
    }
    
    
    @objc func addButtonPushed () {
        self.launchDocumentTypeListforNewDocument()
    }
    
    @objc func doneButtonPushed () {
        self.removeSelectionRectangleToAllSelectedItems()
        self.isEditing = false
        
    }
    
    @objc func deleteDocumentPushed () {
        // Since here we can only delete photo documents, we call to delete photo
       if let indexes = self.collectionView.indexPathsForSelectedItems {
           //let items = selectedCells.map { $0.item }.sorted().reversed() // Esto lo usaban no sé para qué...
           for index in indexes {
            self.deleteDocument(doc: self.documents.filter{String($0.type) == self.sections[index.section][0] && $0.communityId == self.sections[index.section][1]}[index.row])
           }
       }
        self.removeSelectionRectangleToAllSelectedItems()
        self.updateModel()
        
    }
    
    // MARK: Pick Document functions


    func pickDocument () {
        // first we launch the community picker
        self.launchCommunityPickerforNewDocument(block: { comunityId in
            guard (comunityId != nil) else { return }
            self.newDocumentCommunityId = comunityId
            
            //launch document picker
             let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeJPEG)], in: .import)
             importMenu.delegate = self
             importMenu.modalPresentationStyle = .formSheet
             self.present(importMenu, animated: true, completion: nil)
        })
    }

    func addPhoto () {
    
        // first we launch the community picker
        self.launchCommunityPickerforNewDocument( block: { comunityId  in
            guard (comunityId != nil) else { return }
            self.newDocumentCommunityId = comunityId
            // launch image picker
            let picker = NCPhotoPicker.init(delegate: self)
            picker.launchImagePicker()
        })
     }
        
     func deleteDocument (doc: NCDocument) {
         // TODO: Implement delete photo
         guard let docId = doc.id else {
             print ("[NC!!] Deletion error, document has no reference")
            NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "The document couldn't be deleted", vc: self)
             return
         }
         let completeDocumentPath = constants.fireStore.communities.collectionName + "/" + doc.communityId + "/" + constants.fireStore.documents.documentsCollectionName
         NCModelAPI.sharedInstance.deleteFirestoreDocumentWithStorageFile(docRef: Firestore.firestore().collection(completeDocumentPath).document(docId), fileStorageReferencePath: doc.fileStorageReferencePath, block: { err in
             
             if err != nil {
                 NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(err!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
             } else {
                 print ("[NC] Document deleted correctly")
                 self.updateUI()
             }
         })
     }
    
    
 
    
    // MARK: - Document picker functions
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("Document picker import URL : \(myURL)")
        do {
         // inUrl is the document's URL
            var documentData = try Data(contentsOf: myURL) // Getting file data here
            var documentMimeType = myURL.mimeType()
            let fileName = myURL.lastPathComponent
            let fileExtension = myURL.pathExtension
            let imageFileExtensions = ["jpg", "png", "gif", "jpeg", "bmp"]
            // If it is an image, we compress it, if not, we just leave the data
            if imageFileExtensions.contains(fileExtension) {
                guard let fileImage = UIImage(data: documentData)
                    else {
                        print ("[NC!!]: Error obtaining UIImage from file ")
                        return
                }
                if let compressedAndResizedImage = fileImage.resizeAndCompressImageToJpeg(image: fileImage, quality: nil, reSizeTo: nil) {
                    documentData = compressedAndResizedImage
                    documentMimeType = "image/jpeg"
                }
            }
            guard self.newDocumentCommunityId != nil else {
                print ("[NC!!: Error, trying to create a document but the newDocumentCommunityID is nil")
                return
            }
            let completeDocumentPath = constants.fireStore.communities.collectionName + "/" + newDocumentCommunityId! + "/" + constants.fireStore.documents.documentsCollectionName
            guard self.newDocumentCommunityId != nil else {
                print ("[NC!!] Error new communityId for creation of the document is nil")
                return
            }
            NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: documentData, metadataContentType: documentMimeType, documentType: self.documentTypeSelected?.typeNumber ?? Int.init(4), fileName: fileName , collectionRef: Firestore.firestore().collection(completeDocumentPath), communityId: self.newDocumentCommunityId!, blockAfterFileAndDocumentCreated: { docRef, metadata, error in
                    if error != nil {
                        NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
                    } else {
                        print ("[NC] Document created correctly!")
                        guard docRef != nil else {
                            print ("[NC!!] Error: Document reference is nil")
                            return
                        }
                        NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key: docRef!.path, data: documentData)
                    }
            })
        } catch {
            print("[NC!!] Document data loading error")
        }
    }


    func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("[NC] Document picker view was cancelled by the user")
                dismiss(animated: true, completion: nil)
    }
    

    
       // MARK: Image Picker delegate functions
    
    
    func imagePickerController( _ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        // Local variable inserted by Swift 4.2 migrator.
        var imageFileName : String? = nil
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
            else {
                print("[NC!!] Problem picking edited image")
                return
        }
        
        guard let imageResizedAndCompressed = image.resizeAndCompressImageToJpeg(image: image, quality: nil, reSizeTo: nil)
            else { return }
        
        if let url = info[UIImagePickerController.InfoKey.imageURL.rawValue] as? URL {
            imageFileName = url.lastPathComponent
        }
        
        guard let communityId = self.newDocumentCommunityId
            else {
            print("[NC!!] There is no commnity selected for the new document")
            return
        }
        
        NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: imageResizedAndCompressed, metadataContentType: "image/jpeg", documentType: Int.init(5), fileName: imageFileName , collectionRef: Firestore.firestore().collection(constants.fireStore.communities.collectionName+"/"+communityId+"/"+constants.fireStore.documents.documentsCollectionName), communityId: self.newDocumentCommunityId!, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
            if error != nil {
                NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
            } else {
                print ("[NC] Document created correctly!")
            }
        })
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: Community Picker View Delegate functions
    
    
    func launchDocumentTypeListforNewDocument () {
        
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "Please Select Document Type", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        let screenWidth = self.view.frame.size.width
        let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: screenWidth - 20, height: 140)) // CGRectMake(left, top, width, height) - left and top are like margins
        pickerFrame.tag = 2 // this will tell us if that this specific picker is to select one community to make a new communication
        //set the pickers datasource and delegate
        pickerFrame.delegate = self
        pickerFrame.dataSource = self
        
        //Add the picker to the alert controller
        alert.view.addSubview(pickerFrame)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.documentTypeSelected = self.documentTypes[pickerFrame.selectedRow(inComponent: 0)]
            if self.documentTypeSelected?.typeNumber == 5 { // It's a photo
                self.addPhoto()
            } else {
                self.pickDocument()
            }
        })
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.parent!.present(alert, animated: true, completion: nil)
        
        self.newDocumentCommunityId = nil  // We reset the variable
        print ("[NC--]******* Selecting community to create a new document ********")
        
        
            
            
               
    }
    
    
    
    // MARK: Community picker methods
    
    func launchCommunityPickerforNewDocument (block: @escaping ( String?) -> ()) {
        
        self.newDocumentCommunityId = nil  // We reset the variable
        print ("[NC--]******* Selecting community to create a new document ********")
        
        switch userCommunities.count {
        
            case let numberOfUserCommunities where numberOfUserCommunities == 1: // we don't need to show community picker
                print ("[NC--] We only have one community so we don't show community picker ")
                newDocumentCommunityId = userCommunities.first!.communityId
                newDocumentCommunityName = userCommunities.first!.communityName
                guard newDocumentCommunityId != nil else {
                    print ("[NC!!] Error creating document, community id is nil")
                    block (nil)
                    return
                }
                block (newDocumentCommunityId)
            
            case let numberOfUserCommunities where numberOfUserCommunities > 1 :  // we need to show communitiy picker
                
                    let message = "\n\n\n\n\n\n"
                    let alert = UIAlertController(title: "Please Select Community", message: message, preferredStyle: UIAlertController.Style.actionSheet)
                    alert.isModalInPopover = true
                    let screenWidth = self.view.frame.size.width
                    let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: screenWidth - 20, height: 140)) // CGRectMake(left, top, width, height) - left and top are like margins
                    pickerFrame.tag = 1 // this will tell us if that this specific picker is to select one community to make a new communication
                    //set the pickers datasource and delegate
                    pickerFrame.delegate = self
                    pickerFrame.dataSource = self
                    
                    //Add the picker to the alert controller
                    alert.view.addSubview(pickerFrame)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        // Now we check what we did select
                        if self.newDocumentCommunityId == nil { // We didn't roll the picker
                            self.newDocumentCommunityName = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityName
                            self.newDocumentCommunityId = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityId
                            print ("[NC--] We didn't roll the picker, community is: \(String(describing: self.newDocumentCommunityId)) and name: \(String(describing: self.newDocumentCommunityName)) ")
                            block (nil)
                        }
                        guard self.newDocumentCommunityId != nil else {
                            print ("[NC!!] Error creating document, community id is nil")
                            return
                        }
                        
                        block (self.newDocumentCommunityId)
                    })
                    alert.addAction(okAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    self.parent!.present(alert, animated: true, completion: nil)
            
            default:
                block (nil)
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities assigned", comment: ""), message: NSLocalizedString("You must be registered at least in one community", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            }
    }
    
     // MARK: Community Picker View Delegate functions
        
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let rows : Int
        
        switch pickerView.tag {
        case 0: // Filtering communicties, it has "all communities" option plus each individual community
            rows = self.userCommunitiesIds.count + 1
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            rows = userCommunities.count
        case 2: // Selection of document Type
            rows = self.documentTypes.count
        default:
            rows = 0
        }
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let title : String?
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            if row == self.userCommunities.count {
                title = NSLocalizedString("All communities", comment: "")
            } else {
                title = self.userCommunities[row].communityName
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            title = userCommunities[row].communityName
        
        case 2: // Selection of document Type
            title = self.documentTypes[row].description
            
        default:
            title = nil
        }
        return title
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            self.pickerViewRolled = true
            if row == self.userCommunities.count {
                self.userCommunitiesIdsFiltered = self.userCommunitiesIds
            } else {
                self.userCommunitiesIdsFiltered = [userCommunities[row].communityId!]
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            self.newDocumentCommunityId = userCommunities[row].communityId
            self.newDocumentCommunityName = userCommunities[row].communityName
            print ("[NC--] The selected community picker is: \(String(describing: self.newDocumentCommunityId)) and name: \(String(describing: self.newDocumentCommunityName)) ")
            
        case 2: // Selection of document Type
            self.documentTypeSelected = documentTypes[row]
        
        default:
            return
        }
        
    }


    // MARK: UICollectionViewDataSource
           
           
           
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Set(self.sections).count // Creamos un array con un string que tiene el tipo de fichero + comunidad y contamos los elementos diferentes
   }
   
   
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.documents.filter{String($0.type) == self.sections[section][0] && $0.communityId == self.sections[section][1]}.count
   }
   
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        // Configure the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NCDocumentCollectionSectionViewCell

        let document = self.documents.filter{String($0.type) == self.sections[indexPath.section][0] && $0.communityId == self.sections[indexPath.section][1]}[indexPath.row]
        switch document.mimeType {
           case "image/jpeg":
            cell.documentImageView.image = UIImage (named: "icons8-image_file-1")
           case "application/pdf":
            cell.documentImageView.image = UIImage (named: "icons8-pdf-1")
           default:
            cell.documentImageView.image = UIImage (named: "icons8-document")
           }
        cell.docNameLabel.text = document.docName
        cell.docCreationDate.text = document.uploadDate.dateValue().getFormattedDate(format: "dd/MM/yy HH:mm") // Set output formate
        cell.fileMimeType = document.mimeType
        return cell
   }
   
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       guard let cell = collectionView.cellForItem(at: indexPath) as? NCDocumentCollectionSectionViewCell
           else {return}
       if self.isEditing {
           let cell = collectionView.cellForItem(at: indexPath)
           cell?.layer.borderWidth = 2.0
           cell?.layer.borderColor = UIColor.systemBlue.cgColor
           
           //deleteDocumentButton.isHidden = false
       } else {
           switch cell.fileMimeType {
           case "image/jpeg":
               self.performSegue(withIdentifier: "detailToPhoto", sender: cell)
           case "application/pdf":
               self.performSegue(withIdentifier: "toPdfViewer", sender: cell)
           default:
               return
           }
               
           //deleteButton.isEnabled = false
       }
   }
       
   
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
       
       let cell = collectionView.cellForItem(at: indexPath)
       cell?.layer.borderWidth = 2.0
       cell?.layer.borderColor = UIColor.clear.cgColor
       

   }
    
    // MARK: - Collection View Flow Layout Delegate

   func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
          let sectionInsets = UIEdgeInsets(top: spacing/4, left: spacing, bottom: spacing * 2, right: spacing)
          return sectionInsets
      }
    
      internal func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return spacing
      }
      
      private func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          minimumInteritemSpacing section: Int) -> CGFloat {
          return spacing
      }
   //1
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let numberOfItemsPerRow:CGFloat = 4
        let spacingBetweenCells:CGFloat = 16
        
        let totalSpacing = (2 * self.spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        if let collection = self.collectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            self.collectionView.updateConstraints()
            return CGSize(width: width, height: width * 2) // We double the height because the is not a square
        }else{
            return CGSize(width: 0, height: 0)
        }
  }

    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        // 1
        switch kind {
        // 2
        case UICollectionView.elementKindSectionHeader:
          // 3
          guard
            let headerView = collectionView.dequeueReusableSupplementaryView(
              ofKind: kind,
              withReuseIdentifier: "HeaderView",
              for: indexPath) as? NCDocumentCollectionHeaderView
            else {
              fatalError("Invalid view type")
          }
            let document = self.documents.filter{String($0.type) == self.sections[indexPath.section][0] && $0.communityId == self.sections[indexPath.section][1]}[indexPath.row]
          //let documentsByTypeFilteredByTypeDocuments = self.documentsByType.filter({ $0.count != 0}) // This is to avoid showing sections empty (with no documents)
            let docTypeInteger = Int(self.sections[indexPath.section][0]) // We first look what is the type integer of the inside documents
          let docTypeDescription = self.documentTypes.first(where: { $0.typeNumber == docTypeInteger })?.description ?? NSLocalizedString("Unknow type", comment: "A document with unknown type")
            let communityLabel = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: document.communityId)
          headerView.headerViewDocumentTypeLabel.text = docTypeDescription
          if self.userCommunitiesIdsFiltered.count > 1 {
            headerView.headerViewCommunityLabel.isHidden = false
            headerView.headerViewCommunityLabel.text = communityLabel
          } else {
            headerView.headerViewCommunityLabel.isHidden = true
          }
          
           // we look for the description of the type integer
          return headerView
            
        default:
          // 4
          assert(false, "Invalid element type")
          return UICollectionReusableView.init()
        }
    }

    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier
        {
            switch identifier
            {
            
            case "detailToPhoto":
                
                let cell = sender as! NCDocumentCollectionSectionViewCell
                let destinationVC = segue.destination as! NCPhotoVC
                if let index = self.collectionView.indexPath(for: cell) {
                    var document = self.documents.filter{String($0.type) == self.sections[index.section][0] && $0.communityId == self.sections[index.section][1]}[index.row]
                    NCModelAPI.sharedInstance.getStorageFile(path: document.fileStorageReferencePath, blockAfterGettingFile: { data, err in
                        if let error = err {
                            print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                        } else {
                            if data != nil {
                                print ("[NC] Document number \(index.row+1) obtained from firestorage")
                                document.fileData = data
                                NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key: document.fileStorageReferencePath, data: data)
                                destinationVC.imageToPresent = UIImage (data: data!)
                            } else {
                                print ("[NC!!] Error obtaining image data")
                                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting data from image", vc: self)
                            }
                        }
                    })
                }
                
            case "toPdfViewer":
                print("going to pdfviewer")
                let cell = sender as! NCDocumentCollectionSectionViewCell
                let destinationVC = segue.destination as!NCPDFVC
                if let index = self.collectionView.indexPath(for: cell) {
                    var document = self.documents.filter{String($0.type) == self.sections[index.section][0] && $0.communityId == self.sections[index.section][1]}[index.row]
                    NCModelAPI.sharedInstance.getStorageFile(path: document.fileStorageReferencePath, blockAfterGettingFile: { data, err in
                        if let error = err {
                            print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                        } else {
                            if data != nil {
                                print ("[NC] Document number \(index.row+1) obtained from firestorage")
                                document.fileData = data
                                NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key:
                                    document.fileStorageReferencePath, data: data)
                                destinationVC.pdfDocument = PDFDocument.init(data: data!)
                            } else {
                                print ("[NC!!] Error obtaining pdf data")
                                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting pdf from data", vc: self)
                            }
                        }
                    })
                }
                    
            default: return
            }
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}


    

