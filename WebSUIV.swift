//
//  WebSUIV.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 13/9/22.
//  Copyright © 2022 Nutcoders. All rights reserved.
//
import Foundation
import SwiftUI
import WebKit
 
@available(iOS 13.0, *)
struct WebSUIV: UIViewRepresentable {
 
    var url: URL
 
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
 
    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}


