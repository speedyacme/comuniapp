//
//  NCPDFVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 09/01/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import PDFKit

class NCPDFVC: UIViewController {
    
    var pdfDocument : PDFDocument! {
        didSet
        {
            self.updatePdfDocument()
        }
    }
    var pdfView : PDFView = PDFView()
    override func viewDidLoad() {
        super.viewDidLoad()

       
            // ...

            // Add PDFView to view controller.
            pdfView = PDFView(frame: self.view.bounds)
            pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.addSubview(pdfView)
            
            // Fit content in PDFView.
            pdfView.autoScales = true
            
            // Load Sample.pdf file from app bundle.
            //let fileURL = Bundle.main.url(forResource: "Sample", withExtension: "pdf")
            //pdfView.document = PDFDocument(url: fileURL!)
            pdfView.document = pdfDocument
    }
    
    func updatePdfDocument () {
        self.pdfView.document = pdfDocument
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
