//
//  NCIssueTableViewCell.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 16/07/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit

class NCIssueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var issueTitleLabel: UILabel!
    @IBOutlet weak var openningDateLabel: UILabel!
    @IBOutlet weak var communityLabel: UILabel!
    @IBOutlet weak var creatorNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor (named: "NCAppBackgroundColor")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
