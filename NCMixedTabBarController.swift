//
//  NCMixedTabBarController.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 7/4/23.
//  Copyright © 2023 Nutcoders. All rights reserved.
//

import UIKit
import SwiftUI

class NCMixedTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        // Create the first tab
        let firstVC : UIViewController
        let communicationsVC = storyboard.instantiateViewController(withIdentifier: "communicationsTableVC")
        let firstNavController = UINavigationController(rootViewController: communicationsVC)
        firstVC = firstNavController
        if #available(iOS 13.0, *) {
            firstVC.tabBarItem = UITabBarItem(title: "Communications", image: UIImage(systemName: "mail"), tag: 1)
        } else {
            // Fallback on earlier versions
            firstVC.tabBarItem = UITabBarItem(title: "Communications", image: UIImage(named: "icons8-notification_center"), tag: 1)
        }
        
        // Create the second tab
        let secondVC : UIViewController
        let issuesVC = storyboard.instantiateViewController(withIdentifier: "NCIssuesVCID")
        let secondNavController = UINavigationController(rootViewController: issuesVC)
        secondVC = secondNavController
        if #available(iOS 13.0, *) {
            secondVC.tabBarItem = UITabBarItem(title: "Issues", image: UIImage(systemName: "exclamationmark.circle"), tag: 1)
        } else {
            // Fallback on earlier versions
            firstVC.tabBarItem = UITabBarItem(title: "Issues", image: UIImage(named: "icons8-error"), tag: 1)
        }
        
        // Create the third tab
        let thirdVC : UIViewController
        if  #available(iOS 13.0, *) {
            // Use a SwiftUI view if the user has iOS 13 or later
            //thirdVC = UINavigationController(rootViewController: UIHostingController(rootView: NavigationView { NCInfosSwiftUIView() }
                //.navigationBarTitle("Informations", displayMode: .automatic)))
            thirdVC = UIHostingController(rootView: NCInfosSwiftUIView())
        } else {
            // Use a UIKit view controller from the storyboard if the user has iOS below 13
            let informationsVC = storyboard.instantiateViewController(withIdentifier: "NCInfosVCID")
            let thirdNavController = UINavigationController(rootViewController: informationsVC)
            thirdVC = thirdNavController
        }
        if #available(iOS 13.0, *) {
            thirdVC.tabBarItem = UITabBarItem(title: "Info", image: UIImage(systemName: "info.circle"), tag: 2)
        } else {
            // Fallback on earlier versions
            firstVC.tabBarItem = UITabBarItem(title: "Info", image: UIImage(named: "icons8-info"), tag: 1)
        }
        
        // Create the fourth tab
        let fourthVC : UIViewController
        let documentsVC = storyboard.instantiateViewController(withIdentifier: "NCDocumentVCID")
        let fourthNavController = UINavigationController(rootViewController: documentsVC)
        fourthVC = fourthNavController
        if #available(iOS 13.0, *) {
            fourthVC.tabBarItem = UITabBarItem(title: "Documents", image: UIImage(systemName: "doc.text"), tag: 3)
        } else {
            // Fallback on earlier versions
            firstVC.tabBarItem = UITabBarItem(title: "Documents", image: UIImage(named: "icons8-documents"), tag: 1)
        }

        // Create the fifth tab
        let fifthVC: UIViewController
        if  #available(iOS 13.0, *) {
            // Use a SwiftUI view if the user has iOS 13 or later
            fifthVC = UIHostingController(rootView: SettingsSwiftUIView())
        } else {
            // Use a UIKit view controller from the storyboard if the user has iOS below 13
            let settingsVC = storyboard.instantiateViewController(withIdentifier: "uikitSettingsVC")
            let navController = UINavigationController(rootViewController: settingsVC)
            fifthVC = navController
        }
        if #available(iOS 13.0, *) {
            fifthVC.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(systemName: "gear"), tag: 4)
        } else {
            // Fallback on earlier versions
            firstVC.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "icons8-error"), tag: 1)
        }

        // Add the view controllers to the tab bar controller
        viewControllers = [firstVC, secondVC, thirdVC, fourthVC, fifthVC]
    }
}
