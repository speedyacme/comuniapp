//
//  NCInfoTableViewCell.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 20/09/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit

class NCInfoTableViewCell: UITableViewCell {

   
    @IBOutlet weak var callButton: NCUIButtonWithIndexPath!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
