//
//  NCSettingsTableVC.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 26/12/16.
//  Copyright © 2016 Nutcoders. All rights reserved.
//

import UIKit
import MessageUI
import FirebaseAuth

class NCSettingsTableVC: UITableViewController, MFMailComposeViewControllerDelegate, editNameVCDelegateProtocol {

    @IBOutlet var accountNameLabel: UILabel!
    @IBOutlet var accountPhoneNumberLabel: UILabel!
    @IBOutlet var aboutVersionLabel: UILabel!
    @IBOutlet var aboutBundleLabel: UILabel!
    
    var name : String?
    var phoneNumber : String?
    var version : String?
    var bundle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.name = NCModelAPI.sharedInstance.getMyUserName()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.updateLabels()
    }
    
    func updateLabels() {
        accountNameLabel.text = self.name
        accountPhoneNumberLabel.text = NCModelAPI.sharedInstance.getMyPhoneNumber()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            aboutVersionLabel.text = version
            self.version = version
        }
        if let bundle = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            aboutBundleLabel.text = bundle
            self.bundle = bundle
        }
        for cell in self.tableView.visibleCells {
            cell.backgroundColor = UIColor (named: "NCAppBackgroundColor")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signOutButtonPushed(_ sender: UIButton) {
        
        
        DispatchQueue.main.async(execute: {
            // Create the alert
            let beforeSignOutAlert = UIAlertController (title: NSLocalizedString("Signing Out", comment: ""), message: NSLocalizedString("Are you sure you want to Sign Out?", comment: ""), preferredStyle: .alert)
            beforeSignOutAlert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: .none))
            beforeSignOutAlert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .destructive, handler: {(alert: UIAlertAction!) in
                
                print("\nNC Going to Sign Out\n")
                 // MARK: - SIGNOUT DE FIREBASE - This Logs Out of Firebase, for instance to enter with a different phone number
                if let error = NCModelAPI.sharedInstance.logOut() {
                    NCGUI.showErrorAlertOnCurrentVC(errorTitle: error.localizedDescription, error: error, vc: self)
                } else {
                    // MARK: LOG OUT OK - GO TO REGISTER PROCESS AGAIN
                    NCGUI.showVC(viewControllerIdentifier: "startVC")
                }
            }))
            // Present the alert
            self.present(beforeSignOutAlert, animated: true, completion: nil)
        })
    }

    @IBAction func notifyNewEventsSwitchChanged(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: constants.defaults.notifyNewEventsCheck)
    }
 
    @IBAction func notifyEventChangesSwitchChanged(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: constants.defaults.notifyChangesInEventsCheck)
    }
    @IBAction func openInSettingsAppPushed(_ sender: UIButton) {
        guard let url = URL(string:UIApplication.openSettingsURLString) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func sendFeedBackEmailPushed(_ sender: UIButton) {
        self.sendEmail()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([constants.urls.appEMail])
            mail.setMessageBody("\n\n\n\n\n\n\n\nApp Data:\nVersion: \(String(describing: self.version))\nBundle: \(String(describing: self.bundle))", isHTML: false)
            mail.setSubject(NSLocalizedString("CommunyApp Support", comment: ""))
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    @IBAction func shareWithFriendsButtonPressed(_ sender: UIButton) {
        // text to share
        let text = NSLocalizedString("This is some text that I want to share.", comment: "")
        
        // image to share
        let image = UIImage(named: "icon_comunity_white")
        
        // set up activity view controller
       
        let activityViewController = UIActivityViewController(activityItems: [text, image!], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)

    }
    
    @IBAction func webButtonPressed(_ sender: UIButton) {
        
        print ("NC Web Button pressed")
        // Typical usage
        NCGeneric.openURLinExternalBrowser(urlAddress: constants.urls.appUrl)
    }
    
    
    
    // share image
    @IBAction func shareImageButton(_ sender: UIButton) {
        
        // image to share
        let image = UIImage(named: "Image")
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func nameChanged(nameEdited: String?) {
        self.name = nameEdited
        self.updateLabels()
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier
        {
            switch identifier {
                
            case "segueToEditingName":
                let destinationVC = segue.destination as! NCEditNameVC
                destinationVC.delegate = self
                destinationVC.name = self.name
            default: return
                
            }
        }
    }

    
    

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
