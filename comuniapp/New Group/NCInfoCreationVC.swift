//
//  NCInfoCreationVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 27/09/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit

class NCInfoCreationVC: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var organizationTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emergencySwitch: UISwitch!
    var userCommunityId : String?
    var organization : String?
    var phone : String?
    var emergency : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneButtonPushed))
        self.navigationItem.rightBarButtonItem = doneButton
        let cancelButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelButtonPushed))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        self.organizationTextField.delegate = self
        self.organizationTextField.tag = 1
        self.organizationTextField.keyboardType = .default
        self.phoneTextField.delegate = self
        self.phoneTextField.tag = 2
        self.phoneTextField.keyboardType = .phonePad
        
        
    }
    
    // UITexfield delegate functions
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // This will call texFieldDidEndEditing
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 1:
            self.organization = textField.text
            return true
        case 2:
            self.phone = textField.text
            return true
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ sender: UITextField) {
        
    }
    
    @objc func doneButtonPushed () {
        self.organization = self.organizationTextField.text
        self.phone = self.phoneTextField.text
        let type : Int
        switch self.emergency {
        case true:
            type = 1
        default:
            type = 2
        }
        if let description = self.organization, let phoneNumber = self.phone, let communityId = self.userCommunityId {
            let newInfo = NCInfo.init(description: description, phone: phoneNumber, type: type, communityId: communityId)
            let collectionPath =  constants.fireStore.communities.collectionName+"/"+communityId+"/"+constants.fireStore.info.collectionName
            NCModelAPI.sharedInstance.addCodableDocument(codableDocument: newInfo, collectionPath: collectionPath, blockAfterCreatingDocument: { error, ref in
                if error != nil {
                    print ("[NC!!] Error uploading info document: \(String(describing: error?.localizedDescription))")
                } else {
                    print ("[NC] Info Document uploaded correctly")
                    self.navigationController?.popViewController(animated: true)
                }
            })
        } else {
            NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Empty Fields", comment: ""), message: NSLocalizedString("Both text fields must be filled", comment: ""), vc: self)
        }
    }
    
    @objc func cancelButtonPushed () {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func emergencySwithed(_ sender: UISwitch) {
        self.emergency = sender.isOn
    }

}
