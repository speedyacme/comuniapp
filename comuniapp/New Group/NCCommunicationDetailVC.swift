//
//  NCCommunicationDetailVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 29/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import PhotosUI
import MobileCoreServices
import QuickLook
import PDFKit
import JGProgressHUD


private let reuseIdentifier = "newCell"
fileprivate let itemsPerRow: CGFloat = 3

class NCCommunicationDetailVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UITextViewDelegate,UIDocumentPickerDelegate {
    
    let quickLookController = QLPreviewController()
    var isAdministrator : Bool = false
    var communication : NCCommunication!
    @IBOutlet var topLineView: UIView!
    @IBOutlet var bottomLineView: UIView!
    @IBOutlet weak var photoEditButtonsView: UIView!
    @IBOutlet weak var shortDescriptionLabel: UITextField!
    @IBOutlet weak var dateOwnerLabel: UILabel!
    @IBOutlet weak var longDescriptionLabel: UITextView!
    @IBOutlet var collectionDocumentView: UICollectionView!
    @IBOutlet weak var addDocumentButton: UIButton!
    @IBOutlet weak var deleteDocumentButton: UIButton!
    var temporalShortDescription : String?
    var temporalLongDescription : String?
    private var queryListener : ListenerRegistration?
    var listener : ListenerRegistration?
    var completeDocumentsPath : String!
    override var isEditing: Bool {
        didSet {
            // Hacer todo lo que queramos en función si se ha seleccionado edit o no
            if self.viewIfLoaded != nil { // The view is loaded
                self.updateUIbaseOnEditingStatus()
            }
        }
    }
    var documents = [NCDocument]() {
        didSet
        {
            self.updateUI()
        }
    }

    var partialPath : String! { // The path of the communication item where the documents are included
        didSet
        {
            self.completeDocumentsPath = self.partialPath + "/" + constants.fireStore.communications.documentsCollectionName
        }
    }
    
    
    // MARK: ViewDidLoad()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.collectionDocumentView.delegate = self
        self.collectionDocumentView.dataSource = self
        self.shortDescriptionLabel.delegate = self
        self.longDescriptionLabel.delegate = self
        self.updateUI()
        NCModelAPI.sharedInstance.isUserAdmin(blockAfterAdminInfo: { isAdmin, error in
            if let error = error {
                print("[NC] Couldn't get user Admin info \(error.localizedDescription)")
            } else {
                self.isAdministrator = isAdmin
                if isAdmin && self.communication.docRef != nil {
                    self.isEditing = false
                }
            }
        })
        
        self.title = NSLocalizedString("Communication", comment: "")
        self.navigationItem.prompt = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: self.communication.communityId)
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        //self.navigationItem.titleView =  self.navigationItem.setTitle(NSLocalizedString("Communications", comment: ""), subtitle: communication.communityName)
        let dateString = DateFormatter.localizedString(from: communication.date.dateValue(), dateStyle: .long, timeStyle: .short)
        self.dateOwnerLabel.text = communication.creatorNickName + " - " + dateString
        self.setListener()
        self.updateUIbaseOnEditingStatus()
        
    }
    
    func updateUIbaseOnEditingStatus () {
        if isEditing {
            self.photoEditButtonsView.isHidden = !(self.communication.docRef != nil)
            self.topLineView.isHidden = true
            self.bottomLineView.isHidden = !(self.communication.docRef != nil)
            self.shortDescriptionLabel.setBottomBorder()
            self.longDescriptionLabel.layer.borderColor = UIColor.lightGray.cgColor
            self.longDescriptionLabel.layer.borderWidth = 1.0
            self.longDescriptionLabel.layer.cornerRadius = 3
            self.navigationItem.rightBarButtonItems?.removeAll()
            let doneButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.doneButtonPushed))
            self.navigationItem.rightBarButtonItem = doneButton
            if self.communication.docRef != nil {
                let trashButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonItem.SystemItem.trash, target: self, action: #selector(self.deleteCommunicationPushed))
                self.navigationItem.leftBarButtonItem = trashButton
            } else {
                let cancelButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelButtonPushed))
                self.navigationItem.leftBarButtonItem = cancelButton
            }
            if self.isViewLoaded {
                self.longDescriptionLabel.isEditable = true
                self.collectionDocumentView.allowsSelection = true
                self.collectionDocumentView.allowsMultipleSelection = true
                self.addDocumentButton.isHidden = false
                self.shortDescriptionLabel.becomeFirstResponder()
            }
            
        } else { // We are not editing
            
            if self.documents.count != 0 {
                self.topLineView.isHidden = true
                self.bottomLineView.isHidden = false
            } else {
                self.topLineView.isHidden = true
                self.bottomLineView.isHidden = true
            }
            self.shortDescriptionLabel.removeBottomBorder()
            self.longDescriptionLabel.layer.borderColor = UIColor.clear.cgColor
            self.shortDescriptionLabel.endEditing(true)
            self.longDescriptionLabel.endEditing(true)
            self.navigationItem.leftBarButtonItems?.removeAll()
            self.navigationItem.rightBarButtonItems?.removeAll()
            if isAdministrator && self.isViewLoaded {
                self.photoEditButtonsView.isHidden = !(self.communication.docRef != nil)
                self.deleteDocumentButton.isHidden = true // We only show adddocument button
                //let fixedSpace = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
                let editButton = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(self.editButtonPushed))
                self.navigationItem.rightBarButtonItem = editButton
                //self.navigationItem.rightBarButtonItems?.append(fixedSpace)
                if self.isViewLoaded {
                    self.longDescriptionLabel.isEditable = false
                    self.collectionDocumentView.endEditing(true)
                }
            }
            
            if self.isViewLoaded {
                
                self.longDescriptionLabel.isEditable = false
                self.collectionDocumentView.allowsSelection = true // We need to select a picture to see it in detail.
                self.collectionDocumentView.allowsMultipleSelection = false
                self.shortDescriptionLabel.borderStyle = UITextField.BorderStyle.none
                self.longDescriptionLabel.layer.borderColor = UIColor.clear.cgColor;
                self.removeSelectionRectangleToAllSelectedItems()
            }
        }
    }
    
    func setListener() {
        
        if let currentCommunicationPath = self.communication?.docRef?.path {
            // Since the documents inside the communications are a different collection and is not loaded with the communications collection, we have to set a querylistener for this subcollection specifically
            self.queryListener = NCModelAPI.sharedInstance.getNCDocumentsListener(collectionPath: currentCommunicationPath + "/" + constants.fireStore.communications.documentsCollectionName, orderField: constants.fireStore.documents.uploadDate, descending: true, limit: 25, blockAfterGettingDocuments: { NCDocumentsArray, error in
                
                if let error = error {
                    print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                } else {
                    guard let NCDocumentsArray = NCDocumentsArray else {
                        return
                    }
                    self.documents = NCDocumentsArray.sorted(by: { $0.uploadDate.dateValue() < $1.uploadDate.dateValue() })
                    print ("[NC] Listener obtained Snapshot and Mapped \(self.documents.count) documents from FireStore")
                    
                    self.updateUIbaseOnEditingStatus()
                    if self.documents.count != 0 {
                        for index in 0 ... (self.documents.count - 1) {
                            NCModelAPI.sharedInstance.getStorageFile(path: self.documents[index].fileStorageReferencePath, blockAfterGettingFile: { data, error in
                                if error != nil {
                                    print ("[NC!!] Error obtaining files: \(String(describing: error)) ")
                                } else {
                                    print ("[NC] File number \(index+1) obtained correctly")
                                    self.documents[index].fileData = data
                                }
                            })
                        }
                    }
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Now we load the documents of the communication (only the list of documents)
        self.updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isBeingDismissed {
            self.queryListener?.remove()
        }
    }
    
    func updateUI () {
        
        self.shortDescriptionLabel.text = self.communication.shortDescription
        self.longDescriptionLabel.text = self.communication.longDescription
        self.collectionDocumentView.reloadData()
    }
    
    
    // UITexfield delegate functions
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.isEditing {
            textField.layer.borderColor = UIColor(named: "MainAppBlueColor")?.cgColor
            return true
        } else {
            textField.layer.borderColor = UIColor.white.cgColor
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() // This will call texFieldDidEndEditing
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ sender: UITextField) {
        self.temporalShortDescription = sender.text
        if self.shortDescriptionLabel.isFirstResponder {
            self.shortDescriptionLabel.resignFirstResponder()
        }
    }
    
     // UITextview delegate functions
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self.isEditing {
            return true
        } else {
            return false
        }
    }
  
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.temporalLongDescription = textView.text
        if self.longDescriptionLabel.isFirstResponder {
            self.longDescriptionLabel.resignFirstResponder()
        }
    }
    
    @objc func editButtonPushed() {
        
        self.isEditing = !self.isEditing
    }
    
    @IBAction func addDocumentButtonPushed(_ sender: UIButton) {
        // First we assign to curren communication variable the current short and long description
        if self.shortDescriptionLabel.text != nil {
            self.communication.shortDescription = self.shortDescriptionLabel.text!
        }
        if self.longDescriptionLabel.text != nil {
            self.communication.longDescription = self.longDescriptionLabel.text
        }

        // Then show a action menu to choose photos or documents
        let message = ""
        let alert = UIAlertController(title: NSLocalizedString("Select Document type", comment: "") , message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        let photoAction = UIAlertAction(title: NSLocalizedString("Photo", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.addPhoto()
        })
        alert.addAction(photoAction)
        let otherDocumentAction = UIAlertAction(title: NSLocalizedString("PDF Document", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.pickDocument()
        })
        alert.addAction(otherDocumentAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.parent!.present(alert, animated: true, completion: nil)
    }
    
        
    
    func addDocument () {
           self.pickDocument()
       }

    func pickDocument () {
    //launch document picker
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeJPEG)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }

    func addPhoto () {
        // launch image picker
        let picker = NCPhotoPicker.init(delegate: self)
        picker.launchImagePicker()
    }
       
    func deleteAttachedDocuments (blockAfterDeletingAllDocuments: @escaping (Error?) -> ()) {
        
        if self.documents.count > 0 {
            for doc in self.documents {
                if let docId = doc.id, let lastDocId = self.documents.last?.id {
                    
                    NCModelAPI.sharedInstance.deleteFirestoreDocumentWithStorageFile(docRef: Firestore.firestore().collection(completeDocumentsPath).document(docId), fileStorageReferencePath: doc.fileStorageReferencePath, block: { error in
                        if error != nil {
                            print ("[NC!!] Error deleting document \(doc.docName) : \(String(describing: error?.localizedDescription))")
                            blockAfterDeletingAllDocuments (error)
                        } else {
                            print ("[NC] Document \(String(describing: docId)) deleted correctly")
                            if docId == lastDocId { // This is the last doc of the array, so now we erase messages
                                blockAfterDeletingAllDocuments (nil)
                            }
                        }
                    })
                } else {
                    print ("[NC!!] Error deleting documents, doc.docRef and/or self.documents.last?.docRef are nil")
                    blockAfterDeletingAllDocuments (NSError(domain:"nilValue", code:1992, userInfo:nil))
                }
            }
        } else {
            blockAfterDeletingAllDocuments(nil)
        }
    }
    
    
    @objc func deleteCommunicationPushed () {
       
        let alertController = UIAlertController(title: NSLocalizedString("Are you sure to delete this communication?", comment:""), message: NSLocalizedString("If you press delete the communication will be removed from the communications list", comment: ""), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { (action) in
            //self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (action) in
            guard let documentReference = self.communication.docRef
                else {
                    print ("[NC!!] Document couldn't be deleted because docref is nil")
                    return
                }
                
                // First we delete all the documents of files attached
                self.deleteAttachedDocuments(blockAfterDeletingAllDocuments: {error in
                    if error != nil  {
                        print ("[NC!!] Error deleting documents and files: \(String(describing: error))")
                        NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(error!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
                    } else {
                        NCModelAPI.sharedInstance.deleteDocument(pathRef: documentReference.path, block: { err in
                            if err != nil {
                                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
                            } else {
                                print ("[NC] Communication deleted correctly")
                                NCFirebaseCloudMessagingAPI.unsubscribeFromTopic(documentReference.documentID)
                               self.navigationController?.popViewController(animated: true)
                            }
                        })
                    }
                    
                })
        }))
        
        if (self.isViewLoaded) && (self.view.window != nil) {
            DispatchQueue.main.async {
                self.present (alertController, animated: true, completion:nil)
            }
        }
    }
    @IBAction func deleteDocumentButtonPushed(_ sender: Any) { // This is the button for deleting the documents selected.
        // Since here we can only delete photo documents, we call to delete photo
        if let indexes = self.collectionDocumentView.indexPathsForSelectedItems {
            //let items = selectedCells.map { $0.item }.sorted().reversed() // Esto lo usaban no sé para qué...
            for index in indexes {
                self.deleteFileAndFileDocument(doc: self.documents[index.row])
            }
        }
        self.removeSelectionRectangleToAllSelectedItems()
    }
    
    func deleteFileAndFileDocument (doc: NCDocument) { // Here we delete one of the documents of the files attached
        
        guard let docId = doc.id else {
            print ("[NC!!] Deletion error, document has no reference")
            NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "The document couldn't be deleted", vc: self)
            return
        }
        NCModelAPI.sharedInstance.deleteFirestoreDocumentWithStorageFile(docRef: Firestore.firestore().collection(completeDocumentsPath).document(docId), fileStorageReferencePath: doc.fileStorageReferencePath, block: { err in
            
            if err != nil {
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Delete error", comment: ""), message: NSLocalizedString("Please try again. \(err!.localizedDescription)", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            } else {
                print ("[NC] Document deleted correctly")
                self.updateUI()
            }
        })
    }
    
    
    @objc func doneButtonPushed () {
        self.removeSelectionRectangleToAllSelectedItems()
        // Checking if there is title and description
        guard self.shortDescriptionLabel.text != nil, self.shortDescriptionLabel.text != "", self.longDescriptionLabel.text != nil, self.longDescriptionLabel.text != "" else {
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Empty Field", comment: ""), message: NSLocalizedString("Title and description texts are required", comment: ""), cancelText: NSLocalizedString("Ok", comment: ""), continueText: nil)
            return
        }
        self.checkAndSave()
    }
    
    
    
    func checkAndSave () {
        
        if let newShortDescription = self.shortDescriptionLabel.text{
            self.communication.shortDescription = newShortDescription
        }
        if let newLongDescription = self.longDescriptionLabel.text {
             self.communication.longDescription = newLongDescription
        }
        if self.communication.docRef != nil { // editing communication
            self.communicationEdited(communication: self.communication)
        } else { // new communication
            self.createANewCommunication()
        }
    }
    
    func communicationEdited (communication: NCCommunication) {
        // TODO: Guardar comunicación actual
        self.communication = communication
        self.updateUI()
        guard let documentReference = communication.docRef else {
                print ("[NC!!] Error, communication reference is nil")
                self.dismiss(animated: true, completion: nil)
                return
        }
        let hud = JGProgressHUD()
        hud.show(in: self.view, animated: true)
        NCModelAPI.sharedInstance.updateDocument(documentDictionary: self.communication.dictionary, docRef: documentReference, blockAfterUpdating: { error in
             if let error = error{
                 hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                 hud.textLabel.text = NSLocalizedString("Error updating communication", comment: "")
                 hud.dismiss(afterDelay: 3, animated: true)
                 print ("[NC!!] Error updating document in the Cloud: \(error.localizedDescription)")
                 NCGUI.showErrorAlertOnCurrentVC(errorTitle: "We couldn't update the document in the Cloud. Try again later", error: error, vc: self)
             }
             else {
                 self.isEditing = false // We have updated the item, so we go back to not editing mode
                 hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                 hud.textLabel.text = NSLocalizedString("Communication updated", comment: "")
                 hud.dismiss(afterDelay: 3, animated: true)
                 // NCFirebaseCloudMessagingAPI.sendMessage(title: self.communication.shortDescription, message: self.communication.longDescription, topic: self.communication.communityId, type: .community_new_communication, firebaseReferencePath: documentReference.path, senderPhoneNumber: self.communication.creatorUserId ,completion: nil)
             }
        })
    }
    
    func createANewCommunication () {
        let collectionPathString = constants.fireStore.communities.collectionName + "/" + self.communication.communityId + "/" + constants.fireStore.communications.collectionName
        let hud = JGProgressHUD()
        hud.show(in: self.view, animated: true)
        NCModelAPI.sharedInstance.addDocument(documentDictionary: self.communication.dictionary, collectionPath: collectionPathString,
            blockAfterCreatingDocument: { error, ref in
                if error != nil {
                    NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Document creation Error", error: error!, vc: self)
                    self.isEditing = true
                    hud.textLabel.text = "Loading"
                    hud.dismiss(animated: true)
                } else {
                    print("[NC] Document added correctly in collection Path \(collectionPathString) and reference: \(String(describing: ref))")
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                    hud.textLabel.text = NSLocalizedString("Communication created", comment: "")
                    hud.dismiss(afterDelay: 3, animated: true)
                    if let reference = ref {
                        self.communication.docRef = ref
                        //NCFirebaseCloudMessagingAPI.subscribeToTopic(reference.documentID)
                        print ("[NC] Subscribed to topic: \(reference.documentID)")
                        self.partialPath = collectionPathString + "/" + reference.documentID
                        self.setListener()
                    }
                    self.isEditing = false
                }
        })
    }

    
    @objc func cancelButtonPushed () {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    func removeSelectionRectangleToAllSelectedItems () {
        if let selectedIndexPaths = self.collectionDocumentView.indexPathsForSelectedItems {
            for indexPath in selectedIndexPaths {
                self.collectionDocumentView.deselectItem(at: indexPath, animated: true)
                let cell = self.collectionDocumentView.cellForItem(at: indexPath)
                //cell?.layer.borderWidth = 2.0
                cell?.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return documents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Configure the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NCDocumentCollectionViewCell
        
        let document = self.documents[indexPath.row]
        if let data = document.fileData {
            switch document.mimeType {
                case "image/jpeg":
                    cell.cellImageView.image = UIImage (data: data)
                    ?? UIImage (named: "icons8-image_file-1")
                case "application/pdf":
                    cell.cellImageView.image = UIImage (named: "icons8-pdf-1")
                default:
                    cell.cellImageView.image = UIImage (named: "icons8-document")
            }
        } else {    
            if cell.isSelected {
                cell.cellImageView.image = UIImage (named: "icons8-image_file_filled")
            } else {
                cell.cellImageView.image = UIImage (named: "icons8-image_file")
            }
        }
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? NCDocumentCollectionViewCell
            else {return}
        if self.isEditing {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderWidth = 2.0
            cell?.layer.borderColor = UIColor.systemBlue.cgColor
            
            deleteDocumentButton.isHidden = false
        } else {
            switch self.documents[indexPath.row].mimeType {
            case "image/jpeg":
                self.performSegue(withIdentifier: "detailToPhoto", sender: cell)
            case "application/pdf":
                self.performSegue(withIdentifier: "toPdfViewer", sender: cell)
            default:
                print ("[NC!!] Error the mimeType of the document is not valid. Current value is: \(self.documents[indexPath.row].mimeType)")
                return
            }
                
            //deleteButton.isEnabled = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 2.0
        cell?.layer.borderColor = UIColor.clear.cgColor
        

    }
    
 
     // MARK: - Image picker functions
    
    func imagePickerController( _ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        // Local variable inserted by Swift 4.2 migrator.
        var imageFileName : String? = nil
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
            else {
                print("[NC!!] Problem picking edited image")
                dismiss(animated: true, completion: nil)
                return
        }
        
        guard let imageResizedAndCompressed = image.resizeAndCompressImageToJpeg(image: image, quality: nil, reSizeTo: nil)
            else {
                dismiss(animated: true, completion: nil)
                return }
        
        if let url = info[UIImagePickerController.InfoKey.imageURL.rawValue] as? URL {
            imageFileName = url.lastPathComponent
        }
        
        guard self.completeDocumentsPath != nil else {
            print ("[NC!!] completeDocumentsPath is nil because the communication reference is not set")
            dismiss(animated: true, completion: nil)
            NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Upload Error", message: "You must select done and connect to the internet in order to upload a document.", vc: self)
            
            return
        }
        
        /*
        let imageToUpload = fileToUpload(fileData: imageResizedAndCompressed, metadataContentType: "image/jpeg", documentType: Int.init(5), filename: imageFileName, collectionPath: nil, communityId: self.communication.communityId)
        self.filesToUpload.append(imageToUpload)
        */
        let hud = JGProgressHUD()
        hud.show(in: self.view, animated: true)
        
        NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: imageResizedAndCompressed, metadataContentType: "image/jpeg", documentType: Int.init(5), fileName: imageFileName , collectionRef: Firestore.firestore().collection(completeDocumentsPath), communityId: self.communication.communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
            if error != nil {
                hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                hud.dismiss(afterDelay: 2, animated: true)
                NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
            } else {
                print ("[NC] Document created correctly! with id: \(String(describing: documentRef))")
                hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                hud.dismiss(afterDelay: 2, animated: true)
            }
        })
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    

    
     // MARK: - Quicklook datasource protocol functions -> We can't use it, because it needs a URL of the document and the documents are in memory and not in local storage, so we don't have URL
    
    /*
    
    func numberOfPreviewItemsInPreviewController(controller: QLPreviewController) -> Int {
        return self.communication.documents.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.communication.documents[index].fileData.url
    }
    */
    
    // MARK: - Document picker functions
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("Document picker import URL : \(myURL)")
        do {
            let hud = JGProgressHUD()
            hud.show(in: self.view, animated: true)
         // inUrl is the document's URL
            var documentData = try Data(contentsOf: myURL) // Getting file data here
            var documentMimeType = myURL.mimeType()
            let fileName = myURL.lastPathComponent
            // If it is an image, we compress it
            let fileExtension = myURL.pathExtension
            let imageFileExtensions = ["jpg", "png", "gif", "jpeg", "bmp"]
            if imageFileExtensions.contains(fileExtension) {
                guard let fileImage = UIImage(data: documentData)
                    else {
                        print ("[NC!!]: Error obtaining UIImage from file ")
                        hud.dismiss(animated: true)
                        return
                }
                if let compressedAndResizedImage = fileImage.resizeAndCompressImageToJpeg(image: fileImage, quality: nil, reSizeTo: nil) {
                    documentData = compressedAndResizedImage
                    documentMimeType = "image/jpeg"
                }
            }
            
            guard self.completeDocumentsPath != nil else {
                print ("[NC!!] completeDocumentsPath is nil because the communication reference is not set")
                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Uploading Error", message: "You must select done and connect to the internet in order to upload a document.", vc: self)
                hud.dismiss(animated: true)
                return
            }
            
            
            
            NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: documentData, metadataContentType: documentMimeType, documentType: Int.init(5), fileName: fileName , collectionRef: Firestore.firestore().collection(completeDocumentsPath), communityId: self.communication.communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
                    if error != nil {
                        hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
                        hud.dismiss(animated: true)
                        NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
                    } else {
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
                        hud.dismiss(afterDelay: 1, animated: true)
                        print ("[NC] Document created correctly!")
                    }
            })
        } catch {
            print("[NC!!] Document data loading error")
            NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error, vc: self)
        }
    }
    
    func saveDocumentsAttached () {
        
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("[NC] Document picker view was cancelled by the user")
                dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier
        {
            switch identifier
            {
                
            case "detailToPhoto":
                
                let cell = sender as! NCDocumentCollectionViewCell
                let destinationVC = segue.destination as! NCPhotoVC
                if let index = self.collectionDocumentView.indexPath(for: cell) {
                    if let fileData = self.documents[index.row].fileData, let image = UIImage(data: fileData) {
                        destinationVC.imageToPresent = image
                    } else {
                        print ("[NC!!] Error obtaining Image from data")
                        NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting image from data", vc: self)
                    }
                }
                
            case "toPdfViewer":
                print("going to pdfviewer")
                let cell = sender as! NCDocumentCollectionViewCell
                let destinationVC = segue.destination as!NCPDFVC
                if let index = self.collectionDocumentView.indexPath(for: cell) {
                    if let fileData = self.documents[index.row].fileData {
                        destinationVC.pdfDocument = PDFDocument.init(data: fileData)
                    } else {
                        print ("[NC!!] Error obtaining pdf data")
                        NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting pdf from data", vc: self)
                    }
                }
            default: return
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
