//
//  NCNewCollectionViewCell.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 04/01/2019.
//  Copyright © 2019 Nutcoders. All rights reserved.
//

import UIKit

class NCDocumentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var docNameLabel: UILabel!
    @IBOutlet weak var docCreatorLabel: UILabel!
    @IBOutlet weak var docCreationDate: UILabel!
    
    
}
