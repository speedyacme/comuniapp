//
//  NcStartVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 7/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit

class NCStartRegistration: UIViewController {

    @IBOutlet var startButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // First check if we are logged in
        if let name = NCModelAPI.sharedInstance.getMyUserName() {
            //We are logged in, waiting to be move to next screen
            print ("[NC] Logged in Firebase with name \(name)")
            NCGUI.showTabBarVC(tabBarId: "mainTabBarController", tabToShowIndex: 0)
        } else {
            // We are not logged in
            print ("[NC] Not logged in Firebase")
            startButton.isHidden = false
            startButton.setBordersSettings(borderColor: UIColor.white, borderWidth: 1.0, cornerRadius: 5.0)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func createButtonPushed(_ sender: NcButtonBordered) {
        
        self.performSegue(withIdentifier: "segueToPrivacyTerms", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
