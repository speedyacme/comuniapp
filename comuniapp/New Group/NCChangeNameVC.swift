//
//  NCChangeNameVC.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 28/12/16.
//  Copyright © 2016 Nutcoders. All rights reserved.
//

import UIKit


class NCChangeNameVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var continueButton: UIButton!
    @IBOutlet var userNickNameTextField: UITextField!
    var nickName : String?
    override func viewDidLoad() {
        super.viewDidLoad()

        continueButton.isHidden = true
        userNickNameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Let's make appear the keyboard
        userNickNameTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        @IBAction func userNickNameDidEndEditing(_ sender: UITextField) {
        
        continueButton.isHidden = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }

    
    @IBAction func continueButtonPressed2(_ sender: UIButton) {
        
        if let myName = userNickNameTextField.text {
            
            if let error = NCModelAPI.sharedInstance.updateMyUserName(name: myName) {
                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error updating user name", error: error, vc: self)
                self.continueButton.isHidden = true
                NCAppStartProcess.startProcess()
            } else {
                //NCGUI.showTabBarVC(tabBarId: "mainTabBarController", tabToShowIndex: 0)
                self.continueButton.isHidden = true
                NCAppStartProcess.startProcess()
            }
        }
    }
}
