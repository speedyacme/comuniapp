//
//  NCEditNameVC.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 30/12/16.
//  Copyright © 2016 Nutcoders. All rights reserved.
//

import UIKit


protocol editNameVCDelegateProtocol {
    func nameChanged (nameEdited: String?)
}

class NCEditNameVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet var editNameTextField: UITextField!
    var name : String?
    var delegate : editNameVCDelegateProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editNameTextField.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        editNameTextField.text = self.name
        self.editNameTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editNameTextFieldEditChanged(_ sender: UITextField) {
        name = sender.text
    }
    @IBAction func editNameTextFieldDidEndOnExit(_ sender: UITextField) {
        name = sender.text
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if let myName = self.name {
            if let error = NCModelAPI.sharedInstance.updateMyUserName(name: myName) {
                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error updating Name", error: error, vc: self)
            } else {
                self.delegate.nameChanged(nameEdited: myName)
            }
        }
    }

    
    
}
