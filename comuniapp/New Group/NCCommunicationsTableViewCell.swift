//
//  NCCommunicationsTableViewCell.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 18/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit

class NCCommunicationsTableViewCell: UITableViewCell {

    @IBOutlet weak var shortDescriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var communityLabel: UILabel!
    @IBOutlet weak var userDisplayName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor (named: "NCAppBackgroundColor")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
