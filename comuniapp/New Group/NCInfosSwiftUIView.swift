//
//  NCInfosSwiftUIView.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 7/4/23.
//  Copyright © 2023 Nutcoders. All rights reserved.
//

import SwiftUI
import FirebaseFirestoreSwift
import Firebase

@available(iOS 13.0, *)
struct NCInfosSwiftUIView: View {
    @State var infos = [NCInfo]()
    //@State var infos = [NCInfo(id: "1", description: "Bomberos", phone: "+34656160743", type: 1, communityId: "444"), NCInfo(id: "2", description: "Policía", phone: "+34656160743", type: 1, communityId: "444")]
    //@State var userCommunities = [NCCommunity]()
    
    var body: some View {
        NavigationView {
            List(infos) { info in
                HStack {
                    VStack(alignment: .leading) {
                        Text(info.description)
                            .font(.headline)
                            .padding(5)
                        Text(info.phone)
                            .font(.subheadline)
                    }
                    Spacer()
                    Button(action: {
                        guard let url = URL(string: "tel://\(info.phone)") else { return }
                        UIApplication.shared.open(url)
                    }) {
                        Image(systemName: "phone.fill")
                            .imageScale(.large)
                            .scaleEffect(1.5) // Scale the image by a factor of 2
                            .foregroundColor(.blue)
                    }
                }
            }
            .navigationBarTitle("Informations")
        }
        .onAppear(){
            //initializeFirebase()
            fetchDocuments()
        }
        //self.userCommunitiesIds = self.userCommunities.compactMap ({$0.communityId})
    }
    
    private func initializeFirebase() {
            FirebaseApp.configure()
            //var db = Firestore.firestore()
        }
    private func fetchDocuments() {
        let userCommunitiesIds = NCModelAPI.sharedInstance.getUserCommunitiesFromLocalMemory().compactMap({$0.communityId})
        let newListener = NCModelAPI.sharedInstance.getCollectionGroupCodableDocumentsListener(collectionGroupId: "informations", communityIds: userCommunitiesIds, orderField: constants.fireStore.issues.openingDate, descending: false, limit: 25, classType: NCInfo.self, blockAfterGettingDocuments: { informations, err in

                    if let err = err {
                        print("[NC!!] Error getting infos: \(err.localizedDescription)")
                        //if let indicator = self.activityIndicatorView {
                          //  indicator.stopAnimating()
                        //}
                        //NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error downloading infos", comment: ""), error: err, vc: self)
                    } else {
                        //self.pullToRefreshControl.endRefreshing()
                        //if let indicator = self.activityIndicatorView {
                          //  indicator.stopAnimating()
                        //}
                        guard let informations = informations
                            else { return }
                        self.infos = informations
                    }
        })
    }
}

@available(iOS 13.0, *)
struct NCInfosSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView{
                NCInfosSwiftUIView()
            }
            .previewDisplayName("Español")
            .environment(\.locale, .init(identifier: "es"))
            
            NavigationView{
                NCInfosSwiftUIView()
            }
            .previewDisplayName("Inglés")
            .environment(\.locale, .init(identifier: "en"))
            
            NavigationView{
                NCInfosSwiftUIView()
            }
            .previewDisplayName("Catalán")
            .environment(\.locale, .init(identifier: "ca-ES"))
        }
    }
}
