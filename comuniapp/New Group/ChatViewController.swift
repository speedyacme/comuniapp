
import UIKit
import Photos
import Firebase
import MessageKit
import FirebaseFirestore
import InputBarAccessoryView
import MobileCoreServices
import FirebaseStorage


final class ChatViewController: MessagesViewController, UIDocumentPickerDelegate {
  
    private let db = Firestore.firestore()
    private var messagesCollectionReference: CollectionReference?
    private let storage = Storage.storage().reference()
    var messages: [NCMessage] = []
    private var messageListener: ListenerRegistration?
    private var isSendingPhoto = false {
        didSet {
          DispatchQueue.main.async {
            self.messageInputBar.leftStackViewItems.forEach { item in
               // item.isSendingPhoto = !item.isSendingPhoto
                print("hola")
            }
          }
        }
    }
    var userCommunityId : String? {
        didSet {
            if let _ = userCommunityId, let path = issue?.docRef?.path {
                self.messagesCollectionReference = db.collection([path, constants.fireStore.messages.collectionName ].joined(separator: "/"))
                self.viewDidLoad()
            }
        }
    }
    var issue : NCIssue? {
        didSet {
            if self.isViewLoaded && issue != nil {
                self.userCommunityId = issue!.communityId
                title = issue!.title
            }
        }
    }
    // MARK: - Private properties
   private let formatter: DateFormatter = {
       let formatter = DateFormatter()
       formatter.dateStyle = .short
       formatter.timeStyle = .short
       return formatter
   } ()
    
    
    deinit {
    messageListener?.remove()
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // First we configure the data source and delegate
        self.configureDataSourceAndDelegate()
        
        // Second we configure the message input bar
        self.configureMessageInputBar()
        
        // Here we configure the padding of the message cell
        self.configureMessageCollectionView()

        guard NCModelAPI.sharedInstance.getMyPhoneNumber() != nil
            else {
                navigationController?.popViewController(animated: true)
                print("[NC!!] Error: UserId is nil")
                return
        }
        guard self.issue?.docRef?.documentID != nil
            else {
                navigationController?.popViewController(animated: true)
                print("[NC!!] Error: Issue Document ID is nil")
                return
        }
        guard let issuePath = self.issue?.docRef?.path
        else {
                navigationController?.popViewController(animated: true)
                print("[NC!!] Error: Issue Path is nil")
                return
        }

        let messagesCollectionPath =  [issuePath, constants.fireStore.messages.collectionName ].joined(separator: "/")
        self.messagesCollectionReference = db.collection(messagesCollectionPath)

        guard self.messagesCollectionReference != nil
            else {
                navigationController?.popViewController(animated: true)
                print("[NC!!] Error: messagesCollection Path is nil")
                return
        }

        messageListener = self.messagesCollectionReference!.addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
            print("Error listening for conversation updates: \(error?.localizedDescription ?? "No error")")
            return
            }
            print ("[NC] Snapshot documentchanges: \(snapshot.documentChanges)")
            snapshot.documentChanges.forEach { change in
            self.handleDocumentChange(change)
          }
        }
            
    }
    // This is important in order to show input bar when in a subview
    override func viewDidAppear(_ animated: Bool) {
        self.becomeFirstResponder()
    }
   
    // MARK: - Message datasource and delegate configuration
    func configureDataSourceAndDelegate() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
    }
    
   // MARK: - Message input Bar configuration
    func configureMessageInputBar () {
      navigationItem.largeTitleDisplayMode = .never
        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = UIColor.lightGray
        messageInputBar.sendButton.setTitleColor(UIColor(named: "MainAppBlueColor"), for: .normal)
        messageInputBar.inputTextView.placeholder = NSLocalizedString("Write a message", comment: "")
        messageInputBar.delegate = self
        messageInputBar.leftStackView.alignment = .center
        messageInputBar.setLeftStackViewWidthConstant(to: 50, animated: false)
        
        let fileItem = InputBarButtonItem(type: .system) // 1
        fileItem.tintColor = UIColor(named: "MainAppBlueColor")
        fileItem.image = UIImage(named: "icons8-plus_math")
        fileItem.addTarget(
          self,
          action: #selector(addDocumentButtonPushed), // 2
          for: .primaryActionTriggered
        )
        fileItem.setSize(CGSize(width: 60, height: 30), animated: false)
        messageInputBar.setStackViewItems([fileItem], forStack: .left, animated: false) // 3
    }
    
    // MARK: - Message padding configuration
    
    func configureMessageCollectionView() {
        
        let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout
        layout?.sectionInset = UIEdgeInsets(top: 1, left: 8, bottom: 1, right: 8)
        
        // Hide the outgoing avatar and adjust the label alignment to line up with the messages
        layout?.setMessageOutgoingAvatarSize(.zero)
        layout?.setMessageOutgoingMessageTopLabelAlignment(LabelAlignment(textAlignment: .right, textInsets: UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 12)))
        layout?.setMessageOutgoingMessageBottomLabelAlignment(LabelAlignment(textAlignment: .right, textInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)))

        // Set outgoing avatar to overlap with the message bubble
        layout?.setMessageIncomingMessageTopLabelAlignment(LabelAlignment(textAlignment: .left, textInsets: UIEdgeInsets(top: 0, left: 12, bottom: 8, right: 0)))
        layout?.setMessageIncomingAvatarSize(CGSize(width: 0, height: 0))
        layout?.setMessageIncomingMessagePadding(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        layout?.setMessageIncomingAccessoryViewSize(CGSize(width: 30, height: 30))
        layout?.setMessageIncomingAccessoryViewPadding(HorizontalEdgeInsets(left: 8, right: 0))
        layout?.setMessageIncomingAccessoryViewPosition(.messageBottom)
        layout?.setMessageOutgoingAccessoryViewSize(CGSize(width: 30, height: 30))
        layout?.setMessageOutgoingAccessoryViewPadding(HorizontalEdgeInsets(left: 0, right: 8))
    }
  
  // MARK: - Actions
  
    @objc func addDocumentButtonPushed() {
        // We first show a action menu to choose photos or documents
        let message = ""
        let alert = UIAlertController(title: "Select one", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        let photoAction = UIAlertAction(title: "Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.addPhoto()
        })
        alert.addAction(photoAction)
        let otherDocumentAction = UIAlertAction(title: "PDF Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.pickDocument()
        })
        alert.addAction(otherDocumentAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.parent!.present(alert, animated: true, completion: nil)
    }
    
    /* Tenemos que desarrollar esto para elegir el tipo de documento.
    func selectDocumentType() {
        // We first show a action menu to choose photos or documents
        let message = ""
        let alert = UIAlertController(title: "Select Document type", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        alert.isModalInPopover = true
        let photoAction = UIAlertAction(title: "Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.addPhoto()
        })
        alert.addAction(photoAction)
        let otherDocumentAction = UIAlertAction(title: "PDF Document", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.pickDocument()
        })
        alert.addAction(otherDocumentAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        self.parent!.present(alert, animated: true, completion: nil)
    }*/
    
        
    
    func addDocument () {
           self.pickDocument()
       }

    func pickDocument () {
    //launch document picker
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeJPEG)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func addPhoto () {
        // launch image picker
        let picker = NCPhotoPicker.init(delegate: self)
        picker.launchImagePicker()
    }
    /*func addPhoto () {
        let picker = UIImagePickerController()
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
          picker.sourceType = .camera
        } else {
          picker.sourceType = .photoLibrary
        }
        present(picker, animated: true, completion: nil)
    }*/
    
    // MARK: - Document picker functions
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("Document picker import URL : \(myURL)")
        do {
         // inUrl is the document's URL
            var documentData = try Data(contentsOf: myURL) // Getting file data here
            var documentMimeType = myURL.mimeType()
            let fileName = myURL.lastPathComponent
            // If it is an image, we compress it
            let fileExtension = myURL.pathExtension
            let imageFileExtensions = ["jpg", "png", "gif", "jpeg", "bmp"]
            if imageFileExtensions.contains(fileExtension) {
                guard let fileImage = UIImage(data: documentData)
                    else {
                        print ("[NC!!]: Error obtaining UIImage from file ")
                        return
                }
                if let compressedAndResizedImage = fileImage.resizeAndCompressImageToJpeg(image: fileImage, quality: nil, reSizeTo: nil) {
                    documentData = compressedAndResizedImage
                    documentMimeType = "image/jpeg"
                }
            }
            
            guard let communityId = self.issue?.communityId
            else {
                print ("[NC!!]: CommunityId is nil ")
                return
            }
            guard let messageDocumentsPath = self.messagesCollectionReference?.parent?.path
            else {
                print ("[NC!!]: documents reference is nil ")
                return
            }
            
            guard self.issue != nil
            else {
                print ("[NC!!]: Issue is nil ")
                return
            }
            
                
            NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: documentData, metadataContentType: documentMimeType, documentType: Int.init(1), fileName: fileName , collectionRef:Firestore.firestore().collection(messageDocumentsPath + "/documents"),  communityId: communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
                        if error != nil {
                            NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
                        } else {
                            print ("[NC] Document created correctly!")
                            guard let documentRef = documentRef else {
                                return
                            }
                            guard let userId = NCModelAPI.sharedInstance.getMyUserId() else {
                                return
                            }
                            // Now we created the NCMessage document
                            let message = NCMessage.init(documentRef: documentRef, timeStamp: Timestamp.init(date: Date()), userId: userId, username: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), text: fileName, fileReference: documentRef, fileStoragePath: metadata?.path)
                            
                            // Now we insert the message locally before uploading to firebase to update faster the UI, when the message will be uploaded to firebase and we received the listener call we will remove the latest message to avoid duplications
                            guard let messagesCollectionPath = self.messagesCollectionReference?.path else
                            {
                                print("[NC] Message couldn't be uploaded because messagesCollectionReference is nil")
                                return
                            }
                            NCModelAPI.sharedInstance.addDocument(documentDictionary: message.dictionary, collectionPath: messagesCollectionPath, blockAfterCreatingDocument: { err, docref in
                                if err != nil {
                                  print("[NC!!] Error sending message: \(err!.localizedDescription)")
                                  return
                                } else {
                                    self.messagesCollectionView.scrollToLastItem(animated: true)
                                    guard let currentIssue = self.issue,  let issueRef = currentIssue.docRef else {return}
                                    //NCFirebaseCloudMessagingAPI.sendMessage(title: currentIssue.title, message: fileName, topic: issueRef.documentID, type: .chat_message_file, firebaseReferencePath: issueRef.path, senderPhoneNumber: NCModelAPI.sharedInstance.getMyPhoneNumber()! ,completion: nil)
                                    NCFirebaseCloudMessagingAPI.subscribeToTopic(issueRef.documentID)
                                }
                                
                            })
                        }
                })
            } catch {
                print("[NC!!] Document data loading error")
            }
        }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
            documentPicker.delegate = self
            present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
                print("[NC] Document picker view was cancelled by the user")
                dismiss(animated: true, completion: nil)
    }
    
    
  
  // MARK: - InsertMessage
  
  private func insertNewMessage(_ message: NCMessage) {
    guard !messages.contains(message) else {
      return
    }
    
    // Now to avoid repeating the image we put first locally to update faster the UI, we check if the new image is equal the previous one and if so, we remove the last message.
    if let imageData1 = self.messages.last?.messageMediaItem?.image?.pngData() , let imageData2 = message.messageMediaItem?.image?.pngData() {
        if imageData1 == imageData2 {
            self.messages.removeLast()
        }
    }
    messages.append(message)
    messages.sort()
    
    let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
    let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
    
    messagesCollectionView.reloadData()
    
    if shouldScrollToBottom {
      DispatchQueue.main.async {
        self.messagesCollectionView.scrollToLastItem(animated: true)
      }
    }
  }
  
  private func handleDocumentChange(_ change: DocumentChange) {
    
    guard var dataDictionary = change.document.data() as [String:Any]? else {
        print ("[NC!!] Message Change Snapshot dictionary seems empty...")
        return
    }
    //Now we add the document reference
    dataDictionary [constants.fireStore.generic.documentReference] = change.document.reference
    
    
    guard var message = NCMessage.init(dictionary: dataDictionary) else {
        print ("[NC!!] Message Change Snapshot dictionary seems empty...")
        return
    }
    
    switch change.type {
    case .added:
         if let fileDocRef = message.messageFile { // it's a media message
            // first we look if the file is stored locally
            if let imageDataStoredLocally = NCModelAPI.sharedInstance.getFileDataFromLocalDevice(fileNameWithExtension: message.messageText, inStorageType: .fileSystem), let imageStoredLocally = UIImage (data: imageDataStoredLocally) {
                
                let mediaitem = NCMessageMediaItem.init(image: imageStoredLocally)
                message.messageMediaItem = mediaitem
                message.kind = MessageKind.photo(mediaitem)
                self.insertNewMessage(message)
                
            } else { // The image isn't store locally so we need to get it from Firebase Storage
                NCModelAPI.sharedInstance.getDocument(pathRef: fileDocRef.path, block: { snapshot, error in
                    if let error = error {
                        print ("[NC!!] Getting File Document there is an error \(error)")
                    } else {
                        guard let storageFileReference = snapshot?[constants.fireStore.documents.fileStorageReferencePath] as? String else {
                            print ("[NC!!] Getting File from Storage that is nil")
                            return
                        }
                        NCModelAPI.sharedInstance.getStorageFile(path: storageFileReference, blockAfterGettingFile: { data, error in
                            if let error = error {
                                print ("[NC!!] Getting File Document there is an error \(error)")
                            } else {
                                if let data = data, let image = UIImage(data: data) {
                                    let mediaitem = NCMessageMediaItem.init(image: image)
                                    message.messageMediaItem = mediaitem
                                    message.kind = MessageKind.photo(mediaitem)
                                    self.insertNewMessage(message)
                                } else if data != nil { // Must be a file, not a photo
                                    let mediaitem = NCMessageMediaItem.init(image: UIImage.init(named: "icons8-pdf-1"))
                                    message.messageMediaItem = mediaitem
                                    message.kind = MessageKind.photo(mediaitem)
                                    message.messageFileStoragePath = storageFileReference
                                    self.insertNewMessage(message)
                                } else {
                                   print ("[NC!!] Data image from Storage File is nil")
                                }
                            }
                        })
                    }
                    
                })
            }
                        
         } else {
            self.insertNewMessage(message)
         }
    default:
      break
    }
  }
  
  private func downloadImage(at url: URL, completion: @escaping (UIImage?) -> Void) {
    let ref = Storage.storage().reference(forURL: url.absoluteString)
    let megaByte = Int64(1 * 1024 * 1024)
    
    ref.getData(maxSize: megaByte) { data, error in
      guard let imageData = data else {
        completion(nil)
        return
      }
      
      completion(UIImage(data: imageData))
    }
  }
  
}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate { // Here we are the custom configuration (see MessagesDisplayDelegate Protocol for more info)
  
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? UIColor(named: "MainAppBlueColor")! : UIColor.lightGray
    }

    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
    return true
    }

    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
      avatarView.isHidden = true
    }

    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return UIColor.white
    }
  
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
  
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
    }

    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    return 0
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 10
    }

    func cellBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 10
    }

    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        guard let myMessage = message as? NCMessage else {return 0}
        if myMessage.messageFileStoragePath != nil {
            return 20
        } else {
            return 0
        }
    }
    
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
    func currentSender() -> SenderType {
        let currentUser = NCMessageUser.init(senderId: NCModelAPI.sharedInstance.getMyPhoneNumber()!, displayName: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname())
        return currentUser
    }

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }


    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        guard let myMessage = message as? NCMessage else {return nil}
        if myMessage.messageFileStoragePath != nil {
            let docname = myMessage.messageText
            return NSAttributedString(string: docname, attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption1),
                .foregroundColor: UIColor(white: 0.3, alpha: 1)
            ])
        } else {
            return nil
        }
    }
  
   /*
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(
          string: name,
          attributes: [
            .font: UIFont.preferredFont(forTextStyle: .caption1),
            .foregroundColor: UIColor(white: 0.3, alpha: 1)
          ]
        )
    }*/
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        let dateString = formatter.string(from: message.sentDate)
        let topLabelText = name + " - " + dateString
        return NSAttributedString(
            string: topLabelText,
            attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption2),
                .foregroundColor: UIColor(white: 0.3, alpha: 1)
            ]
        )
    }
}

// MARK: - MessageInputBarDelegate
extension ChatViewController: InputBarAccessoryViewDelegate {

    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        
        let message = NCMessage.init(userId: NCModelAPI.sharedInstance.getMyPhoneNumber()!, username: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname() , text: text, creationDate: Date())
        print("[NC] Se ha escrito el mensaje \(text)")
        guard let messagesCollectionPath = self.messagesCollectionReference?.path else
        {
            print("[NC] Message couldn't be uploaded because path is collection path is nil \(text)")
            return
        }
        inputBar.sendButton.startAnimating()
        inputBar.inputTextView.placeholder = NSLocalizedString("Sending...", comment: "")
        NCModelAPI.sharedInstance.addDocument(documentDictionary: message.dictionary, collectionPath: messagesCollectionPath, blockAfterCreatingDocument: { err, docref in
            if err != nil {
              print("[NC!!] Error sending message: \(err!.localizedDescription)")
              return
            } else {
                inputBar.sendButton.stopAnimating()
                self.messagesCollectionView.scrollToLastItem(animated: true)
                inputBar.inputTextView.text = ""
                inputBar.inputTextView.placeholder = NSLocalizedString("Write a comment", comment: "")
                guard let currentIssue = self.issue,  let issueRef = currentIssue.docRef else {return}
                //NCFirebaseCloudMessagingAPI.sendMessage(title: currentIssue.title, message: text, topic: issueRef.documentID, type: .chat_message, firebaseReferencePath: issueRef.path, senderPhoneNumber: NCModelAPI.sharedInstance.getMyPhoneNumber()! ,completion: nil)
                NCFirebaseCloudMessagingAPI.subscribeToTopic(issueRef.documentID)
            }
        })
    }

    
    
    // TODO: The following are delegate functions of the Message input bar not mandatory that we are not using
        
    // func messageInputBar(_ inputBar: MessageInputBar, didChangeIntrinsicContentTo size: CGSize)
        
    //func messageInputBar(_ inputBar: MessageInputBar, textViewTextDidChangeTo text: String)
}





// MARK: - UIImagePickerControllerDelegate

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    picker.dismiss(animated: true, completion: nil)
    
        // Local variable inserted by Swift 4.2 migrator.
        var imageFileName : String? = nil
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
           else {
               print("[NC!!] Problem picking edited image in ChatViewController")
               return
        }

        guard let imageResizedAndCompressed = image.resizeAndCompressImageToJpeg(image: image, quality: nil, reSizeTo: nil)
           else { return }

        if let url = info[UIImagePickerController.InfoKey.imageURL.rawValue] as? URL {
           imageFileName = url.lastPathComponent
        }
        guard let messagesCollectionPath = self.messagesCollectionReference?.path
            else {
                print("[NC!!] Error: messagesCollection Path is nil")
                return
        }

        guard var documentsCollectionPath = self.messagesCollectionReference?.parent?.path
            else {
                print("[NC!!] Error: messagesCollection Path is nil")
                return
        }
        
        // We have 2 collections under a issue document, 1st is documents with all the attached documents and 2nd is messages with all the messages. Each media message has a link to a document document
        documentsCollectionPath = documentsCollectionPath + "/documents"

        guard let UIImageResizedAndCompressed = UIImage.init(data: imageResizedAndCompressed) else {
            return
        }
        
        guard  self.issue != nil else {
            return
        }
        
        // Now we created the NCMessage document
        
        var message = NCMessage.init(userId: self.issue!.creatorUserId, username: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), photoUrl: nil, photoImage: UIImageResizedAndCompressed, fileReference: nil)
       
        // Now we insert the message locally before uploading to firebase to update faster the UI, when the message will be uploaded to firebase and we received the listener call we will remove the latest message to avoid duplications
        insertNewMessage(message)
        
       
        // First we created the document document
        NCModelAPI.sharedInstance.createAndSaveDocumentWithFileData(fileData: imageResizedAndCompressed, metadataContentType: "image/jpeg", documentType: Int.init(5), fileName: imageFileName , collectionRef: Firestore.firestore().collection(documentsCollectionPath) , communityId: self.issue!.communityId, blockAfterFileAndDocumentCreated: { documentRef, metadata, error in
            if error != nil {
               NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error creating document", comment: ""), error: error!, vc: self)
            } else {
                print ("[NC] Document created correctly!")
                guard let fileDocumentReference = documentRef else {
                    return
                }
               
                if let imageFileName = imageFileName {
                    message.messageText = imageFileName
                }
                message.messageFile = fileDocumentReference
                
                NCModelAPI.sharedInstance.addDocument(documentDictionary: message.dictionary, collectionPath: messagesCollectionPath, blockAfterCreatingDocument: { err, docref in
                    if err != nil {
                      print("[NC!!] Error sending message: \(err!.localizedDescription)")
                      return
                    } else {
                        self.messagesCollectionView.scrollToLastItem(animated: true)
                        guard let currentIssue = self.issue,  let issueRef = currentIssue.docRef else {return}
                        //NCFirebaseCloudMessagingAPI.sendMessage(title: currentIssue.title, message: imageName, topic: issueRef.documentID, type: .chat_message_file, firebaseReferencePath: issueRef.path, senderPhoneNumber: NCModelAPI.sharedInstance.getMyPhoneNumber()! ,completion: nil)
                        NCFirebaseCloudMessagingAPI.subscribeToTopic(issueRef.documentID)
                    }
                })

            }
        })
        dismiss(animated: true, completion: nil)
        self.isSendingPhoto = false
        
    }
        
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
  
}

    

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

// MARK: - MessageCellDelegate
extension ChatViewController: MessageCellDelegate {
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    
    func didTapImage(in cell: MessageCollectionViewCell) {
        print("Image tapped")
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else { return }
        guard let messagesDataSource = messagesCollectionView.messagesDataSource else { return }
        guard let message = messagesDataSource.messageForItem(at: indexPath, in: messagesCollectionView) as? NCMessage  else {return}
        switch message.kind {
            case .photo(let photoItem):
            //let url = URL.init(string: message.messageFileStoragePath)
            //let fileName = myURL.lastPathComponent
            //let fileExtension = myURL.pathExtension
            //let imageFileExtensions = ["jpg", "png", "gif", "jpeg", "bmp"]
            //if imageFileExtensions.contains(fileExtension) {
            //var documentData = try Data(contentsOf: myURL) // Getting file data here
            //var documentMimeType = myURL.mimeType()
            if let path = message.messageFileStoragePath {
                self.parent?.performSegue(withIdentifier: "issueDetailToPdfViewer", sender: path)
            } else {
                if let image = photoItem.image{
                    self.parent?.performSegue(withIdentifier: "issueDetailToPhoto", sender: image)
                }
            }
            default:
                break
            }        
    }
    
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        print("Top cell label tapped")
    }
    
    func didTapCellBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom cell label tapped")
    }
    
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        print("Top message label tapped")
    }
    
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }

    func didTapAccessoryView(in cell: MessageCollectionViewCell) {
        print("Accessory view tapped")
    }

}



