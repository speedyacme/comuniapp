//
//  NCDocumentCollectionSectionViewCell.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 01/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit

class NCDocumentCollectionSectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var docNameLabel: UILabel!
    @IBOutlet weak var docCreationDate: UILabel!
    var fileMimeType : String!
    
}

