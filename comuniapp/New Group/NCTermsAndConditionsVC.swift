//
//  NCTermsAndConditionsVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 19/9/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit
import WebKit

class NCTermsAndConditionsVC: UIViewController, WKUIDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = Bundle.main.url(forResource: "infos_legales", withExtension: "rtf")!
        let opts : [NSAttributedString.DocumentReadingOptionKey : Any] =
            [.documentType : NSAttributedString.DocumentType.rtf]
        let s = try! NSAttributedString(url: url, options: opts, documentAttributes: nil)
        self.textView.attributedText = s
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor(named: "MainAppBlueColor")
    }
    
    override func viewDidLayoutSubviews() {
        // This is to make a scroll of the view to show the top
        self.textView.setContentOffset(.zero, animated: false)
    }
    /*
    var webView: WKWebView!
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = Bundle.main.url(forResource: "infos_legales", withExtension: "html")
        webView.loadFileURL(url!, allowingReadAccessTo: url!)
        
    }*/
}

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


