//
//  NCNameEditorSwiftUIView.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 5/4/23.
//  Copyright © 2023 Nutcoders. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct NCNameEditorSwiftUIView: View {
    @Binding var name: String
    
    var body: some View {
        VStack() {
            TextField("Enter your name", text: $name)
                .padding()
                .background(Color.gray.opacity(0.2))
                .cornerRadius(8)
            Text ("This name will be shown to other users")
                .padding(.top, 10)
                .foregroundColor(Color.gray)
        }
        .padding()
        .padding(.top, 30)
        .navigationBarTitle(Text("Name"), displayMode: .inline)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .onDisappear {
            if let error = NCModelAPI.sharedInstance.updateMyUserName(name: name) {
                //NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error updating user name", error: error, vc: self)
                print ("[NC] Error updating user name \(error)")
            } else {
                //NCGUI.showTabBarVC(tabBarId: "mainTabBarController", tabToShowIndex: 0)
                //NCAppStartProcess.startProcess()
                print ("[NC] User name updated correctly: \(name)")
            }
        }
    }
}

/*
struct NCNameEditorSwiftUIView_Previews: PreviewProvider {
    @State var names = "Pepito"
    static var previews: some View {
        Group {
            NCNameEditorSwiftUIView(name: $names)
                .previewDisplayName("Español")
                .environment(\.locale, .init(identifier: "es"))
            
            NCNameEditorSwiftUIView(name: $names)
            .previewDisplayName("Inglés")
            .environment(\.locale, .init(identifier: "en"))
            
            NCNameEditorSwiftUIView(name: $names)
            .previewDisplayName("Catalán")
            .environment(\.locale, .init(identifier: "ca-ES"))

        }
    }
}*/
