//
//  NCPhotoVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 21/01/2019.
//  Copyright © 2019 Nutcoders. All rights reserved.
//

import UIKit

class NCPhotoVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var photoView: UIImageView!
    var imageToPresent : UIImage? {
        didSet {
            if self.isViewLoaded {
                self.updatePhotoViewer ()
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.photoView.image = imageToPresent
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoView
    }
    
    func updatePhotoViewer () {
        self.photoView.image = imageToPresent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
