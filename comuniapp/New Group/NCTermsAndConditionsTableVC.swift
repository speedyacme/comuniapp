//
//  NCTermsAndConditionsTableVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 9/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit

class NCTermsAndConditionsTableVC: UITableViewController {
    
    @IBOutlet var acceptButton: NcButtonBordered!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.acceptButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        //self.acceptButton.setTitleColor(UIColor.gray, for: UIControlState.disabled)
        
        //self.enableAcceptButton(enabled: false)
        //self.acceptButton.isEnabled = false
        
        self.acceptButton.isHidden = true
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "MainAppBlueColor")
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    @IBAction func acceptSwitch(_ sender: UISwitch) {
        if sender.isOn { self.acceptButton.isHidden = false}
        else { self.acceptButton.isHidden = true}
    }
    
    
    
    
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
