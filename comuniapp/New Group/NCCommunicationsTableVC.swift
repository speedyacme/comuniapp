//
//  NCCommunicationsTableVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 16/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import AppTrackingTransparency


class NCCommunicationsTableVC: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    private var queryListeners = [ListenerRegistration]()
    let pullToRefreshControl = UIRefreshControl()
    var filterButton = UIBarButtonItem ()
    var db:Firestore! = Firestore.firestore()
    var userCommunities = [NCCommunity]()
    var userCommunitiesIds = [String]()
    var userCommunitiesIdsFiltered = [String]() {
        didSet { // We change the name of the navigation top bar based on community filtered
            if userCommunitiesIdsFiltered.count == 1{
                if let userCommunitySelected =  self.userCommunities.first(where: { $0.communityId == self.userCommunitiesIdsFiltered.first!})  {
                    self.navigationItem.prompt = userCommunitySelected.communityName
                    let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
                }
            } else {
                self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
                let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
            }
        }
    }
    var pickerViewRolled = false
    var communications = [NCCommunication]() {
        didSet
        {
            self.updateUI()
        }
    }
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    var newCommunicationCommunityId : String?
    var newCommunicationCommunityName : String?
   
    
    func communicationEdited (communication: NCCommunication) {
        
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = NSLocalizedString("Communications", comment: "")
        self.userCommunities = NCModelAPI.sharedInstance.getUserCommunitiesFromLocalMemory()
        self.userCommunitiesIds = self.userCommunities.compactMap ({$0.communityId})
        self.userCommunitiesIdsFiltered = self.userCommunitiesIds
        //self.navigationController?.navigationBar.tintColor = UIColor(named: "MainAppBlueColor") // This affects only to buttons color on the navigation bar
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "MainAppBlueColor")!]
        self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
        // Add Refresh Control to Table View
        pullToRefreshControl.tintColor = UIColor(named: "MainAppBlueColor")
        pullToRefreshControl.attributedTitle = NSAttributedString (string:NSLocalizedString("Loading communications...", comment: ""))
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullToRefreshControl
        } else {
            tableView.addSubview(pullToRefreshControl)
        }
        // Configure Refresh Control
        pullToRefreshControl.addTarget(self, action: #selector(updateModel), for: .valueChanged)
        // end configuring refresh control
        NCModelAPI.sharedInstance.isUserAdmin(blockAfterAdminInfo: { isAdmin, error in
            if let error = error {
               print("[NC] Couldn't get user Admin info \(error.localizedDescription)")
            } else {
                if isAdmin {
                    let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPushed))
                    self.navigationItem.rightBarButtonItem = addButton
                }
            }
        })
        self.filterButton = UIBarButtonItem.init(image: UIImage(named: "icons8-filter"), landscapeImagePhone: UIImage(named: "icons8-filter"), style: .plain, target: self, action: #selector(self.filterButtonPushed))
        if self.userCommunities.count > 1 { self.navigationItem.leftBarButtonItem = filterButton } // If we have only one community, we don't need to show the filter button
        self.updateModel()
        self.requestPermission()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    
    func stopObserving () {
        for listener in self.queryListeners {
            listener.remove()
        }
        self.queryListeners.removeAll() // We reset the queryListeners
    }
    
    func updateUI () {
        if (self.userCommunitiesIds.count != self.userCommunitiesIdsFiltered.count) && self.userCommunitiesIds.count > 1 {
            self.filterButton.image = UIImage (named: "icons8-filter_filled")
        } else {
            self.filterButton.image = UIImage (named: "icons8-filter")
        }
        self.tableView.reloadData()
    }
    
    func runActivityIndicator () {
        // See if it is animating.
        if activityIndicatorView.isAnimating {
            // If currently animating, stop it.
            activityIndicatorView.stopAnimating()
        }
        else {
            // Begin.
            activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivityIndicator () {
        activityIndicatorView.stopAnimating()
    }
    
    
    @objc func addButtonPushed () {
        
        self.launchCommunityPickerforNewCommunication()
    }
    
    func launchCommunityPickerforNewCommunication () {
        print ("[NC--]******* Creating new communication ********")
        print ("[NC--] ******************************")
        
        guard let userId = NCModelAPI.sharedInstance.getMyPhoneNumber() else {
            print ("[NC!!] Error creating communication, UserId is nil")
            return
        }
        
        switch userCommunities.count {
        
            case let numberOfUserCommunities where numberOfUserCommunities == 1: // we don't need to show community picker
                newCommunicationCommunityId = userCommunities.first!.communityId
                newCommunicationCommunityName = userCommunities.first!.communityName
                guard let id = newCommunicationCommunityId else {
                    print ("[NC!!] Error creating communication, community id is nil")
                    return
                }
                
            let newCommunication = NCCommunication (docRef: nil, communityId: id, shortDescription: "", longDescription: "", creatorUserId: userId, creatorNickName: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), date: Timestamp.init(date: Date()), documents: [NCDocument]())
                self.performSegue(withIdentifier: "segueToCommunicationCreator", sender: newCommunication)
                
            
            case let numberOfUserCommunities where numberOfUserCommunities > 1 :  // we need to show communitiy picker
                
                    let message = "\n\n\n\n\n\n"
                    let alert = UIAlertController(title: "Please Select Community for publishing", message: message, preferredStyle: UIAlertController.Style.actionSheet)
                    alert.isModalInPopover = true
                    let screenWidth = self.view.frame.size.width
                    let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: screenWidth - 20, height: 140)) // CGRectMake(left, top, width, height) - left and top are like margins
                    pickerFrame.tag = 1 // this will tell us if that this specific picker is to select one community to make a new communication
                    //set the pickers datasource and delegate
                    pickerFrame.delegate = self
                    
                    //Add the picker to the alert controller
                    alert.view.addSubview(pickerFrame)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        if self.newCommunicationCommunityId == nil { // We didn't roll the picker
                            self.newCommunicationCommunityName = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityName
                            self.newCommunicationCommunityId = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityId
                        }
                        guard let id = self.newCommunicationCommunityId else {
                            print ("[NC!!] Error creating communication, community id is nil")
                            return
                        }
                        
                        let newCommunication = NCCommunication (docRef: nil, communityId: id, shortDescription: "", longDescription: "", creatorUserId: userId, creatorNickName: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), date: Timestamp.init(date: Date()), documents: [NCDocument]())
                        self.performSegue(withIdentifier: "segueToCommunicationCreator", sender: newCommunication)

                    })
                    alert.addAction(okAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    self.parent!.present(alert, animated: true, completion: nil)
            
            default:
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities assigned", comment: ""), message: NSLocalizedString("You must be registered at least in one community", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.communications.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "communicationsCell", for: indexPath) as! NCCommunicationsTableViewCell
        let communicationsItem = communications[indexPath.row]
        cell.shortDescriptionLabel.text = communicationsItem.shortDescription
        cell.dateLabel.text = DateFormatter.localizedString(from: communicationsItem.date.dateValue(), dateStyle: .long, timeStyle: .short)
        cell.communityLabel.text = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: communicationsItem.communityId)
        cell.userDisplayName.text = communicationsItem.creatorNickName
        //cell.accessoryType = .disclosureIndicator
        //cell.tintColor = UIColor(named: "MainAppBlueColor")

        return cell
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier
        {
            switch identifier
            {
            case "segueToCommunicationDetail":
                let cell = sender as! UITableViewCell
                if let index = tableView.indexPath(for: cell) {
                    let destinationVC = segue.destination as! NCCommunicationDetailVC
                    print ("[NC--]******* Communication Selected ********")
                    print ("[NC--] Communication: \(communications[index.row].shortDescription) ")
                    print ("[NC--] ******************************")
                    destinationVC.communication = communications[index.row]
                    destinationVC.isEditing = false
                    if let currentPath = self.communications[index.row].docRef?.path {
                        destinationVC.partialPath = currentPath
                    }
                }
                
            case "segueToCommunicationCreator":
                let destinationVC = segue.destination as! NCCommunicationDetailVC
                destinationVC.isEditing = true
                destinationVC.communication = (sender as! NCCommunication)
                // Here we don't set the destinationVC.partialPath because we still don't know what is the path
                
            default: return
            }
        }
    }
    

    
    @objc func updateModel () {
        if self.userCommunitiesIdsFiltered.count != 0 {
            // Find communications in each community
            self.stopObserving()
            // Find communications in each of the customers community
            let newListener = NCModelAPI.sharedInstance.getCollectionGroupDocumentsListener(collectionGroupId: "communications", communityIds: self.userCommunitiesIdsFiltered, orderField: constants.fireStore.communications.date, descending: false, limit: 25, blockAfterGettingDocuments: { communicationsDictionaries, err in
                
                if let err = err {
                    print("[NC!!] Error getting communications: \(err.localizedDescription)")
                    self.pullToRefreshControl.endRefreshing()
                    if let indicator = self.activityIndicatorView {
                        indicator.stopAnimating()
                    }
                    NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error downloading communications", comment: ""), error: err, vc: self)
                } else {
                    self.pullToRefreshControl.endRefreshing()
                    if let indicator = self.activityIndicatorView {
                        indicator.stopAnimating()
                    }
                    guard let comsDicts = communicationsDictionaries
                        else { return }
                    let newCommunicationsFromCommunity = comsDicts.compactMap({
                        NCCommunication(dictionary: $0)
                    })
                    self.communications = newCommunicationsFromCommunity.sorted(by: { $0.date.dateValue() > $1.date.dateValue() })
                }
            })
            if newListener != nil {
                self.queryListeners.append(newListener!)
            }
        } else {
        // User doesn't have any community set -> he must talk to administrator
        NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities found", comment: ""), message: NSLocalizedString("Contact the community administrator to include you in the community", comment:""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
        }
    }
    
    
    @objc func filterButtonPushed () {
        if self.userCommunitiesIds.count > 1 { // show picker
            self.pickerViewRolled = false
            let message = "\n\n\n\n\n\n"
            let alert = UIAlertController(title: "Please Select Community for filter", message: message, preferredStyle: UIAlertController.Style.actionSheet)
            alert.isModalInPopover = true
            let screenWidth = self.view.frame.size.width
            let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 30, width: screenWidth - 20, height: 150)) // CGRectMake(left, top, width, height) - left and top are like margins
            pickerFrame.tag = 0 // this will tell us if that this specific picker is to filter one specific community or select "all communities" option
            //set the pickers datasource and delegate
            pickerFrame.delegate = self
            
            
            //Add the picker to the alert controller
            alert.view.addSubview(pickerFrame)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if !self.pickerViewRolled {
                    self.userCommunitiesIdsFiltered = [self.userCommunitiesIds[0]]
                }
                self.updateModel()
            })
            alert.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(cancelAction)
            self.parent!.present(alert, animated: true, completion: nil)
            if self.userCommunitiesIdsFiltered.count == 1 { // We have selected already a community
                if let indexCommunity = self.userCommunitiesIds.firstIndex(of: self.userCommunitiesIdsFiltered[0]) {
                pickerFrame.selectRow(indexCommunity, inComponent: 0, animated: true)
                }
            } else {
                pickerFrame.selectRow(self.userCommunitiesIds.count, inComponent: 0, animated: true)
            }
        } else {
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Only one community assigned", comment: ""), message: NSLocalizedString("To filter communities you must be registered at least in two communities", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            return
        }
    }
    
    // MARK: Picker View Delegate functions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let rows : Int
        
        switch pickerView.tag {
        case 0: // Filtering communicties, it has "all communities" option plus each individual community
            rows = self.userCommunitiesIds.count + 1
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            rows = userCommunities.count
        default:
            rows = 0
        }
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let title : String?
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            if row == self.userCommunities.count {
                title = NSLocalizedString("All communities", comment: "")
            } else {
                title = self.userCommunities[row].communityName
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            title = userCommunities[row].communityName
        default:
            title = nil
        }
        return title
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            self.pickerViewRolled = true
            if row == self.userCommunities.count {
                self.userCommunitiesIdsFiltered = self.userCommunitiesIds
            } else {
                self.userCommunitiesIdsFiltered = [userCommunities[row].communityId!]
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            self.newCommunicationCommunityId = userCommunities[row].communityId
            self.newCommunicationCommunityName = userCommunities[row].communityName
        default:
            return
        }
    }
    
    func requestPermission() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("NC Transparency permission Authorized")
                    // Now that we are authorized we can get the IDFA
                    //print(ASIdentifierManager.shared().advertisingIdentifier)
                case .denied:
                    // Tracking authorization dialog was
                    // shown and permission is denied
                    print("NC Transparency permission Denied")
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("NC Transparency permission Not Determined")
                case .restricted:
                    print("NC Transparency permission Restricted")
                @unknown default:
                    print("NC Transparency permission Unknown")
                }
            }
        }
    }
    
}
