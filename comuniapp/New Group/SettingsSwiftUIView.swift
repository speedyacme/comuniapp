
import SwiftUI
import UIKit
import MessageUI

@available(iOS 13.0, *)
struct SettingsSwiftUIView: View {
    
    @State var alertaSignOut: Bool = false
    @State private var isSharePresented: Bool = false
    @State var name : String
    
    init() {
        name = NCModelAPI.sharedInstance.getMyUserName() ?? ""
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section (header: Text("Account")){
                    HStack (alignment: .top){
                        NavigationLink {
                            NCNameEditorSwiftUIView(name: $name)
                        }
                        label: {
                            Image(systemName: "person.text.rectangle")
                            //.foregroundColor(Color("iconColor"))
                            Text(name)
                                .padding(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 0))
                        }
                    }
                    HStack (alignment: .top){
                    label: do {
                            Image(systemName: "iphone.homebutton")
                            //.foregroundColor(Color("iconColor"))
                            Text(NCModelAPI.sharedInstance.getMyPhoneNumber() ?? "")
                            .padding(.leading)
                        }
                    }
                    Button(action: {alertaSignOut = true}) {
                        HStack {
                            Spacer()
                            Text("Sign Out")
                                .foregroundColor(.red)
                            Image(systemName: "rectangle.portrait.and.arrow.right")
                                .foregroundColor(.red)
                            Spacer()
                        }
                    }
                    .alert(isPresented: $alertaSignOut) {
                        Alert(
                            title: Text("Signing Out"),
                            message: Text("Are you sure you want to Sign Out?"),
                            primaryButton: .default(Text("Cancel")),
                            secondaryButton: .destructive(Text("Sign Out"), action: signOut)
                        )
                    }
                }
                Section (header: Text("Notifications")){
                    Button(action: openInSettingsAppPushed) {
                        HStack {
                            Spacer()
                            Image(systemName: "gearshape")
                            Text("Open settings app")
                            Spacer()
                        }
                    }
                }
                Section (header: Text("Do you like it?")){
                    Button(action: {
                        let url = URL(string: "https://apps.apple.com/es/app/communyapp/id1513750080")!
                        let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                        UIApplication.shared.windows.first?.rootViewController?.present(activityVC, animated: true, completion: nil)
                    })
                    {
                        HStack {
                            Spacer()
                            Image(systemName: "square.and.arrow.up")
                            Text("Share with friends")
                            Spacer()
                        }
                    }
                }
                Section (header: Text("Support & contact")){
                    HStack (alignment: .top){
                        NavigationLink {
                            WebSUIV(url: URL(string: "https://communyapp.web.app")!)
                        }
                    label: {
                        Image(systemName: "questionmark.circle")
                        //.foregroundColor(Color("iconColor"))
                        Text("Help")
                    }
                    }
                    HStack (alignment: .top){
                        NavigationLink {
                            WebSUIV(url: URL(string: "https://communyapp.web.app")!)
                        }
                    label: {
                        Image(systemName: "link")
                        //.foregroundColor(Color("iconColor"))
                        Text("CommunyApp Website")
                    }
                    }
                    Button(action: {
                        EmailHelper.shared.sendEmail(subject: NSLocalizedString("CommunyApp Support", comment: ""), body: "\n\n\n\n\n\n\n\nApp Data:\nVersion: \(String(describing: Bundle.main.infoDictionary?["CFBundleShortVersionString"]))\nBundle: \(String(describing: Bundle.main.infoDictionary?["CFBundleVersion"]))", to: "communyapp.nutcoders@gmail.com")
                      }) {
                        HStack {
                            Spacer()
                            Image(systemName: "envelope")
                            Text("Send us feedback")
                            Spacer()
                        }
                    }
                }
                Section (header: Text("Legal")){
                    HStack (alignment: .top){
                        NavigationLink {
                            NCLegalTermsSwiftUIView(fileName: "infos_legales")
                        }
                    label: {
                        Image(systemName: "info.circle")
                        //.foregroundColor(Color("iconColor"))
                        Text("Legal terms and conditions")
                    }
                    }
                    HStack (alignment: .top){
                        NavigationLink {
                            WebSUIV(url: URL(string: "https://icons8.com")!)
                        }
                    label: {
                        Image(systemName: "info.circle")
                        //.foregroundColor(Color("iconColor"))
                        Text("Licenses")
                    }
                    }
                }
                Section (header: Text("App")){
                    HStack {
                        Image(systemName: "number")
                        //.foregroundColor(Color("iconColor"))
                        Text("Version: ")
                        Text(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Desconocido")
                            .foregroundColor(Color.gray)
                    }
                    HStack (alignment: .top){
                        Image(systemName: "cpu")
                        //.foregroundColor(Color("iconColor"))
                        Text("Build number: ")
                        Text(Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "Desconocido")
                            .foregroundColor(Color.gray)
                    }
                    HStack (alignment: .top){
                        Image(systemName: "c.circle")
                        //.foregroundColor(Color("iconColor"))
                        Text("Nutcoders S.L. 2022")
                    }
                }
            }
            .navigationBarTitle("Settings")
        }
    }
    
    func signOut () {
        // MARK: - SIGNOUT DE FIREBASE - This Logs Out of Firebase, for instance to enter with a different phone number
        if let error = NCModelAPI.sharedInstance.logOut() {
            print ("[NC!!] Error signing out: \(error)")
            //NCGUI.showErrorAlertOnCurrentVC(errorTitle: error.localizedDescription, error: error, vc: self)
        } else {
            // MARK: LOG OUT OK - GO TO REGISTER PROCESS AGAIN
            print ("[NC] Signed out going to startVC")
            NCGUI.showVC(viewControllerIdentifier: "startVC")
        }
    }
    
    func openInSettingsAppPushed() {
        guard let url = URL(string:UIApplication.openSettingsURLString) else {
            return //be safe
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        /*
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            
        }*/
    }

    
    struct ActivityViewController: UIViewControllerRepresentable {
        var activityItems: [Any]
        var applicationActivities: [UIActivity]? = nil
        func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityViewController>) -> UIActivityViewController {
            let controller = UIActivityViewController(activityItems: activityItems, applicationActivities: applicationActivities)
            return controller
        }
        func updateUIViewController(_ uiViewController: UIActivityViewController, context: UIViewControllerRepresentableContext<ActivityViewController>) {
            
        }
    }
}

@available(iOS 13.0, *)
struct SettingsSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView{
                SettingsSwiftUIView()
            }
            .previewDisplayName("Español")
            .environment(\.locale, .init(identifier: "es"))
            
            NavigationView{
                SettingsSwiftUIView()
            }
            .previewDisplayName("Inglés")
            .environment(\.locale, .init(identifier: "en"))
            
            NavigationView{
                SettingsSwiftUIView()
            }
            .previewDisplayName("Catalán")
            .environment(\.locale, .init(identifier: "ca-ES"))

        }
    }
}
