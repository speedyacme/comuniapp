//
//  NCIssueTableVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 16/07/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class NCIssueTableVC: UITableViewController,  UIPickerViewDataSource, UIPickerViewDelegate {
    
    private var queryListeners = [ListenerRegistration]()
    let pullToRefreshControl = UIRefreshControl()
    var filterButton = UIBarButtonItem ()
    var db:Firestore! = Firestore.firestore()
    var userCommunities = [NCCommunity]()
    var userCommunitiesIds = [String]()
    var userCommunitiesIdsFiltered = [String]() {
        didSet { // We change the name of the navigation top bar based on community filtered
            if userCommunitiesIdsFiltered.count == 1{
                if let userCommunitySelected =  self.userCommunities.first(where: { $0.communityId == self.userCommunitiesIdsFiltered.first!})  {
                    self.navigationItem.prompt = userCommunitySelected.communityName
                    let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
                }
            } else {
                self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
                let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
            }
        }
    }
    var pickerViewRolled = false
    var issues = [NCIssue]() {
        didSet
        {
            self.updateUI()
        }
    }
        @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    var newIssueCommunityId : String?
    var newIssueCommunityName : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Issues", comment: "")
        self.userCommunities = NCModelAPI.sharedInstance.getUserCommunitiesFromLocalMemory()
        self.userCommunitiesIds = self.userCommunities.compactMap ({$0.communityId})
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
        pullToRefreshControl.tintColor = UIColor(named: "MainAppBlueColor")
        pullToRefreshControl.attributedTitle = NSAttributedString (string:NSLocalizedString("Loading issues...", comment: ""))
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullToRefreshControl
        } else {
            tableView.addSubview(pullToRefreshControl)
        }
        // Configure Refresh Control
        pullToRefreshControl.addTarget(self, action: #selector(updateModel), for: .valueChanged)
        // end configuring refresh control
        self.userCommunitiesIdsFiltered = self.userCommunitiesIds
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPushed)) // We add the button to everybody, since everybody can create an issue.
        self.navigationItem.rightBarButtonItem = addButton
        self.filterButton = UIBarButtonItem.init(image: UIImage(named: "icons8-filter"), landscapeImagePhone: UIImage(named: "icons8-filter"), style: .plain, target: self, action: #selector(self.filterButtonPushed))
        if self.userCommunities.count > 1 { self.navigationItem.leftBarButtonItem = filterButton } // If we have only one community, we don't need to show the filter button
        self.updateModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func stopObserving () {
        for listener in self.queryListeners {
            listener.remove()
        }
        self.queryListeners.removeAll() // We reset the queryListeners
    }
    
    func updateUI () {
        if (self.userCommunitiesIds.count != self.userCommunitiesIdsFiltered.count) && self.userCommunitiesIds.count > 1 {
            self.filterButton.image = UIImage (named: "icons8-filter_filled")
        } else {
            self.filterButton.image = UIImage (named: "icons8-filter")
        }
        self.tableView.reloadData()
    }
    
    func runActivityIndicator () {
        // See if it is animating.
        if activityIndicatorView.isAnimating {
            // If currently animating, stop it.
            activityIndicatorView.stopAnimating()
        }
        else {
            // Begin.
            activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivityIndicator () {
        activityIndicatorView.stopAnimating()
    }
    
    @objc func addButtonPushed () {
        
        self.launchIssuePickerforNewIssue()
    }
    
    func launchIssuePickerforNewIssue () {
        print ("[NC--]******* Creating new issue ********")
        print ("[NC--] ******************************")
        
        guard let userId = NCModelAPI.sharedInstance.getMyPhoneNumber() else {
            print ("[NC!!] Error creating issue, UserId is nil")
            return
        }
        
        switch userCommunities.count {
        
            case let numberOfUserCommunities where numberOfUserCommunities == 1: // we don't need to show community picker
                newIssueCommunityId = userCommunities.first!.communityId
                newIssueCommunityName = userCommunities.first!.communityName
                guard let id = newIssueCommunityId else {
                    print ("[NC!!] Error creating Issue, community id is nil")
                    return
                }
            let newIssue = NCIssue.init(communityId: id, title: "", description: "", creatorUserId: userId, creatorNickname: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), openingDate: Timestamp.init(date: Date()), type: 7, status: 1) // Type = 7 es otros y Status 1 = Pending
                
                self.performSegue(withIdentifier: "segueToIssueCreator", sender: newIssue)
                
            
            case let numberOfUserCommunities where numberOfUserCommunities > 1 :  // we need to show communitiy picker
                
                    let message = "\n\n\n\n\n\n"
                    let alert = UIAlertController(title: "Please Select Community for publishing", message: message, preferredStyle: UIAlertController.Style.actionSheet)
                    alert.isModalInPopover = true
                    let screenWidth = self.view.frame.size.width
                    let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: screenWidth - 20, height: 140)) // CGRectMake(left, top, width, height) - left and top are like margins
                    pickerFrame.tag = 1 // this will tell us if that this specific picker is to select one community to make a new communication
                    //set the pickers datasource and delegate
                    pickerFrame.delegate = self
                    
                    //Add the picker to the alert controller
                    alert.view.addSubview(pickerFrame)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        if self.newIssueCommunityId == nil { // We didn't roll the picker
                            self.newIssueCommunityName = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityName
                            self.newIssueCommunityId = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityId
                        }
                        guard let id = self.newIssueCommunityId else {
                            print ("[NC!!] Error creating issue, community id is nil")
                            return
                        }
                        let newIssue = NCIssue.init(docRef: nil, communityId: id, title: "", description: "", creatorUserId: userId, creatorNickname: NCModelAPI.sharedInstance.getMyOwnerIdAndNickname(), openingDate: Timestamp.init(date: Date()),
                                                    startingDate: nil, resolutionDate: nil, closingDate: nil, documents: [NCDocument](), type: 7, status: 1) // Type = 7 es otros y Status 1 = Pending
                        self.performSegue(withIdentifier: "segueToIssueCreator", sender: newIssue)

                    })
                    alert.addAction(okAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    self.parent!.present(alert, animated: true, completion: nil)
            
            default:
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities assigned", comment: ""), message: NSLocalizedString("You must be registered at least in one community", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            }
    }
    
    
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.issues.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "issueCell", for: indexPath) as! NCIssueTableViewCell
        let issueItem = self.issues[indexPath.row]
        cell.issueTitleLabel.text = issueItem.title
        cell.openningDateLabel.text = DateFormatter.localizedString(from: issueItem.openingDate.dateValue(), dateStyle: .short, timeStyle: .short)
        cell.communityLabel.text = NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: issueItem.communityId)
        cell.creatorNameLabel.text = issueItem.creatorNickname
        cell.statusLabel.layer.masksToBounds = true
        cell.statusLabel.layer.borderWidth = 1.0
        cell.statusLabel.padding = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: 5)
        cell.statusLabel.layer.cornerRadius = 5
        switch issueItem.status {
        case 1:
            cell.statusLabel.text = NSLocalizedString("PENDING", comment: "")
            cell.statusLabel.textColor = UIColor.white
            cell.statusLabel.backgroundColor = UIColor.red
            cell.statusLabel.layer.borderColor = UIColor.red.cgColor
            cell.statusLabel.padding = UIEdgeInsets(top: 25, left: 5, bottom: 25, right: 5)
        case 2:
            cell.statusLabel.text = NSLocalizedString("ON COURSE", comment: "")
            cell.statusLabel.textColor = UIColor.white
            cell.statusLabel.backgroundColor = UIColor.orange
            cell.statusLabel.layer.borderColor = UIColor.orange.cgColor
        case 3:
            cell.statusLabel.text = NSLocalizedString("SOLVED", comment: "")
            cell.statusLabel.textColor = UIColor.white
            cell.statusLabel.backgroundColor = UIColor.systemGreen
            cell.statusLabel.layer.borderColor = UIColor.systemGreen.cgColor
        case 4:
            cell.statusLabel.text = NSLocalizedString("CLOSED", comment: "")
            cell.statusLabel.textColor = UIColor.white
            cell.statusLabel.backgroundColor = UIColor.lightGray
            cell.statusLabel.layer.borderColor = UIColor.lightGray.cgColor
        default:
            cell.statusLabel.text = NSLocalizedString("PENDING", comment: "")
            cell.statusLabel.textColor = UIColor.white
            //cell.statusLabel.backgroundColor = UIColor.red
            //cell.statusLabel.layer.borderColor = UIColor.red.cgColor
            
            //self.statusLabel.padding = UIEdgeInsets(top: 25, left: 5, bottom: 25, right: 5)
        }
        /*
        if issueItem.closingDate != nil {
            cell.statusLabel.text = NSLocalizedString("CLOSED", comment: "")
            cell.statusLabel.layer.borderColor = UIColor.lightGray.cgColor
            cell.statusLabel.textColor = UIColor.lightGray
        } else {
            if issueItem.resolutionDate != nil {
                cell.statusLabel.text = NSLocalizedString("SOLVED", comment: "")
                cell.statusLabel.layer.borderColor = UIColor.green.cgColor
                cell.statusLabel.textColor = UIColor.green
            } else {
                if issueItem.startingDate != nil {
                    cell.statusLabel.text = NSLocalizedString("ON COURSE", comment: "")
                    cell.statusLabel.layer.borderColor = UIColor.orange.cgColor
                    cell.statusLabel.textColor = UIColor.orange
                } else {
                    cell.statusLabel.text = NSLocalizedString("PENDING", comment: "")
                    cell.statusLabel.layer.borderColor = UIColor.red.cgColor
                    cell.statusLabel.textColor = UIColor.red
                }
            }
        }*/
        

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier
        {
            switch identifier
            {
            case "segueToIssueDetail":
                let cell = sender as! UITableViewCell
                if let index = tableView.indexPath(for: cell) {
                    let destinationVC = segue.destination as! NCIssueDetailVC
                    print ("[NC--]******* Issue Selected ********")
                    print ("[NC--] Issue: \(issues[index.row].title) ")
                    print ("[NC--] ******************************")
                    destinationVC.issue = issues[index.row]
                    destinationVC.isEditing = false
                    if let currentPath = self.issues[index.row].docRef?.path {
                        destinationVC.partialPath = currentPath
                    }
                }
                
            case "segueToIssueCreator":
                let destinationVC = segue.destination as! NCIssueDetailVC
                destinationVC.isEditing = true
                destinationVC.issue = (sender as! NCIssue)
            default: return
            }
        }
    }
    
    @objc func updateModel () {
        if self.userCommunitiesIdsFiltered.count != 0 {
            // Find communications in each community
            self.stopObserving()
            let newListener = NCModelAPI.sharedInstance.getCollectionGroupDocumentsListener(collectionGroupId: "issues", communityIds: self.userCommunitiesIdsFiltered, orderField: constants.fireStore.issues.openingDate, descending: false, limit: 25, blockAfterGettingDocuments: { issuesDictionaries, err in
                    
                        if let err = err {
                            print("[NC!!] Error getting issues: \(err.localizedDescription)")
                            self.pullToRefreshControl.endRefreshing()
                            if let indicator = self.activityIndicatorView {
                                indicator.stopAnimating()
                            }
                            NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error downloading issues", comment: ""), error: err, vc: self)
                        } else {
                            self.pullToRefreshControl.endRefreshing()
                            if let indicator = self.activityIndicatorView {
                                indicator.stopAnimating()
                            }
                            guard let issuesDicts = issuesDictionaries
                                else { return }
                            let newIssuesFromCommunity = issuesDicts.compactMap({
                                NCIssue(dictionary: $0)})
                            self.issues = newIssuesFromCommunity.sorted(by: {$0.openingDate.dateValue() > $1.openingDate.dateValue() })
                        }
            })
            if newListener != nil {
                self.queryListeners.append(newListener!)
            }
        } else {
            // User doesn't have any community set -> he must talk to administrator
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities found", comment: ""), message: NSLocalizedString("Contact the community administrator to include you in the community", comment:""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
        }
    }
    
    
    @objc func filterButtonPushed () {
        if self.userCommunitiesIds.count > 1 { // show picker
            self.pickerViewRolled = false
            let message = "\n\n\n\n\n\n"
            let alert = UIAlertController(title: "Please Select Community for filter", message: message, preferredStyle: UIAlertController.Style.actionSheet)
            alert.isModalInPopover = true
            let screenWidth = self.view.frame.size.width
            let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 30, width: screenWidth - 20, height: 150)) // CGRectMake(left, top, width, height) - left and top are like margins
            pickerFrame.tag = 0 // this will tell us if that this specific picker is to filter one specific community or select "all communities" option
            //set the pickers datasource and delegate
            pickerFrame.delegate = self
            
            
            //Add the picker to the alert controller
            alert.view.addSubview(pickerFrame)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if !self.pickerViewRolled {
                    self.userCommunitiesIdsFiltered = [self.userCommunitiesIds[0]]
                }
                self.updateModel()
                self.updateUI()
            })
            alert.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(cancelAction)
            self.parent!.present(alert, animated: true, completion: nil)
            if self.userCommunitiesIdsFiltered.count == 1 { // We have selected already a community
                if let indexCommunity = self.userCommunitiesIds.firstIndex(of: self.userCommunitiesIdsFiltered[0]) {
                pickerFrame.selectRow(indexCommunity, inComponent: 0, animated: true)
                }
            } else {
                pickerFrame.selectRow(self.userCommunitiesIds.count, inComponent: 0, animated: true)
            }
        } else {
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Only one community assigned", comment: ""), message: NSLocalizedString("To filter communities you must be registered at least in two communities", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            return
        }
    }
    
    
    
// MARK: Picker View Delegate functions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let rows : Int
        
        switch pickerView.tag {
        case 0: // Filtering communicties, it has "all communities" option plus each individual community
            rows = self.userCommunitiesIds.count + 1
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            rows = userCommunities.count
        default:
            rows = 0
        }
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let title : String?
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            if row == self.userCommunities.count {
                title = NSLocalizedString("All communities", comment: "")
            } else {
                title = self.userCommunities[row].communityName
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            title = userCommunities[row].communityName
        default:
            title = nil
        }
        return title
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            self.pickerViewRolled = true
            if row == self.userCommunities.count {
                self.userCommunitiesIdsFiltered = self.userCommunitiesIds
            } else {
                self.userCommunitiesIdsFiltered = [userCommunities[row].communityId!]
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            self.newIssueCommunityId = userCommunities[row].communityId
            self.newIssueCommunityName = userCommunities[row].communityName
        default:
            return
        }
    }
    
}
