//
//  NCPhotoVC.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 24/1/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//
// NC - his is a photo viewer with zoom capability and photo picker option

import UIKit


protocol NCPhotoUpdatedProtocol {
    func updatePhoto (photo: UIImage)
}

class NCPhotoViewerVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {
    
    
    
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet var eventNameButton: UIButton!
    
    
    var imageToPresent : UIImage?
    var imagePicked :UIImage?
    var delegate : NCPhotoUpdatedProtocol!
 
    var imageChanged = false
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if imageToPresent != nil { self.photoView.image = imageToPresent }
        else { self.photoView.image = #imageLiteral(resourceName: "defaultEventImage256") }
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        
        /* This is if we want to edit the photo
        let photoButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.camera, target: self, action: #selector(photoButtonPressed(_ :)))
        if self.eventPassed.owner?.username == NCModelAPI.sharedInstance.getMyUserAsOwner()?.username {
            self.navigationItem.rightBarButtonItems = [photoButton]
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if imageChanged { self.sendImageToDelegate() }
        imageChanged = false
        //self.tabBarController?.tabBar.isHidden = false
        
    }
    
    func applicationWillResignActive() {
        if imageChanged { self.sendImageToDelegate() }
        imageChanged = false
    }
  
    func sendImageToDelegate () {
        if imagePicked != nil { delegate.updatePhoto(photo: self.imagePicked!) }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoView
    }
    
    @objc func photoButtonPressed(_ sender: Any) {
        self.launchImagePicker()
    }
    @IBAction func eventNameButtonPushed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goingToChangeEventNameSegue", sender: self)
    }
    
    
    
    
    func launchImagePicker () {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("Choose Option", comment: ""), preferredStyle: .actionSheet)
        
        // 2
        let cameraRollAction = UIAlertAction(title: NSLocalizedString("Camera Roll", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] Camera Roll selected")
            picker.allowsEditing = false //2
            picker.sourceType = .photoLibrary //3
            self.present(picker, animated: true, completion: nil)
        })
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] Taken photo selected")
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.cameraCaptureMode = .photo
                self.present(picker, animated: true, completion: nil)
            } else {
                self.noCamera()
            }
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] UIAlertAction Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraRollAction)
        optionMenu.addAction(takePhotoAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: NSLocalizedString("No camera", comment: ""),
            message: NSLocalizedString("Sorry, this device has no camera", comment: ""),
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(alertVC,
                animated: true,
                completion: nil)
    }
    
    
    // MARK: Photopicker delegate methods
    
    func imagePickerController( _ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage { //2
            self.photoView.image = chosenImage
            self.imagePicked = chosenImage
            self.imageChanged = true
            
        } else { print("[NC!!] Problem picking image") }
        
        dismiss(animated: true, completion: nil) //5
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
