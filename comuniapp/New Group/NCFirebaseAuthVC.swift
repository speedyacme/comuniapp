//
//  NCFirebaseAuthVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 10/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebasePhoneAuthUI

class NCFirebaseAuthVC: UIViewController, FUIAuthDelegate {
    
    fileprivate(set) var auth:Auth?
    fileprivate(set) var authUI: FUIAuth? //only set internally but get externally
    fileprivate(set) var authStateListenerHandle: AuthStateDidChangeListenerHandle?
    @IBOutlet weak var tryAgainButton: NcButtonBordered!
    var darkMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tryAgainButton.isHidden = true
        //self.auth = Auth.auth()
        self.authUI = FUIAuth.defaultAuthUI()
        self.authUI?.delegate = self
        let providers: [FUIAuthProvider] = [
            //FUIGoogleAuth(),
            //FUIFacebookAuth(),
            //FUITwitterAuth(),
            FUIPhoneAuth(authUI: self.authUI!),
            ]
        self.authUI?.providers = providers
    }
    
    override func viewDidAppear(_ animated: Bool) { // We need to do it in the viewdidappear because in viewdidload will not work, but sure why, but...
        guard let user = Auth.auth().currentUser else {
            self.login(sender: self)
            return
        }
        self.userIsLoggedIn(user: user)
        
    }
    
    func login(sender: AnyObject) {
        if self.authUI != nil {
            if #available(iOS 13.0, *), self.traitCollection.userInterfaceStyle == .dark {
                // User Interface is Dark
                darkMode = true
                self.view.window?.overrideUserInterfaceStyle = .light
                print ("[NC] Dark Mode deactivated")
            }
            let phoneProvider = self.authUI!.providers.first as! FUIPhoneAuth
            phoneProvider.signIn(withPresenting: self, phoneNumber: nil)
        }
    }
    
    // Call back FIRAuthUIDelegate
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        // handle user (`authDataResult.user`) and error as necessary
        guard let authError = error else {
            if let user = authDataResult?.user {
                self.userIsLoggedIn(user: user)
                if #available(iOS 13.0, *), darkMode == true {
                    self.view.window?.overrideUserInterfaceStyle = .dark
                    print ("[NC] Dark Mode reactivated")
                }
            } else {
                // User is not signed in
                print ("[NC] Authenticating to firebase but User is nil ")
                if #available(iOS 13.0, *), darkMode == true {
                    self.view.window?.overrideUserInterfaceStyle = .dark
                    print ("[NC] Dark Mode reactivated")
                }
            }
            return
        }
        
        let errorCode = UInt((authError as NSError).code)
        
        switch errorCode {
        case FUIAuthErrorCode.userCancelledSignIn.rawValue:
            print("User cancelled sign-in");
            break
            
        default:
            let detailedError = (authError as NSError).userInfo[NSUnderlyingErrorKey] ?? authError
            print("Login error: \((detailedError as! NSError).localizedDescription)");
        }
        if #available(iOS 13.0, *), darkMode == true {
            self.view.window?.overrideUserInterfaceStyle = .dark
        }
        self.tryAgainButton.isHidden = false
    }
    
    func userIsLoggedIn (user: User) {
        // User is signed in.
        if let phone = user.phoneNumber {
            print("[NC] logged with user uid:\(String(describing: user.uid)) and phone Number: \(phone)")
            // Now we go to the the start again
            NCAppStartProcess.startProcess()
            //NCGUI.showVC(viewControllerIdentifier: "changeNameVC")
            //self.performSegue(withIdentifier: "segueToChangeName", sender: self)
        }
    }
    
    @IBAction func tryAgainButtonPressed(_ sender: Any) {
        self.login(sender: self)
    }
    
    // You need to adopt a FUIAuthDelegate protocol to receive callback
    
    
    /*
     // *** This is only needed for Google of Facebook authentication ***
     //For Google Sign-in support, add custom URL schemes to your Xcode project (step 1 of the implement Google Sign-In documentation).
     For Facebook Login support, follow step 3 and 4 of Facebook login documentation, and add custom URL schemes in step 4 of Facebook SDK for iOS-Getting started documentation.
     Finally, add a call to handle the URL that your application receives at the end of the Google/Facebook authentication process.
     
     // *** This is only needed for Terms of service for user/password access   *** ///
     let kFirebaseTermsOfService = URL(string: "https://firebase.google.com/terms/")!
     authUI?.tosurl = kFirebaseTermsOfService
     
     // *** Customs strings   *** ///
     
     authUI?.customStringsBundle = NSBundle.mainBundle() // Or any custom bundle.
     */

}
