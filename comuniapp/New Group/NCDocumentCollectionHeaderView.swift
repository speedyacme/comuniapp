//
//  NCDocumentCollectionHeaderView.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 07/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit

class NCDocumentCollectionHeaderView: UICollectionReusableView {
    
 @IBOutlet weak var headerViewDocumentTypeLabel: UILabel!
    @IBOutlet weak var headerViewCommunityLabel: UILabel!
    
}
