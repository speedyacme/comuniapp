//
//  NCFileCollectionVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 01/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import PhotosUI
import MobileCoreServices
import QuickLook
import PDFKit


class NCFileCollectionReadOnlyVC: UICollectionViewController, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout  {
   
    private var queryListeners = [ListenerRegistration]()
    private let reuseIdentifier = "imageCell"
    private let spacing:CGFloat = 16.0  // Inset spacing
    let pullToRefreshControl = UIRefreshControl()
    
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    var topTitle = ""
    var mainTitle = "Documents"
   
    var documents = [NCDocument]() {
        didSet
        {
            self.updateUI()
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        // UI Labeling and buttons
        self.title = self.mainTitle
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.navigationItem.prompt = self.topTitle
        self.updateUI()
    }
    
    func stopObserving () {
        for listener in self.queryListeners {
            listener.remove()
        }
        self.queryListeners.removeAll() // We reset the queryListeners
    }
    
    
    
    func updateUI () {
        //self.navigationItem.leftBarButtonItems?.removeAll()
        //self.navigationItem.rightBarButtonItems?.removeAll()
        self.collectionView.reloadData()
    }
    
    
    
   
    func runActivityIndicator () {
        // See if it is animating.
        if activityIndicatorView.isAnimating {
            // If currently animating, stop it.
            activityIndicatorView.stopAnimating()
        }
        else {
            // Begin.
            activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivityIndicator () {
        activityIndicatorView.stopAnimating()
    }
    
    
    
    

    // MARK: UICollectionViewDataSource
           
           
           
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
   }
   
   
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.documents.count
   }
   
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        // Configure the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! photoCollectionViewCell

        let document = self.documents[indexPath.row]
    
        switch document.mimeType {
           case "image/jpeg":
            if let thumbnailData = document.fileData {
                cell.imageView.image = UIImage(data: thumbnailData)
            } else {
                cell.imageView.image = UIImage (named: "icons8-image_file-1")
            }
            
           case "application/pdf":
            cell.imageView.image = UIImage (named: "icons8-pdf-1")
           default:
            cell.imageView.image = UIImage (named: "icons8-document")
           }
        
        return cell
   }
   
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       guard let cell = collectionView.cellForItem(at: indexPath) as? photoCollectionViewCell
           else {return}
       
       switch self.documents[indexPath.row].mimeType {
       case "image/jpeg":
           self.performSegue(withIdentifier: "detailToPhoto", sender: cell)
       case "application/pdf":
           self.performSegue(withIdentifier: "toPdfViewer", sender: cell)
       default:
           return
       }
   }
       
   
    // MARK: - Collection View Flow Layout Delegate

   func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
          let sectionInsets = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing * 2, right: spacing)
          return sectionInsets
      }
    
      internal func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return spacing
      }
      
      private func collectionView(_ collectionView: UICollectionView,
                          layout collectionViewLayout: UICollectionViewLayout,
                          minimumInteritemSpacing section: Int) -> CGFloat {
          return spacing
      }
   //1
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        let numberOfItemsPerRow:CGFloat = 4
        let spacingBetweenCells:CGFloat = 10
        let margins:CGFloat = 16
        
        let totalSpacing = (2 * margins) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        if let collection = self.collectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            self.collectionView.updateConstraints()
            return CGSize(width: width, height: width) // We use the same height because it is a square
        }else{
            return CGSize(width: 0, height: 0)
        }
  }
  

   
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier
        {
            switch identifier {
            
            case "detailToPhoto":
                
                let cell = sender as! photoCollectionViewCell
                let destinationVC = segue.destination as! NCPhotoVC
                if let index = self.collectionView.indexPath(for: cell) {
                    NCModelAPI.sharedInstance.getStorageFile(path: self.documents[index.row].fileStorageReferencePath, blockAfterGettingFile: { data, err in
                        if let error = err {
                            print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                        } else {
                            if data != nil {
                                print ("[NC] Document number \(index.row+1) obtained from firestorage")
                                self.documents[index.row].fileData = data
                                NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key: self.documents[index.row].fileStorageReferencePath, data: data)
                                destinationVC.imageToPresent = UIImage (data: data!)
                            } else {
                                print ("[NC!!] Error obtaining image data")
                                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting data from image", vc: self)
                            }
                        }
                    })
                }
                                    
            case "toPdfViewer":
                print("going to pdfviewer")
                let cell = sender as! photoCollectionViewCell
                let destinationVC = segue.destination as!NCPDFVC
                if let index = self.collectionView.indexPath(for: cell) {
                    NCModelAPI.sharedInstance.getStorageFile(path: self.documents[index.row].fileStorageReferencePath, blockAfterGettingFile: { data, err in
                        if let error = err {
                            print ("[NC!!] Error getting documents: \(error.localizedDescription)")
                        } else {
                            if data != nil {
                                print ("[NC] Document number \(index.row+1) obtained from firestorage")
                                self.documents[index.row].fileData = data
                                NCModelAPI.sharedInstance.addToLocalDocumentBuffer(key:
                                    self.documents[index.row].fileStorageReferencePath, data: data)
                                destinationVC.pdfDocument = PDFDocument.init(data: data!)
                            } else {
                                print ("[NC!!] Error obtaining pdf data")
                                NCGUI.showErrorAlertOnCurrentVC(errorTitle: "Error", message: "Error getting pdf from data", vc: self)
                            }
                        }
                    })
                }
                    
            default: return
            }
        }
    }

}


    

