//
//  NCPersistencyManager.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 11/08/2019.
//  Copyright © 2019 Nutcoders. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase
import FirebaseFirestoreSwift
import FirebaseStorage
import UIKit

public enum StorageType {
    case userDefaults
    case fileSystem
}


class NCPersistencyManager: NSObject {
    
    var userCommunities = [NCCommunity]()
    var documentBuffer : [String: Data] = [:]
    var documentTypeList = [NCDocumentType]()
    
    // MARK: Global parameters
    
    func getDocumentTypesList (blockAfterGettingDocuments: @escaping ([NCDocumentType], Error?) -> ()) {
        
        
        // First we try to obtain it from User Defaults
        if self.documentTypeList.count != 0 {
            blockAfterGettingDocuments (self.documentTypeList, nil)
        // If it is not in defaults, we go to FireStore
        } else {
            
            // First we get the documents in collection documentTypes
            let db = Firestore.firestore()
            db.collection(constants.fireStore.documentTypes.collectionName).addSnapshotListener (includeMetadataChanges: true) { querySnapshot, error in
                
                guard let documents = querySnapshot?.documents else {
                    print("[NC!!] Error fetching documents: \(error!)")
                    blockAfterGettingDocuments ([NCDocumentType](), error)
                    return
                }
                var documentsDictionaries = [[String:Any]]()
                for document in documents {
                    guard var data = document.data() as [String:Any]? else {
                        print("Document data was empty.")
                    }
                    // Muy importante!!! asignación de la referencia al documento.
                    data [constants.fireStore.generic.documentReference] = document.reference
                    //print("Current data: \(data)")
                    documentsDictionaries.append(data)
                    
                }
                let documentTypes = documentsDictionaries.compactMap({
                    NCDocumentType(dictionary: $0)
                })
                
                self.documentTypeList = documentTypes
                blockAfterGettingDocuments (documentTypes, nil)

            }
        }
    }
    
    // MARK: User functionalities
    
    func signUserIn () {
        DispatchQueue.main.async {
            NCGUI.showVC(viewControllerIdentifier: "startVC")
        }
    }
    
    func getMyPhoneNumber() -> String? {
        var phoneNumberString : String? = nil
        if let phoneNumberDefaults = UserDefaults.standard.string(forKey: constants.defaults.phoneNumber) {
            phoneNumberString = phoneNumberDefaults
        } else {
            phoneNumberString = Auth.auth().currentUser?.phoneNumber
            if phoneNumberString != nil {
                UserDefaults.standard.set(phoneNumberString!, forKey: constants.defaults.phoneNumber)
                print("[NC] User phone Number updated correctly in UserDefaults with phoneNumber: \(phoneNumberString!)")
            } else {
                print("[NC] User phone Number is nil")
            }
        }
        return phoneNumberString
    }
    
    func updateMyUserName(name: String) -> Error? {
        var errorToReturn : Error? = nil
        let db:Firestore! = Firestore.firestore()
        guard let phone = getMyPhoneNumber() else {
            return NSError(domain: "", code: 401, userInfo: [ NSLocalizedDescriptionKey: "No phone number"])
            }
        let docRef = db.document(constants.fireStore.users.collectionName+"/"+phone)
        updateAndMergeFireStoreDocument(documentDictionary: [constants.fireStore.users.nickName:name], docRef: docRef, blockAfterUpdating: { error in
            if error != nil {
                print("[NC] User nickname error after trying to update in Firebase with name: \(name), with error: \(String(describing: error))")
                errorToReturn = error
            } else {
                // user document updated
                print("[NC] User nickname updated correctly in Firebase with name: \(name)")
                // Updating user defaults
                UserDefaults.standard.setValue(name, forKey: constants.defaults.displayName)
                print("[NC] User display name updated correctly in UserDefaults with name: \(name)")
            }
        })
        return errorToReturn
    }
    
    func getMyUserName() -> String? {
        var userNameString : String? = nil
        if let displayNameDefaults = UserDefaults.standard.string(forKey: constants.defaults.displayName) {
            userNameString = displayNameDefaults
        } else {
            print("[NC] User name is nil")
        }
        return userNameString
    }
    
    func getMyUserId() -> String? {
        
        var userId : String? = nil
        if let userIdDefaults = UserDefaults.standard.string(forKey: constants.defaults.userId) {
            userId = userIdDefaults
        } else {
            if let userId = Auth.auth().currentUser?.uid {
                UserDefaults.standard.set(userId, forKey: constants.defaults.userId)
                print("[NC] User id updated correctly in UserDefaults with name: \(userId)")
            } else {
                print("[NC] User id is nil")
            }
        }
        return userId
    }
    
    func getMyOwnerIdAndNickname() -> String {
        if let ownerId = UserDefaults.standard.string(forKey: constants.defaults.ownerId){
            return ownerId + " - " + (self.getMyUserName() ?? "")
        } else {
            return self.getMyUserName() ?? ""
        }
    }
    
    func isUserAdmin (blockAfterAdminInfo: @escaping (Bool, Error?) -> ()) {
        
        if let adminDefaults = UserDefaults.standard.object(forKey: constants.defaults.admin) as! Bool? {
            blockAfterAdminInfo (adminDefaults, nil)
        } else {
            if let phone = self.getMyPhoneNumber() {
                self.getFireStoreDocument(pathRef: constants.fireStore.users.collectionName+"/"+phone, block: { document, error in
                    
                    if let error = error {
                        print("[NC!!] Error on getting user document: \(error.localizedDescription)")
                        blockAfterAdminInfo (false, error)
                    } else {
                        if let document = document, document.exists {
                            let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                            print("[NC] Obtained Document data: \(dataDescription)")
                            if let adminAccess = document.get(constants.fireStore.users.admin) as? Bool {
                                UserDefaults.standard.setValue(adminAccess, forKey: constants.defaults.admin)
                                print("[NC] User admin privilegies updated correctly in UserDefaults with bool: \(adminAccess)")
                                blockAfterAdminInfo (adminAccess, nil)
                            } else {
                                blockAfterAdminInfo (false, nil)
                            }
                        } else {
                            blockAfterAdminInfo (false, nil)
                        }
                    }
                })
            }
        }
    }
    
    
    func logAccess (userPhoneNumber: String) {
        let localTime = Timestamp.init()
        let lastAccess = localTime
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
        let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"]
        let device = UIDevice.current.model
        let deviceName = UIDevice.current.name
        let os = UIDevice.current.systemName
        let osVersion = UIDevice.current.systemVersion
        let logRecord = ["localTime": localTime,
                      "appVersion": appVersion,
                     "buildNumber": buildNumber,
                          "device": device,
                      "deviceName": deviceName,
                              "os": os,
                       "osVersion": osVersion]
        self.addFireStoreDocument(documentDictionary: logRecord as [String : Any], collectionPath: "users" + "/" + userPhoneNumber + "/" + "accessLogs", blockAfterCreatingDocument: { error, docRef in
            if error != nil {
                print ("[NC!!] Error logging timeStamp: \(String(describing: error))")
            } else {
                print ("[NC] Timestamp logged with docRef: \(String(describing: docRef)), now updating user doc with lastAccess")
                Firestore.firestore().collection("users").document(userPhoneNumber).updateData(["lastAccess":lastAccess])
            }
        })
    }
    
    func logOut() -> Error? {
        var errorToReturn : Error? = nil
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("[NC] Error signing out: %@", signOutError)
            errorToReturn = signOutError
        }
        if errorToReturn == nil {
            // We have logged out so we remove defaults data to reset account info
            self.eraseUserDefaults()
        }
        return errorToReturn
    }
    
    func eraseUserDefaults () {
        let defaultObjects = [constants.defaults.displayName, constants.defaults.phoneNumber, constants.defaults.userId, constants.defaults.admin, constants.defaults.communitiesIdList,constants.defaults.userCommunities,constants.defaults.notifyNewEventsCheck, constants.defaults.notifyChangesInEventsCheck]
        for item in defaultObjects {
            UserDefaults.standard.removeObject(forKey: item)
        }
    }
    
    // MARK: - User Communities
      
    func setUserCommunitiesInLocalMemory (communitiesArray: [NCCommunity]) { // Saves user communities in Defaults
      
      self.userCommunities = communitiesArray
    }

    func getUserCommunitiesFromLocalMemory () -> [NCCommunity] {
      
      return self.userCommunities
    }

    func getCommunityNameForCommunityId (communityId: String) -> String {
      var communityname = ""
      if let community = self.getUserCommunitiesFromLocalMemory().first(where: { $0.communityId == communityId }) {
          communityname = community.communityName
      }
      return communityname
    }
    
    
    // MARK: - FireStore Document functionalities
    
    func addFireStoreDocument (documentDictionary: [String:Any], collectionPath: String, blockAfterCreatingDocument: ((Error?, DocumentReference?) -> ())?) {
        // Add a new document with a generated id.
        var ref: DocumentReference? = nil
        let db = Firestore.firestore()
        ref = db.collection(collectionPath).addDocument(data: documentDictionary) { err in
            if let err = err {
                print("[NC] Error adding document: \(err)")
                if let block = blockAfterCreatingDocument {
                    block (err,nil)
                }
            } else {
                print("[NC] Document added to FireStore with ID: \(ref!.documentID)")
                if let block = blockAfterCreatingDocument {
                    block (nil,ref)
                }
            }
        }
    }
    
    func addFireStoreCodableDocument <T: Codable> (codableDocument: T, collectionPath: String, blockAfterCreatingDocument: ((Error?, DocumentReference?) -> ())) {
        // Add a new document with a generated id.
        do {
            let db = Firestore.firestore()
            let newDocReference = try db.collection(collectionPath).addDocument(from:codableDocument, encoder: Firestore.Encoder(), completion: nil)
            print("[NC] document stored in firestore with new document reference: \(newDocReference)")
            blockAfterCreatingDocument (nil, newDocReference)
        }
        catch let error {
            print("[NC] Error writing document to Firestore: \(error)")
            blockAfterCreatingDocument (error, nil)
        }
    }
    
    
    
    
    
    
    
    
    func getDocumentsListener(collectionPath: String, orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([[String:Any]]?, Error?) -> ()) -> ListenerRegistration? {
        
        
        let db = Firestore.firestore()
        let listener = db.collection(collectionPath).addSnapshotListener (includeMetadataChanges: true) { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            var documentsDictionaries = [[String:Any]]()
            for document in documents {
                guard var data = document.data() as [String:Any]? else {
                    print("Document data was empty.")
                }
                // Muy importante!!! asignación de la referencia al documento.
                data [constants.fireStore.generic.documentReference] = document.reference
                print("[NC] getDocumentsListener has obtained the following data: \(data)")
                documentsDictionaries.append(data)
            }
            blockAfterGettingDocuments (documentsDictionaries, nil)
        }
        return listener
    }
    
    func getCodableDocumentsListener <T: Codable> (collectionPath: String, orderField: String, descending: Bool, limit: Int, classType: T.Type, blockAfterGettingDocuments: @escaping ([T]?, Error?) -> ()) -> ListenerRegistration? {
        
        let db = Firestore.firestore()
        var documentArray = [T]()
        let listener = db.collection(collectionPath).addSnapshotListener(includeMetadataChanges: true) { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            // Construct a Result type to encapsulate deserialization errors or
            // successful deserialization. Note that if there is no error thrown
            // the value may still be `nil`, indicating a successful deserialization
            // of a value that does not exist.
            //
            // There are thus three cases to handle, which Swift lets us describe
            // nicely with built-in sum types:
            //
            //      Result
            //        /\
            //   Error  Optional<City>
            //               /\
            //            Nil  City
            
            documentArray = documents.compactMap { queryDocumentSnapshot in
                let result = Result { try queryDocumentSnapshot.data(as: classType) }
                switch result {
                case .success(let documentObject):
                    //if let documentObject = documentObject {
                        // A documentObject value was successfully initialized from the DocumentSnapshot.
                        print("Document obtained from firestore successfully: \(documentObject)")
                        return documentObject
                    //} else {
                        // A nil value was successfully initialized from the DocumentSnapshot,
                        // or the DocumentSnapshot was nil.
                      //  print("Document does not exist")
                    //}
                case .failure(let error):
                    // A `City` value could not be initialized from the DocumentSnapshot.
                    print("Error decoding document from firestore: \(error)")
                    return nil
                }
            }
            blockAfterGettingDocuments (documentArray, nil)
        }
        return listener
    }
    
    func getNCDocumentsListener (collectionPath: String, orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([NCDocument]?, Error?) -> ()) -> ListenerRegistration? {
        
        let db = Firestore.firestore()
        var documentArray = [NCDocument]()
        let listener = db.collection(collectionPath).addSnapshotListener(includeMetadataChanges: true) { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            // Construct a Result type to encapsulate deserialization errors or
            // successful deserialization. Note that if there is no error thrown
            // the value may still be `nil`, indicating a successful deserialization
            // of a value that does not exist.
            //
            // There are thus three cases to handle, which Swift lets us describe
            // nicely with built-in sum types:
            //
            //      Result
            //        /\
            //   Error  Optional<City>
            //               /\
            //            Nil  City
            
            documentArray = documents.compactMap { queryDocumentSnapshot in
                let result = Result { try queryDocumentSnapshot.data(as: NCDocument.self) }
                switch result {
                case .success(let documentObject):
                    //if let documentObject = documentObject {
                        // A documentObject value was successfully initialized from the DocumentSnapshot.
                        print("Document obtained from firestore successfully: \(documentObject)")
                        return documentObject
                    //} else {
                        // A nil value was successfully initialized from the DocumentSnapshot,
                        // or the DocumentSnapshot was nil.
                      //  print("Document does not exist")
                    //}
                case .failure(let error):
                    // A `City` value could not be initialized from the DocumentSnapshot.
                    print("Error decoding document from firestore: \(error)")
                    return nil
                }
            }
            blockAfterGettingDocuments (documentArray, nil)
        }
        return listener
    }
    
    
    
    func getCollectionGroupDocumentsListener(collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([[String:Any]]?, Error?) -> ()) -> ListenerRegistration? {
        
        
        let db = Firestore.firestore()
        let listener  =
        db.collectionGroup(collectionGroupId).whereField(constants.fireStore.communities.communityId, in: communityIds).addSnapshotListener (includeMetadataChanges: true) { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            var documentsDictionaries = [[String:Any]]()
            for document in documents {
                guard var data = document.data() as [String:Any]? else {
                    print("Document data was empty.")
                }
                // Muy importante!!! asignación de la referencia al documento.
                data [constants.fireStore.generic.documentReference] = document.reference
                //print("Current data: \(data)")
                documentsDictionaries.append(data)
            }
            
            blockAfterGettingDocuments (documentsDictionaries, nil)
        }
        return listener
    }
    
    func getCollectionGroupNCDocumentsListener(collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([NCDocument]?, Error?) -> ()) -> ListenerRegistration? {
        
        
        let db = Firestore.firestore()
        var documentArray = [NCDocument]()
        let listener  =
        db.collectionGroup(collectionGroupId).whereField(constants.fireStore.communities.communityId, in: communityIds).addSnapshotListener (includeMetadataChanges: true) { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            documentArray = documents.compactMap { queryDocumentSnapshot in
                let result = Result { try queryDocumentSnapshot.data(as: NCDocument.self) }
                switch result {
                case .success(let documentObject):
                    //if let documentObject = documentObject {
                        // A documentObject value was successfully initialized from the DocumentSnapshot.
                        print("Document obtained from firestore successfully: \(documentObject)")
                        return documentObject
                    //} else {
                        // A nil value was successfully initialized from the DocumentSnapshot,
                        // or the DocumentSnapshot was nil.
                      //  print("Document does not exist")
                    //}
                case .failure(let error):
                    // A `City` value could not be initialized from the DocumentSnapshot.
                    print("Error decoding document from firestore: \(error)")
                    return nil
                }
            }
            blockAfterGettingDocuments (documentArray, nil)
        }
        return listener
    }
    
    func getCollectionGroupCodableDocumentsListener <T: Codable> (collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, classType: T.Type, blockAfterGettingDocuments: @escaping ([T]?, Error?) -> ()) -> ListenerRegistration? {
        
        
        let db = Firestore.firestore()
        var documentArray = [T]()
        let listener  =
        db.collectionGroup(collectionGroupId).whereField(constants.fireStore.communities.communityId, in: communityIds).addSnapshotListener (includeMetadataChanges: true) { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                blockAfterGettingDocuments (nil, error)
                return
            }
            documentArray = documents.compactMap { queryDocumentSnapshot in
                let result = Result { try queryDocumentSnapshot.data(as: classType) }
                switch result {
                case .success(let documentObject):
                    //if let documentObject = documentObject {
                        // A documentObject value was successfully initialized from the DocumentSnapshot.
                        print("Document obtained from firestore successfully: \(documentObject)")
                        return documentObject
                    //} else {
                        // A nil value was successfully initialized from the DocumentSnapshot,
                        // or the DocumentSnapshot was nil.
                      //  print("Document does not exist")
                    //}
                case .failure(let error):
                    // A `City` value could not be initialized from the DocumentSnapshot.
                    print("Error decoding document from firestore: \(error)")
                    return nil
                }
            }
            blockAfterGettingDocuments (documentArray, nil)
        }
        return listener
    }
    
    // WATCH OUT! THIS WILL OVERRIDE THE COMPLET DOCUMENT, NOT MERGING IT
    func updateFireStoreDocument(documentDictionary: [String:Any], docRef: DocumentReference, blockAfterUpdating: ((Error?)->Void)? )  {
        docRef.setData(documentDictionary, completion: blockAfterUpdating)
    }
    
    // THIS ONE WILL MERGE IT, USE IT FOR CHANGING JUST SOME FIELDS BUT NOT ALL
    func updateAndMergeFireStoreDocument(documentDictionary: [String:Any], docRef: DocumentReference, blockAfterUpdating: ((Error?)->Void)? )  {
        docRef.setData(documentDictionary, merge:true, completion: blockAfterUpdating)
    }
    
    func getFireStoreDocument (pathRef: String, block: @escaping ( DocumentSnapshot?, Error?) -> ()) {
        
        let db = Firestore.firestore()
        let docRef = db.document(pathRef)
        docRef.getDocument(source: .default, completion: block )
        
    }
    
    
    func deleteFireStoreDocument(pathRef: String, block: ((Error?) -> ())? ) {
        let db = Firestore.firestore()
        let docRef = db.document(pathRef)
        docRef.delete(completion: block)
    }
    
    
    
    
    
  
    
    // MARK: Storage functionality
    
    // For function addStorageFile filePath INCLUDES filename!!!

    func addStorageFile (fileData: Data, metadataContentType: String?, filePath: String, blockAfterUpdating: @escaping ((StorageMetadata?, Error?) -> Void)) {
        let storage = Storage.storage()
        
        // MetadataContentType should be a String with the standard MIME
        
        /* FYI Documentation from firebase
         Create a root reference
        let storageRef = storage.reference()
        
        Create a reference to "mountains.jpg"
        let mountainsRef = storageRef.child("mountains.jpg")
        
        Create a reference to 'images/mountains.jpg'
        let mountainImagesRef = storageRef.child("images/mountains.jpg")
        Create a reference to the file you want to upload
        let riversRef = storageRef.child("images/rivers.jpg")*/
        
        // Now we add the name of the file
        
        let pathReference = storage.reference(withPath: filePath)
        let metadata = StorageMetadata ()
        metadata.contentType = metadataContentType
      
        pathReference.putData(fileData, metadata: metadata, completion: { metadata, error in
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                if let error = error {
                    blockAfterUpdating (nil, error)
                }
                return
            }
            // Metadata contains file metadata such as size, content-type.
            //let size = metadata.size
            blockAfterUpdating (metadata, nil)
            // You can also access to download URL after upload.
            /*
            pathReference.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }
            }*/
        })
    }
    
    func getStorageFile(path: String, blockAfterGettingFile: @escaping (Data?, Error?) -> ()) {
        // Create a reference to the file you want to download
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: path)
        
        // Before we get it from Firebase Storage, we see if we have it in Local file Device system
        if let dataFromBuffer = self.getFileDataFromLocalDevice(fileNameWithExtension: pathReference.name, inStorageType: .fileSystem) {
            blockAfterGettingFile (dataFromBuffer, nil)
        } else {
            // We go to firebase Storage to download file
            // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
            pathReference.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    // Uh-oh, an error occurred!
                    blockAfterGettingFile (nil, error)
                } else {
                    // We store it in our Local file Device system
                    if let dataToBuffer = data {
                        self.addFileDataToLocalDevice(fileData: dataToBuffer, fileNameWithExtension: pathReference.name, withStorageType: .fileSystem)
                    }
                    blockAfterGettingFile (data, nil)
                }
            }
        }
    }
    
    func deleteStorageFile(path: String, blockAfterDeletingFile: @escaping (Error?) -> ()) {
        // Create a reference to the file you want to download
        let storage = Storage.storage()
        let pathReference = storage.reference(withPath: path)
        
        // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
        pathReference.delete(completion: { error in
            if let error = error {
                // Uh-oh, an error occurred!
                print ("[NC!!] Error deleting FireStorage File \(error.localizedDescription)")
                blockAfterDeletingFile (error)
            } else {
                // File Delete correctly
                print ("[NC] FireStorage File deleted correctly")
                blockAfterDeletingFile (nil)
            }
        })
    }
    
    
    // MARK:  - Combined FireStore Documents + Storage functionality
    
    
    func createAndSaveDocumentWithFileData(fileData: Data, metadataContentType: String, documentType: Int, fileName: String?, collectionRef: CollectionReference, communityId: String, blockAfterFileAndDocumentCreated: @escaping (DocumentReference?, StorageMetadata?, Error?) -> ()) {
        
        var finalFileName : String
        if fileName != nil {
            finalFileName = fileName!
        } else {
            finalFileName = self.getFileName (metadataContentType: metadataContentType)
        }
        let fileCompletePath = collectionRef.path + "/" + finalFileName
        self.addStorageFile(fileData: fileData, metadataContentType: metadataContentType, filePath: fileCompletePath, blockAfterUpdating: ({ metadata, error in
            
            guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                if let error = error {
                    blockAfterFileAndDocumentCreated (nil, nil, error)
                }
                return
            }
            
            // TODO: CREO QUE AQUÍ TENDRÍAMOS QUE PONER EL CASO EN EL QUE HAY UN ERROR
            
            guard let dateUpdated = metadata.updated else {
                print ("[NC!!] Error creating document, the timeStamp is nil")
                return
            }
            guard let userId = self.getMyPhoneNumber() else { // Now we use the phone number as userId.
                print ("[NC!!] Error creating document, the userId is nil")
                return
            }
            guard let userName = self.getMyUserName() else {
                print ("[NC!!] Error creating document, the userName is nil")
                return
            }
            // The way to create the document is like this:
            let documentCreated = NCDocument.init(docName: finalFileName, description: "", type: documentType, mimeType: metadataContentType, creatorUserId: userId, creatorNickname: userName, uploadDate: Timestamp.init(date: dateUpdated), fileStorageReferencePath: fileCompletePath, communityId: communityId)
            do {
                let newDocReference = try collectionRef.addDocument(from: documentCreated)
                print("[NC] NCDocument stored in firestore with new document reference: \(newDocReference)")
                // Now we use the device local file buffer to save all the images and avoid retrieve them later
                self.addFileDataToLocalDevice (fileData: fileData, fileNameWithExtension: finalFileName, withStorageType: .fileSystem)
                blockAfterFileAndDocumentCreated (newDocReference, metadata, nil)
            }
            catch let error {
                print("[NC] Error writing NCDocument to Firestore: \(error)")
                blockAfterFileAndDocumentCreated (nil, nil, error)
            }
        }))
    }
    
    func deleteFirestoreDocumentWithStorageFile(docRef: DocumentReference, fileStorageReferencePath: String, block: @escaping (Error?) -> ()) {
        // First we delete the file
        self.deleteStorageFile(path: fileStorageReferencePath, blockAfterDeletingFile: { error in
            if let error = error {
                block (error)
            } else {
                // Now we delete the document
               print ("[NC] The Storage file has been deleted correctly")
               self.deleteFireStoreDocument(pathRef: docRef.path, block: { error in
                    if let error = error {
                        block (error)
                    } else {
                       block(nil)
                    }
                })
            }
        })
    }
    
    func getFileName (metadataContentType: String?) -> String {
       
        var fileName = ""
        var docTypeName = ""
        switch (metadataContentType) {
        case nil:
            docTypeName = "noType_"
            break;
        case "image/jpeg":
            docTypeName = ".jpeg"
        case "application/pdf":
            docTypeName = ".pdf"
        default:
            docTypeName = "noType_"
            break;
        }
        //let date = Date()
        let dateFormatted = Date().getFormattedDate(format: "yyMMdd_HHmmss_Z") // Set output formate
        fileName = dateFormatted + docTypeName
        return fileName
      
    }
    
    // MARK: - LOCAL RAM MEMORY BUFFER (we are not using it now)
    
    func getFromDocumentBuffer (key: String) -> Data? {
        return self.documentBuffer[key]
    }
    
    func addToLocalDocumentBuffer (key: String, data: Data?) {
        if data != nil {
            self.documentBuffer[key] = data
        }
    }

    
    
    // MARK: - LOCAL DEVICE FILE BUFFER
    
    
    private func filePath(forFileNameWithExtension: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        
        return documentURL.appendingPathComponent(forFileNameWithExtension)
    }
    
    func addFileDataToLocalDevice (fileData: Data, fileNameWithExtension: String, withStorageType storageType: StorageType) {
        switch storageType {
        case .fileSystem:
            if let filePath = filePath(forFileNameWithExtension: fileNameWithExtension) {
                do  {
                    try fileData.write(to: filePath,
                                                options: .atomic)
                    print("[NC] Filedata saved in local device")
                } catch let err {
                    print("[NC!!] Saving file resulted in error: ", err)
                }
            }
        case .userDefaults:
            UserDefaults.standard.set(fileData,
                                        forKey: fileNameWithExtension)
        }
    }
    
    func getFileDataFromLocalDevice (fileNameWithExtension: String,
                                inStorageType storageType: StorageType) -> Data? {
        switch storageType {
        case .fileSystem:
            if let filePath = self.filePath(forFileNameWithExtension: fileNameWithExtension),
                let fileData = FileManager.default.contents(atPath: filePath.path) {
                print("[NC] Filedata obtained from local device")
                return fileData
            } else {
                print("[NC] Filedata not found on local device we will look in Firebase storage")
                return nil
            }
        case .userDefaults:
            if let fileData = UserDefaults.standard.object(forKey: fileNameWithExtension) as? Data {
                return fileData
            }
        }
        
        return nil
    }
    
    private func deleteFileDataFromLocalDevice (fileNameWithExtension: String,
                                inStorageType storageType: StorageType) {
        switch storageType {
        case .fileSystem:
            if let filePath = self.filePath(forFileNameWithExtension: fileNameWithExtension),
                FileManager.default.fileExists(atPath: filePath.path) {
                
                do {
                    try FileManager.default.removeItem(atPath: filePath.path)
                    print("[NC] Filedata deleted correctly from local device")
                } catch let error as NSError {
                    print("[NC!!] Error deleting filedata from local device: \(error.domain)")
                }
            }

        case .userDefaults:
            if UserDefaults.standard.object(forKey: fileNameWithExtension) as? Data != nil {
                UserDefaults.standard.removeObject(forKey: fileNameWithExtension)
                print("[NC] Filedata removed from defaults")
            }
        }
    }
    
    func getDownloadUrlForFileReference (documentReference: DocumentReference, blockAfterGettingUrl: @escaping (URL?, Error?) -> ()) {
       self.getFireStoreDocument(pathRef: documentReference.path, block: { docSnapshot, error in
            if let error = error {
                print("[NC!!] Error fetching Document Document: \(error)")
                blockAfterGettingUrl (nil, error)
                return
            } else {
                guard var documentDictionary = docSnapshot?.data() else {
                    print("[NC!!] Error fetching message Document, data snapshot was nil")
                    blockAfterGettingUrl (nil, nil)
                    return
                }
                 // Muy importante!!! asignación de la referencia al documento.
                documentDictionary [constants.fireStore.generic.documentReference] = docSnapshot!.reference
                print("[NC] the document has the following data: \(documentDictionary)")
                let filePath = documentDictionary [constants.fireStore.documents.fileStorageReferencePath] as! String
                let fileReference = Storage.storage().reference(forURL: filePath)
                // Fetch the download URL
                fileReference.downloadURL { url, error in
                  if let error = error {
                    print("[NC!!] Error fetching download URL from file: \(error)")
                    blockAfterGettingUrl (nil, error)
                    return
                  } else {
                    // Get the download URL for 'images/stars.jpg'
                    blockAfterGettingUrl (url, nil)
                  }
                }
                
            }
            
        })
    }
    
    
    
    
    
}
