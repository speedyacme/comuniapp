//
//  NCLegalTermsSwiftUIView.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 5/4/23.
//  Copyright © 2023 Nutcoders. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct NCLegalTermsSwiftUIView: View {
    
    let fileName: String
    
    var body: some View {
        UITextViewWrapper(attributedString: getAttributedString())
            .padding()
    }
    
    private func getAttributedString() -> NSAttributedString? {
        guard let fileURL = Bundle.main.url(forResource: fileName, withExtension: "rtf") else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: fileURL)
            let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [.documentType: NSAttributedString.DocumentType.rtf]
            let attributedString = try NSAttributedString(data: data, options: options, documentAttributes: nil)
            return attributedString
        } catch {
            print("Error: \(error)")
            return nil
        }
    }
}

@available(iOS 13.0, *)
struct UITextViewWrapper: UIViewRepresentable {
    typealias UIViewType = UITextView
    
    var attributedString: NSAttributedString?
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.isEditable = false
        return textView
    }
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.attributedText = attributedString
    }
}


@available(iOS 13.0, *)
struct NCLegalTermsSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        NCLegalTermsSwiftUIView(fileName: "infos_legales")
    }
}
