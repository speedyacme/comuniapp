//
//  NcConstants.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 7/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import Foundation
import UIKit
// comentario inútil, se puede borrar.

var communitiesArray : [NCCommunity] = [NCCommunity]()

struct constants {
    struct fireStore {
        
        struct generic {
            static let documentReference = "documentId"
        }
        struct communities {
            static let collectionName = "communities"
            static let communityId = "communityId"
            static let communityName = "communityName"
            static let landRegistryReference = "landRegistryReference"
            static let picture = "picture"
            static let location = "location"
            static let googleMapsId = "googleMapsId"
        }
        
        struct documentTypes {
            static let collectionName = "documentTypes"
        }
        
        struct communications {
            static let collectionName = "communications"
            static let date = "date"
            static let shortDescription = "shortDescription"
            static let longDescription = "longDescription"
            static let creatorUserId = "creatorUserId"
            static let creatorNickname = "creatorNickname"
            static let communityId = "communityId"
            static let documentsCollectionName = "documents"
        }
        
        struct issues {
            
            static let collectionName = "issues"
            static let communityId = "communityId"
            static let title = "title"
            static let description = "description"
            static let openingDate = "openingDate"
            static let startingDate = "startingDate"
            static let resolutionDate = "resolutionDate"
            static let closingDate = "closingDate"
            static let creatorUserId = "creatorUserId"
            static let creatorNickname = "creatorNickname"
            static let status = "status"
            static let type = "type"
            static let documentsCollectionName = "documents"
        }
        
        struct info {
            static let collectionName = "informations"
            static let description = "description"
            static let phone = "phone"
            static let type = "type"
            static let communityId = "communityId"
        }
        
        struct messages {
            
            static let collectionName = "messagesList"
            static let sentDate = "messagePostDate"
            static let creatorUserId = "creatorUserId"
            static let creatorNickname = "creatorNickname"
            static let mesageText = "messageText"
            static let messageFile = "messageFile"
        }
        
        struct documents {
            
            static let documentsCollectionName = "documentation"
            static let communityId = "communityId"
            static let documentName = "docName"
            static let documentDescription = "description"
            static let documentType = "type"
            static let mimeType = "mimeType"
            static let creatorUserId = "creatorUserId"
            static let creatorNickname = "creatorNickname"
            static let uploadDate = "uploadDate"
            static let fileStorageReferencePath = "fileStorageReferencePath"
           
        }
        
        struct users {
            static let collectionName = "users"
            static let fieldNameCommunities = "communities"
            static let admin = "admin"
            static let nickName = "nickname"
            static let ownerId = "ownerid"
        }
    }
    struct colors {
        static let blueBackgroundColor = UIColor (red: 0.10, green: 0.36, blue: 0.64, alpha: 1.0)
        static let blueControlColor = UIColor (red: 0.15, green: 0.24, blue: 0.50, alpha: 1.0)
        static let blueTextColor = UIColor (red: 0.65, green: 0.77, blue: 0.86, alpha: 1.0)
        static let greenBackgroundColor = UIColor (red: 0.08, green: 0.57, blue: 0.57, alpha: 1.0)
        static let iosDefaultBlueControlColor = UIColor (red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
        static let mainAppColor = UIColor (red: 0.06, green: 0.5, blue: 1, alpha: 1) // #0F80FF comuniApp Blue
        
    }
    
    struct fonts {
        static let mainAppRegularTextFontName = "Lato-Regular"
        static let mainAppBoldTextFontName = "Lato-Bold"
    }
    
    struct defaults {
        
        static let notifyNewEventsCheck = "notifyNewEventsCheck"
        static let notifyChangesInEventsCheck = "notifyChangesInEventsCheck"
        static let admin = "admin"
        static let phoneNumber = "phoneNumber"
        static let displayName = "displayName" // Este lo eliminaremos cuando nickname funcione
        static let nickName = "nickName" // Este será el definitivo para que sea compatible con Jordi
        static let ownerId = "ownerId"
        static let userId = "userId"
        static let communitiesIdList = "communitiesIdList"
        static let userCommunities = "userCommunities"
        static let documentTypeList = "documentTypeList"

    }
    
    
    struct urls {
        static let appUrl = "https://communyapp.web.app" // This is the dynamic link of firebase
        static let appEMail = "communyapp.nutcoders@gmail.com"
    }
}

// Appearance constants



// IMPORTANT!!!: CHANGING THIS PARAMETER, CHANGES BETWEEN PARSE TEST OR PRODUCTION. USE FALSE FOR TESTING
// *****************************************************************************************************
let isInProduction = false  // When we want to test, we need to set false. For production set to TRUE
// *****************************************************************************************************



enum communicationsType : String {
    case meeting = "Meeting"
    case noType = ""
    func localizedString() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
    static func getTitleFor(title:communicationsType) -> String {
        return title.localizedString()
    }
    // usage: let localizedString: String = communicationsType.getTitleFor(title: .meeting)
}



protocol DocumentSerializable  {
    init?(dictionary:[String:Any])
}


