//
//  NCModelAPI.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 11/08/2019.
//  Copyright © 2019 Nutcoders. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseStorage

class NCModelAPI: NSObject {
    private let persistencyManager: NCPersistencyManager
    //private let httpClient: HTTPClient
    //private let isOnline: Bool
    
    //1
    class var sharedInstance: NCModelAPI {
        //2
        struct Singleton {
            //3
            static let instance = NCModelAPI()
        }
        //4
        return Singleton.instance
    }
    
    override init() {
        persistencyManager = NCPersistencyManager()
        // httpClient = HTTPClient()
        //isOnline = false
        
        super.init()
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector:"downloadImage:", name: "BLDownloadImageNotification", object: nil)
    }
    
    deinit {
        //NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    // MARK: Global parameters
       
    func getDocumentTypesList (blockAfterGettingDocuments: @escaping ([NCDocumentType], Error?) -> ()) {
        persistencyManager.getDocumentTypesList (blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
       

    // MARK: USER APIS
        
    func signUserIn () {
        persistencyManager.signUserIn()
    }
    
    func getMyPhoneNumber() -> String? {
        return persistencyManager.getMyPhoneNumber()
    }
    
    func updateMyUserName(name: String) -> Error? {
        return persistencyManager.updateMyUserName (name: name)
    }
    
    func getMyUserName() -> String? {
        return persistencyManager.getMyUserName()
    }
    
    func getMyUserId() -> String? {
        return persistencyManager.getMyUserId()
    }
    
    func getMyOwnerIdAndNickname() -> String {
        return persistencyManager.getMyOwnerIdAndNickname()
    }
    
    func isUserAdmin (blockAfterAdminInfo: @escaping (Bool, Error?) -> ()) {
        persistencyManager.isUserAdmin(blockAfterAdminInfo: blockAfterAdminInfo)
    }
    
    func logOut() -> Error? {
        return persistencyManager.logOut()
    }
    
    func logAccess (userPhoneNumber: String) {
        persistencyManager.logAccess(userPhoneNumber: userPhoneNumber)
    }
    
 
// MARK: User Communities
    
    /*
    func getAndSetUserCommunitiesIdList () {
        persistencyManager.getAndSetUserCommunitiesIdList()
    }*/
    /*
    
    func setUserCommmunitiesIdListInDefaults (communitiesIdArray: [String]) {
        persistencyManager.setUserCommmunitiesIdListInDefaults(communitiesIdArray: communitiesIdArray)
    }*/
    
    // Sets Defaults with user communities array types
    func setUserCommunitiesInLocalMemory (communitiesArray: [NCCommunity]) {
        persistencyManager.setUserCommunitiesInLocalMemory(communitiesArray: communitiesArray)
    }
    
    // Returns user communities array types from defaults
    func getUserCommunitiesFromLocalMemory () -> [NCCommunity] {
        return persistencyManager.getUserCommunitiesFromLocalMemory()
    }
    
    func getCommunityNameForCommunityId (communityId: String) -> String {
        return persistencyManager.getCommunityNameForCommunityId(communityId: communityId)
    }
    
   
    
    // MARK: Manage Documents
    
    func getDocument (pathRef: String, block: @escaping ( DocumentSnapshot?, Error?) -> ()) {
        persistencyManager.getFireStoreDocument(pathRef: pathRef, block: block)
    }
    
    func addDocument(documentDictionary: [String:Any], collectionPath: String, blockAfterCreatingDocument: ((Error?, DocumentReference?) -> ())?) {
        persistencyManager.addFireStoreDocument (documentDictionary: documentDictionary, collectionPath: collectionPath, blockAfterCreatingDocument: blockAfterCreatingDocument)
    }
    
    func addCodableDocument <T: Codable> (codableDocument: T, collectionPath: String, blockAfterCreatingDocument: ((Error?, DocumentReference?) -> ())) {
        persistencyManager.addFireStoreCodableDocument(codableDocument: codableDocument, collectionPath: collectionPath, blockAfterCreatingDocument: blockAfterCreatingDocument)
    }
    
    func getDocumentsListener(collectionPath: String, orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([[String:Any]]?, Error?) -> ()) -> ListenerRegistration? {
        return persistencyManager.getDocumentsListener(collectionPath: collectionPath, orderField: orderField, descending: descending, limit: limit, blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
    
    func getNCDocumentsListener (collectionPath: String, orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([NCDocument]?, Error?) -> ()) -> ListenerRegistration? {
        return persistencyManager.getNCDocumentsListener(collectionPath: collectionPath, orderField: orderField, descending: descending, limit: limit, blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
    
    func getCollectionGroupDocumentsListener(collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([[String:Any]]?, Error?) -> ()) -> ListenerRegistration? {
        return persistencyManager.getCollectionGroupDocumentsListener(collectionGroupId: collectionGroupId, communityIds: communityIds, orderField: orderField, descending: descending, limit: limit, blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
    
    func getCollectionGroupNCDocumentsListener(collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, blockAfterGettingDocuments: @escaping ([NCDocument]?, Error?) -> ()) -> ListenerRegistration? {
        return persistencyManager.getCollectionGroupNCDocumentsListener(collectionGroupId: collectionGroupId, communityIds: communityIds, orderField: orderField, descending: descending, limit: limit, blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
    
    func getCollectionGroupCodableDocumentsListener <T: Codable> (collectionGroupId: String, communityIds: [String], orderField: String, descending: Bool, limit: Int, classType: T.Type, blockAfterGettingDocuments: @escaping ([T]?, Error?) -> ()) -> ListenerRegistration? {
        return persistencyManager.getCollectionGroupCodableDocumentsListener(collectionGroupId: collectionGroupId, communityIds: communityIds, orderField: orderField, descending: descending, limit: limit, classType: classType, blockAfterGettingDocuments: blockAfterGettingDocuments)
    }
    
    func updateDocument(documentDictionary: [String:Any], docRef: DocumentReference, blockAfterUpdating: ((Error?)->Void)? ) {
        persistencyManager.updateFireStoreDocument(documentDictionary: documentDictionary, docRef: docRef, blockAfterUpdating: blockAfterUpdating)
    }
    
    func deleteDocument(pathRef: String, block: ((Error?) -> ())? ) {
        persistencyManager.deleteFireStoreDocument(pathRef: pathRef, block: block)
    }
    
    // MARK: - StorageFiles
    
    func createAndSaveDocumentWithFileData(fileData: Data, metadataContentType: String, documentType: Int, fileName: String?, collectionRef: CollectionReference, communityId: String, blockAfterFileAndDocumentCreated: @escaping (DocumentReference?, StorageMetadata?, Error?) -> ()) {
        persistencyManager.createAndSaveDocumentWithFileData(fileData: fileData, metadataContentType: metadataContentType, documentType: documentType, fileName: fileName, collectionRef: collectionRef, communityId: communityId, blockAfterFileAndDocumentCreated: blockAfterFileAndDocumentCreated)
    }
    
    func deleteFirestoreDocumentWithStorageFile(docRef: DocumentReference, fileStorageReferencePath: String, block: @escaping (Error?) -> ()) {
        persistencyManager.deleteFirestoreDocumentWithStorageFile(docRef: docRef, fileStorageReferencePath: fileStorageReferencePath, block: block)
    }
    
    func getStorageFile(path: String, blockAfterGettingFile: @escaping (Data?, Error?) -> ()) {
       persistencyManager.getStorageFile(path: path, blockAfterGettingFile: blockAfterGettingFile)
    }
    
    func getFileName (metadataContentType: String?) -> String {
        return persistencyManager.getFileName(metadataContentType: metadataContentType)
    }
    
     // MARK: - Local Device flash memory
    
    func getFileDataFromLocalDevice (fileNameWithExtension: String,
    inStorageType storageType: StorageType) -> Data? {
        return persistencyManager.getFileDataFromLocalDevice(fileNameWithExtension: fileNameWithExtension, inStorageType: storageType)
    }
    
      // MARK: - Local Device RAM memory
    
    func getFromDocumentBuffer (key: String) -> Data? {
        return persistencyManager.getFromDocumentBuffer(key: key)
    }
    
  
    
    func addToLocalDocumentBuffer (key: String, data: Data?) {
        persistencyManager.addToLocalDocumentBuffer(key: key, data: data)
    }
    
      // MARK: - Get Download URL for a file in Storage
    
     func getDownloadUrlForFileReference (documentReference: DocumentReference, blockAfterGettingUrl: @escaping (URL?, Error?) -> ()) {
        persistencyManager.getDownloadUrlForFileReference(documentReference: documentReference, blockAfterGettingUrl: blockAfterGettingUrl)
    }
}
