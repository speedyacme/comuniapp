//
//  NCAppStartProcess.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 22/10/2018.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFunctions
import UIKit

enum startProcessError: Error {
    case nilUser
    case nilCommunities
    case outOfStock
}

class NCAppStartProcess: NSObject {
    
    class func startProcess () {
        guard let currentUser = Auth.auth().currentUser else { // No user is signed in, go to startVC to sign in
            print("[NC!!] Firebase user not signed in")
            DispatchQueue.main.async {
             NCGUI.showVC(viewControllerIdentifier: "startVC")
            }
            return
        }
        // First we get the community list of the current user, if he has no communities we offer him to input ownertag
        getUserCommunitiesIdList(blockToGetCommunityDocuments: { communitiesList in
            if communitiesList.count == 0 { // The user isn't created or it has 0 communities
                self.offerOwnerTagInput ()
            } else { // The user is created and has communities
                getCommunityDocuments (communitiesIdList: communitiesList, block: { communityDocuments in
                    subscribeTopicCommunitiesAndStart (communities: communityDocuments ,currentUser: currentUser)
                })
            }
        })
    }
    
    class func startTabVC () {
        NCGUI.showTabBarVC(tabBarId: "mainMixedTabBarController", tabToShowIndex: 0)
    }
    
    class func subscribeTopicCommunitiesAndStart (communities: [NCCommunity] , currentUser: User) {
        // Afterwards we save it to the Local Memory
        for community in communities {
            if let communityId = community.communityId {
                NCFirebaseCloudMessagingAPI.subscribeToTopic(communityId)
            }
        }
        NCModelAPI.sharedInstance.setUserCommunitiesInLocalMemory(communitiesArray: communities)
        guard let currentTopViewController = NCGUI.currentVC() else {return}
        guard let phone = currentUser.phoneNumber else {return}
        NCModelAPI.sharedInstance.logAccess(userPhoneNumber: phone)
        if !currentTopViewController.isKind(of: UITabBarController.self) {
            self.startTabVC()
        }
    }
    
    class func getUserCommunitiesIdList (blockToGetCommunityDocuments: @escaping ([String]) -> Void ) {
        
        let db:Firestore! = Firestore.firestore()
        guard let phoneNumber = NCModelAPI.sharedInstance.getMyPhoneNumber() else {
            print ("[NC!!] Error, there is no user Phone number")
            return
        }
        // First we get the user document from collection Users
        let docRef = db.document(constants.fireStore.users.collectionName+"/"+phoneNumber)
        docRef.getDocument(source:.default, completion: {  document, error in // We use source to default, so in case there is not network we get it from the cache.
            if let error = error {
                print("[NC!!] Error on getting user document: \(error.localizedDescription)")
                self.offerOwnerTagInput ()
            } else {
                if let document = document, document.exists {
                    let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                    print("[NC] startProcess finded user document: \(dataDescription)")
                    if let adminAccess = document.get(constants.fireStore.users.admin) as? Bool {
                        UserDefaults.standard.setValue(adminAccess, forKey: constants.defaults.admin)
                    }
                    // We take the opportunity to set ownerId in defaults
                    UserDefaults.standard.setValue(document.get(constants.fireStore.users.ownerId), forKey: constants.defaults.ownerId)
                    if let userCommunyIdArray = document.get(constants.fireStore.users.fieldNameCommunities) as? [DocumentReference] {
                        blockToGetCommunityDocuments( userCommunyIdArray.compactMap({$0.documentID}))
                    }
                } else {
                    print("[NC!!] User Document in \(constants.fireStore.users.collectionName+"/"+phoneNumber) does not exist")
                    self.offerOwnerTagInput ()
                }
            }
        })
    }
    
    class func getCommunityDocuments (communitiesIdList: [String], block: @escaping ([NCCommunity]) -> Void)  {
        
        var communitiesArray = [NCCommunity]()
        let db:Firestore! = Firestore.firestore()
        let communitiesRef = db.collection(constants.fireStore.communities.collectionName)
        print ("[NC] communitiesIdList: \(communitiesIdList)")
        communitiesRef.whereField(FieldPath.documentID(), in: communitiesIdList).getDocuments (source: .default, completion: { (querySnapshot, err) in  // We use source to default, so in case there is not network we get it from the cache.
            if let err = err {
                print("[NC!!] Error getting communities information: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print ("[NC] document.data(): \(document.data())")
                    if var newCommunity = NCCommunity(dictionary: document.data())  {
                        newCommunity.communityId = document.documentID
                        communitiesArray.append(newCommunity)
                    }
                }
                print ("[NC] User Communities Array: \(communitiesArray.description)")
                block(communitiesArray)
                }
            })
    }
    
    class func offerOwnerTagInput () {
        var inputTextField = UITextField()
        let passwordPrompt = UIAlertController(title: NSLocalizedString("Enter onwer code", comment: ""), message: NSLocalizedString("Enter your onwer code. If you don't have one, please ask your community administrator to provide it.", comment: ""), preferredStyle: UIAlertController.Style.alert);
        passwordPrompt.addTextField(configurationHandler: { (textField: UITextField!) in
            textField.placeholder = NSLocalizedString("Owner Tag", comment: "")
            textField.isSecureTextEntry = false       // true here for pswd entry
            inputTextField = textField
        })
        passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("Activate", comment: ""), style: UIAlertAction.Style.default, handler: { (action) -> Void in
            // Now do whatever you want with inputTextField (remember to unwrap the optional)
            if let ownerTag = inputTextField.text {
                // Llamamos a la cloud function
                createUserWithOwnerTag(ownerTag: ownerTag)
            }
        }))
        passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertAction.Style.default, handler: { (action) -> Void in
            print("Cancel pushed")
            offerOwnerTagInput()
        }));
        
        NCGUI.currentVC()!.present(passwordPrompt, animated: true, completion: nil)
    }
    
    class func createUserWithOwnerTag (ownerTag: String) {
        // Llamamos a la cloud function
        lazy var functions = Functions.functions()
        functions.httpsCallable("createUserFromOnwerTag").call(ownerTag){ result, error in
            if let error = error as NSError? {
                print("[NC!!] Error calling createUserFromOwnerTag Cloud function on firebase : \(error)")
                let errorAlert = UIAlertController(title: NSLocalizedString("Error creating user", comment: ""), message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert);
                errorAlert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { (action) -> Void in
                    // Now do whatever you want with inputTextField (remember to unwrap the optional)
                    offerOwnerTagInput()
                }))
                NCGUI.currentVC()!.present(errorAlert, animated: true, completion: nil)
            } else {
                if let data = result?.data as? String {
                    print("[NC] la función createOwnerTag ha funcionado correctamente y ha devuelto: \(String(describing: data)) ")
                    // Ahora pedimos cambiar el nombre
                    NCGUI.showVC(viewControllerIdentifier: "changeNameVC")
                }
            }
        }
    }
}

