//
//  NCCommunity.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 18/10/2018.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import FirebaseFirestore



struct NCCommunity {
    //var docRef: DocumentReference?
    var communityName: String
    var communityId: String?
    var landRegistryReference: String?
    var picture: UIImage?
    var location: CLLocation?
    var googleMapsId: String?
    var dictionary:[String:Any?] {
        return [
            constants.fireStore.communities.collectionName : communityName,
            constants.fireStore.communities.landRegistryReference : landRegistryReference,
            constants.fireStore.communities.picture : picture,
            constants.fireStore.communities.location : location,
            constants.fireStore.communities.googleMapsId : googleMapsId
        ]
    }
}



extension NCCommunity : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        guard let communityName = dictionary[constants.fireStore.communities.communityName] as? String
        else {
            return nil
        }
        let landRegistryReference = dictionary [constants.fireStore.communities.landRegistryReference] as? String
        let picture = dictionary [constants.fireStore.communities.picture] as? UIImage
        let location = dictionary [constants.fireStore.communities.location] as? CLLocation
        let googleMapsId = dictionary [constants.fireStore.communities.googleMapsId] as? String
        let communityId : String? = nil
        
        self.init(communityName: communityName, communityId: communityId, landRegistryReference: landRegistryReference, picture: picture, location: location, googleMapsId: googleMapsId)
    }
}
