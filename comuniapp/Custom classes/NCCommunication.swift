//
//  NCCommunication.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 16/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct NCCommunication {
    
    var docRef: DocumentReference?
    var communityId: String
    var shortDescription: String
    var longDescription: String
    var creatorUserId: String
    var creatorNickName: String
    var date: Timestamp
    var documents = [NCDocument]()
    var dictionary:[String:Any] {
        return [
            constants.fireStore.communications.shortDescription:shortDescription,
            constants.fireStore.communications.longDescription : longDescription,
            constants.fireStore.communications.creatorUserId : creatorUserId,
            constants.fireStore.communications.creatorNickname: creatorNickName,
            constants.fireStore.communications.date: date,
            constants.fireStore.communications.communityId : communityId
        ]
    }
}

extension NCCommunication : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        
        guard let shortDescription = dictionary[constants.fireStore.communications.shortDescription] as? String,
            let longDescription = dictionary[constants.fireStore.communications.longDescription] as? String,
            let creatorUserId = dictionary [constants.fireStore.communications.creatorUserId] as? String,
            let creatorNickname = dictionary [constants.fireStore.communications.creatorNickname] as? String,
            let date = dictionary [constants.fireStore.communications.date] as? Timestamp
            else {
                if let desc = dictionary[constants.fireStore.communications.shortDescription] as? String {
                    print ("[NC!!] Trying to create  NCCommunication \(desc) from Firebase dictionary failed due to null values")
                }
            return nil
            }
        let docRef = dictionary [constants.fireStore.generic.documentReference] as? DocumentReference // It can be nil when you create the communication
        let documents = [NCDocument]()
        let communityId = dictionary [constants.fireStore.communications.communityId] as? String ?? "undefined"
        self.init(docRef: docRef, communityId: communityId, shortDescription: shortDescription, longDescription: longDescription, creatorUserId: creatorUserId, creatorNickName: creatorNickname, date: date, documents: documents)
    }
}


