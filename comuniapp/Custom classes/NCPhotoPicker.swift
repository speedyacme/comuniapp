//
//  NCPhotoPicker.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 02/01/2019.
//  Copyright © 2019 Nutcoders. All rights reserved.
//
// Launches the picker, let's you choose between library and camera and returns the info of the image picked

import UIKit
import PhotosUI


class NCPhotoPicker: NSObject {
    
    
    let delegate : UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate
    
    init(delegate: UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate) {
        
        self.delegate = delegate
        
    }
    
    func launchImagePicker () {
        
        let picker = UIImagePickerController()
        picker.delegate = self.delegate
        self.checkPermission()
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("Choose Option", comment: ""), preferredStyle: .actionSheet)
        
        // 2
        let cameraRollAction = UIAlertAction(title: NSLocalizedString("Camera Roll", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] Camera Roll selected")
            
            picker.allowsEditing = true //2
            picker.sourceType = .photoLibrary //3
            self.delegate.present(picker, animated: true, completion: nil)
        })
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] Taken photo selected")
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                picker.allowsEditing = true
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.cameraCaptureMode = .photo
                self.delegate.present(picker, animated: true, completion: nil)
            } else {
                self.noCamera()
            }
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("[NC--] UIAlertAction Cancelled")
        })
        
        // 4
        optionMenu.addAction(cameraRollAction)
        optionMenu.addAction(takePhotoAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.delegate.present(optionMenu, animated: true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: NSLocalizedString("No camera", comment: ""),
            message: NSLocalizedString("Sorry, this device has no camera", comment: ""),
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: NSLocalizedString("OK", comment: ""),
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        self.delegate.present(alertVC,
                animated: true,
                completion: nil)
    }
    
    
    // MARK: Photopicker delegate methods
    /*
    func imagePickerController( _ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        self.delegate.NCPhotopicker(didFinishPickingMediaWithInfo: info)
        self.delegate.dismiss(animated: true, completion: nil) //5
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.delegate.dismiss(animated: true, completion: nil)
        self.delegate.NCPhotopickerDidCancel()
    }*/
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("[NC] Access to photo library is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("[NC] Photo library access status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("[NC] success getting authorization")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        default:
            print("[NC!!] Error when checking permission for the photo picker.")
        }
    }
}
