//
//  NCGUI.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 1/2/17.
//  Copyright © 2017 Nutcoders. All rights reserved.
//

import UIKit

class NCGUI: NSObject {
    
    class func currentVC () -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController // topController should now be your topmost view controller
        } else {
            return nil
        }
    }
    
    // Func to show a tab Bar with tabBarID the storyBoard Id of the tabBar and the tab that you want to show
    class func showTabBarVC (tabBarId: String, tabToShowIndex: Int) {
        
        // define view controller to present
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: tabBarId) as! UITabBarController // It is necessary to go to TabBar, otherwise it won't show the Tab Bar
        DispatchQueue.main.async {
            appDelegate.window?.rootViewController = tabBarController
            appDelegate.window?.makeKeyAndVisible()
            if (tabBarController.selectedIndex != tabToShowIndex) {tabBarController.selectedIndex = tabToShowIndex } // We move to the tabToShowIndex tab -> Be sure to send a number equal or less than the number of tabs-1 in TabBar. Tab 0 is the first tab.
        }
        
    }
    
    /*
    class func showAlertToShowObjectViewController (text: String , viewControllerIdentifier: String, objectPath: String) {
        // Show alert on current view and allow to see the invitation
        
        let alertController = UIAlertController(title: NSLocalizedString("Notification", comment: ""), message: text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: ""), style: .cancel, handler: .none))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Show", comment: ""), style: .default, handler: { (action) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: "initialTabBarID") as! UITabBarController // It is necessary to go to TabBar, otherwise it won't show the Tab Bar
            DispatchQueue.main.async {
                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                appDelegate.window?.rootViewController = tabBarController
                appDelegate.window?.makeKeyAndVisible()
                if (tabBarController.selectedIndex != 0) {tabBarController.selectedIndex = 0 } // We move to the first tab -> Communications Tab
                if let navigationVC = tabBarController.selectedViewController as? UINavigationController {
                    navigationVC.popToRootViewController(animated: true)
                    let communicationsDetailVC = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier) as! NCCommunicationDetailVC
                    // We update the  viewController model
                    NCModelAPI.sharedInstance.getDocument(pathRef: objectPath, block: { docSnapshot, err in
                        if err != nil {
                            print ("[NC!!] Error retrieving document in firestore")
                        } else {
                            communicationsDetailVC.communication = NCCommunication.init(dictionary: (docSnapshot?.data())!)
                            navigationVC.show(communicationsDetailVC, sender: tabBarController.selectedViewController!) // We show the Event
                        }
                        
                    })
                    
                }
            }
        }))

        // Now we present the alert on the viewController defined by the parameter
        if let current = self.currentVC() {
            DispatchQueue.main.async {
                current.present (alertController, animated: true, completion:nil)
            }
        }
    }*/
    
    class func showVC (viewControllerIdentifier: String) {
        
        // define view controller to present
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        DispatchQueue.main.async {
            appDelegate.window?.rootViewController = VC
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    
    
    class func showAlertOnViewController (_ viewController: UIViewController, title: String, message: String, cancelText: String?, continueText: String?) {
        // Show alert on current view and allow to see the invitation
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if cancelText != nil {
            alertController.addAction(UIAlertAction(title: cancelText!, style: .cancel, handler: .none))
        }
        
        if continueText != nil {
            alertController.addAction(UIAlertAction(title: continueText!, style: .default, handler: { (action) in
            }))
        }
        
        // Now we present the alert on the viewController defined by the parameter
        if (viewController.isViewLoaded) && (viewController.view.window != nil) {
            DispatchQueue.main.async {
                viewController.present (alertController, animated: true, completion:nil)
            }
        }
    }
    
   class func showErrorAlertOnCurrentVC (errorTitle: String, error: Error, vc: UIViewController) {
        // Show alert on current view and allow to see the invitation
        
        NCGUI.showAlertOnViewController(vc, title: NSLocalizedString(NSLocalizedString(errorTitle, comment: ""), comment: ""), message: error.localizedDescription, cancelText:  nil, continueText: NSLocalizedString("Accept", comment: ""))
    }
    
    class func showErrorAlertOnCurrentVC (errorTitle: String, message: String, vc: UIViewController) {
        // Show alert on current view and allow to see the invitation
      
        NCGUI.showAlertOnViewController(vc, title: NSLocalizedString(NSLocalizedString(errorTitle, comment: ""), comment: ""), message: NSLocalizedString(message, comment: ""), cancelText:  nil, continueText: NSLocalizedString("Accept", comment: ""))
    }
}
