//
//  NCDocumentType.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 01/06/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseStorage


struct NCDocumentType {
    
    var description : String
    var typeNumber : Int
}

extension NCDocumentType : DocumentSerializable {
init?(dictionary: [String : Any]) {
    guard let typeNumber = dictionary["type"] as? Int,
    let description = dictionary["description"] as? String else {return nil}
    
    self.init(description: description, typeNumber: typeNumber)
    }
}
