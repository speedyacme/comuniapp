//
//  NCMessage.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 18/07/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import Foundation
import MessageKit
import FirebaseFirestore
import FirebaseStorage
import UIKit


struct NCMessageUser : SenderType {
    var senderId: String
    var displayName: String
}

struct NCMessageMediaItem : MediaItem {
    // This variables allow to be compliant with MediaItem protocol
    /// The url where the media is located.
    var url: URL?
    /// The image.
    var image: UIImage?
    /// A placeholder image for when the image is obtained asychronously.
    var placeholderImage: UIImage = UIImage()
    /// The size of the media item.
    var size: CGSize = CGSize.init(width: 500, height: 500)
    
    init(image: UIImage?) {
        self.image = image
        if let image = image {
            self.size = image.size
        }
    }
}

struct NCMessage: MessageType {

    var docRef: DocumentReference?
    var creatorUserId: String
    var creatorNickname: String
    var messageText : String
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
    var messageMediaItem : NCMessageMediaItem?
    var messageFile : DocumentReference?  // Document reference of the file in issuexxx/messages/messageyyyy/documents
    var messageFileStoragePath: String?  // Path to the file in firebase Storage
    
    // I will use this init when creating a new message (I don't have docRef)
    init(userId: String, username: String, text: String, creationDate: Date) {
        creatorUserId = userId
        creatorNickname = username
        messageText = text
        sender = NCMessageUser.init(senderId: userId, displayName: username)
        messageId = UUID().uuidString
        sentDate = creationDate
        kind = MessageKind.text(text)
    }
    
    // I will use this init when creating a new multimedia message (I don't have docRef)
    init(userId: String, username: String, photoUrl: URL?, photoImage: UIImage?, fileReference: DocumentReference?) {
        self.creatorUserId = userId
        self.creatorNickname = username
        self.messageText = ""
        self.sender = NCMessageUser.init(senderId: userId, displayName: username)
        self.messageId = UUID().uuidString
        self.sentDate = Date()
        let mediaItemFromPhoto = NCMessageMediaItem.init(image: photoImage)
        self.messageMediaItem = mediaItemFromPhoto
        self.kind = MessageKind.photo(mediaItemFromPhoto)
        self.messageFile = fileReference
        
    }
    
    
    // I will use the following init when receiving a multimedia message doc from firestore converting from dictionary
    init(documentRef: DocumentReference, timeStamp: Timestamp, userId: String, username: String, text: String, fileReference: DocumentReference?,fileStoragePath: String?) {
        docRef = documentRef
        creatorUserId = userId
        creatorNickname = username
        sender = NCMessageUser.init(senderId: userId, displayName: username)
        messageId = documentRef.documentID
        messageText = text
        sentDate = timeStamp.dateValue()
        if fileReference != nil { // It's not a messageText
            messageFile = fileReference
            let mediaItemFromPhoto = NCMessageMediaItem.init(image: UIImage.init(named: "icons8-pdf-1"))
            self.messageMediaItem = mediaItemFromPhoto
            self.kind = MessageKind.photo(mediaItemFromPhoto)
            self.messageFileStoragePath = fileStoragePath
        } else {
            // It's a messageText
            kind = MessageKind.text(text)
        }
    }
    

    var dictionary:[String:Any] {
        return [
            constants.fireStore.messages.creatorUserId : creatorUserId,
            constants.fireStore.messages.creatorNickname: creatorNickname,
            constants.fireStore.messages.sentDate : Timestamp.init(date: sentDate), // In Firebase must be TimeStamp
            constants.fireStore.messages.mesageText : messageText,
            constants.fireStore.messages.messageFile : messageFile as Any
            //constants.fireStore.generic.documentReference : docRef ?? messageId
        ]
    }
    /*
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()

        guard let sentDate = data["created"] as? Date else {
        return nil
        }
        guard let senderID = data["senderID"] as? String else {
        return nil
        }
        guard let senderName = data["senderName"] as? String else {
        return nil
        }
          
        creatorNickname = senderName
        creatorUserId = senderID
        messageId = document.documentID
        
        self.sentDate = sentDate
        sender = NCMessageUser.init(senderId: senderID, displayName: senderName)
        // TODO: Revisar esto porque va a petar
        kind = MessageKind.text("")
        if let content = data["content"] as? String {
        messageText = content
        downloadURL = nil
        kind = MessageKind.text(content)
        } else if let urlString = data["url"] as? String, let url = URL(string: urlString) {
        downloadURL = url
        messageText = ""
        } else {
        return nil
        }
    }*/
}

// I will use the following init when receiving a message doc from firestore
extension NCMessage : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        
        // First we check the mandatory fields are not empty, if any of the mandatory fields in null we don't create the object
        guard
            let docRef = dictionary [constants.fireStore.generic.documentReference] as? DocumentReference,
            let creatorUserId = dictionary [constants.fireStore.messages.creatorUserId] as? String,
            let creatorNickname = dictionary [constants.fireStore.messages.creatorNickname] as? String,
            let sentTimeStamp = dictionary [constants.fireStore.messages.sentDate] as? Timestamp,
            let messageText = dictionary [constants.fireStore.messages.mesageText] as? String
            else {
                print ("[NC!!] Trying to create NCMessage from Firebase dictionary failed due to null values")
            return nil
        }
        
        let fileDocumentReference = dictionary [constants.fireStore.messages.messageFile] as? DocumentReference // If it is a text message it will be nil
        self.init(documentRef: docRef, timeStamp: sentTimeStamp, userId: creatorUserId, username: creatorNickname, text: messageText, fileReference: fileDocumentReference, fileStoragePath: nil)
    }
}

extension NCMessage: Comparable {
  
  static func == (lhs: NCMessage, rhs: NCMessage) -> Bool {
    return lhs.messageId == rhs.messageId
  }
  
  static func < (lhs: NCMessage, rhs: NCMessage) -> Bool {
    return lhs.sentDate < rhs.sentDate
  }
  
}


