//
//  NCNotificationProcessor.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 1/2/17.
//  Copyright © 2017 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging


// This module allows to interact with th
class NCFirebaseCloudMessagingAPI: NSObject {
    
    // To send messages we are using legacy http Server Protocol: https://firebase.google.com/docs/cloud-messaging/http-server-ref
    
    enum notificationsType: String, Decodable { //communication_update, issue_update, community_new_document, chat_message,chat_message_file
        case community_new_communication = "community_new_communication"
        case community_new_issue = "community_new_issue"
        case community_new_document = "community_new_document"
        case chat_message = "chat_message"
        case chat_message_file = "chat_message_file"
        //case communication_update = "communication_update"
        //case issue_update = "issue_update"
    }
    
    struct FCMNotification: Codable {
        let title: String
        let body: String
        //let body_loc_key: String  // La clave de la string del cuerpo en los recursos de string de la app que se usa para localizar el texto del cuerpo en la localización actual del usuario.Corresponde a loc-key en la carga útil de APNS. Para obtener más información, consulta la referencia de la clave de carga útil y cómo localizar el contenido de las notificaciones remotas. https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/PayloadKeyReference.html, https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/CreatingtheNotificationPayload.html#//apple_ref/doc/uid/TP40008194-CH10-SW9
        //let body_loc_args : [String]
    }
    
    struct FCMData: Codable {
        enum CodingKeys: String, CodingKey {
                case DocumentPath, From
                // Map the JSON key "url" to the Swift property name "htmlLink"
                case notificationType = "Type" // This is just for struct keys that we want to be different from JSON keys
            }
        let DocumentPath: String
        let From: String
        let notificationType: String
        
    }
    
    struct FCMMessage: Codable {
        let to: String
        let content_available: Bool
        let message_id: String
        let notification : FCMNotification
        let data : FCMData
    }
    
    struct FCMAps : Decodable {
        let alert : FCMNotification
    }
    
    struct FCMResponse: Decodable {
        enum CodingKeys: String, CodingKey {
                case DocumentPath, From, aps
                // Map the JSON key "url" to the Swift property name "htmlLink"
                case notificationType = "Type" // This is just for struct keys that we want to be different from JSON keys
            }
        let DocumentPath : String
        let From : String
        let notificationType: notificationsType //notificationsType
        let aps: FCMAps
        init(decoding userInfo: [AnyHashable : Any]) throws {
                let data = try JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted)
                self = try JSONDecoder().decode(FCMResponse.self, from: data)
            }
    }
    
    class func manageIncomingMessage (userInfo: [AnyHashable: Any], appState: UIApplication.State) {
        
        var isMyMessage = false
        let myPhone = NCModelAPI.sharedInstance.getMyPhoneNumber()
        
        //Check if the notification comes from us (it should be used +34 or other country notation before 9 digits)
        if let senderPhone = userInfo["From"] as? String, senderPhone.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range:nil) == myPhone {
            isMyMessage = true
        }  // we do this to remove "" from senderPhone which are needed for Jordi's cloud function
        
        if !isMyMessage { // if it is not my message, we show it
            
            guard let push = try? FCMResponse(decoding: userInfo)
                else {
                    print ("[NC!!] Notification Error: We are not able to decode userInfo from notification")
                    return
                }
            
            switch appState {
            case .active:
                let alertController = UIAlertController(title: push.aps.alert.title, message: push.aps.alert.body, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: .none))
                alertController.addAction(UIAlertAction(title: NSLocalizedString("View", comment: ""), style: .default, handler: { action in
                    showNotificationChangeScreen(push: push)
                })
                )
              // Now we present the alert on the viewController defined by the parameter
              if let currentVC = NCGUI.currentVC(), currentVC.isViewLoaded {
                  DispatchQueue.main.async {
                      currentVC.present (alertController, animated: true, completion:nil)
                  }
              } else {
                  print ("[NC!!] Error, current VC is nil")
              }
                
            case .background:
                showNotificationChangeScreen(push: push)
            case .inactive:
                showNotificationChangeScreen(push: push)
            default:
                showNotificationChangeScreen(push: push)
                    print ("[NC] App State: \(appState)")
            }
            
        } else {
            print ("[NC] Received a message sent by me, not doing anything: \(String(describing: self.obtainMessageTextFromUserInfo(userInfo: userInfo)))")
        }
        
    }

    
    class func obtainMessageTextFromUserInfo (userInfo: [AnyHashable: Any]) -> String? {
        
        
        var messageToReturn : String? = nil
        //Check if the notification comes from us
        
        
        if (userInfo["ObjectId"] != nil) { // The notification received is generated by Jordi's parse function , it is not a firebase test
            if let aps = userInfo ["aps"] as? NSDictionary {
                if let alert = aps ["alert"] as? NSDictionary {
                    if let localizedStringKey = alert ["loc-key"] as? String {
                        //print("NC lock-key of the notification\(localizedStringKey)")
                        if let localizedArguments = alert ["loc-args"] as? [String] {
                            //print("NC lock-arguments of the notification\(localizedArguments)")
                                // The following string will change for different messages, in this App all the messages have the same format, otherwise we will have to use a switch statement where we can handle correctly the different parameters
                                
                                let localizedString: String = NSLocalizedString(localizedStringKey, comment: "")
                                let messageToShowInAlert = String.localizedStringWithFormat(localizedString, localizedArguments[0], localizedArguments[1])
                                print ("\nNC Mensaje recibido: \(messageToShowInAlert)\n")
                                messageToReturn = messageToShowInAlert
                        }
                    }
                }
            }
        }
        
        return messageToReturn
    }
    
    
    class func subscribeToTopic (_ topic: String) {
        
        Messaging.messaging().subscribe (toTopic: topic)
        // TODO: AVOID REGISTERING IF ALREADY REGISTERED USING NSUSERDEFAULTS

    }
    
    class func unsubscribeFromTopic (_ topic: String) {
        
        Messaging.messaging().unsubscribe(fromTopic: topic)
    }
    
    
   /*
    // - MARK: Firebase Cloud Messaging http function to send a message to a topic through firebase (push notification)
    
    class func sendMessage (title: String, message: String, topic: String, type: notificationsType, firebaseReferencePath: String, senderPhoneNumber: String,  completion:((Error?) -> Void)?) { // The topic String is the object id of the firestore Document
        
        
        let messageNotification = FCMNotification.init(title: title, body: message)
        let notificationData = FCMData.init(DocumentPath: firebaseReferencePath, From: senderPhoneNumber, notificationType: type.rawValue)
        let newMessage = FCMMessage.init(to: "/topics/"+topic, content_available: true, message_id: UUID.init().uuidString, notification: messageNotification, data: notificationData)
        self.postJSON(post: newMessage, hostUrl: "https://fcm.googleapis.com/fcm/send", completion: nil)
        // We'll need a completion block that returns an error if we run into any problems
        
        
    }
    
    class func postJSON <T: Encodable> (post: T, hostUrl: String, completion:((Error?) -> Void)?) {
        let webAPIKey = "key=xxx" // We get this value from Firebase settings page
        let url = URL(string: hostUrl)!
        // Specify this request as being a POST method
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        // Make sure that we include headers specifying that our request's HTTP body
        request.setValue(webAPIKey, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Now let's encode out Post struct into JSON data...
        let encoder = JSONEncoder()
        
        if #available(iOS 13.0, *) {
            encoder.outputFormatting = .withoutEscapingSlashes
        }
        
        guard let jsonData = try? JSONEncoder().encode(post) else {
            return
        }
        print("[NC] Sending jsonData: ", String(data: jsonData, encoding: .utf8) ?? "no body data")
        for header in request.allHTTPHeaderFields! {
            print("[NC] Header field: ", header.key + ":" + header.value)
        }
        print ("[NC] Request description: \(request.description)")
        dump(request)
        
        // Create and run a URLSession data task with our JSON encoded POST request
        //let config = URLSessionConfiguration.default
        //let session = URLSession(configuration: config)
        let task = URLSession.shared.uploadTask(with: request, from: jsonData) { data, response, error in
            if let error = error {
                print("[NC!!] Error posting JSON, response: \(String(describing: error.localizedDescription))")
                return
            }
            guard let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                print ("server error")
                return
            }
            if let mimeType = response.mimeType,
                mimeType == "application/json",
                let data = data,
                let dataString = String(data: data, encoding: .utf8) {
                print ("got data: \(dataString)")
            } else {
                print("[NC!!] Error: no readable data received in response after posting JSON")
            }
        }
        task.resume()
    }*/
    
    class func showNotificationChangeScreen (push: FCMResponse) {
        
        // First we get the document from Firebase and once we have it, we can show it.
        var newDocDictionary : [String:Any] = [:]
        NCModelAPI.sharedInstance.getDocument(pathRef: push.DocumentPath, block: { doc, err in
            if let err = err {
                print ("[NC!!] Error getting document indicated by notification push: \(err)")
            } else {
                guard let doc = doc else {
                    print("[NC!!] Document data was empty.")
                    return
                }
                guard let DocDictionary = doc.data() as [String:Any]? else {
                    print("[NC!!] Document data was empty.")
                    return
                }
                newDocDictionary = DocDictionary
                guard let tabBarVC = NCGUI.currentVC() as? UITabBarController, tabBarVC.isViewLoaded
                    else {
                        print ("[NC!!] Error: Top View Controller is nil")
                        return
                    }
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                switch push.notificationType {
                    case .community_new_communication:
                        tabBarVC.selectedIndex = 0
                        guard let nav = tabBarVC.selectedViewController as? UINavigationController
                                else {
                                    print ("[NC!!] Error, current VC does not have NavigationController")
                                    return
                                }
                        let communicationsDetailVC = storyboard.instantiateViewController(withIdentifier: "NCCommunicationDetailVCID") as! NCCommunicationDetailVC
                        // We update the  viewController model
                        var newCommunication = NCCommunication.init(dictionary: newDocDictionary)
                        newCommunication?.docRef = doc.reference
                        guard newCommunication?.docRef != nil else {return}
                        communicationsDetailVC.communication = newCommunication
                        communicationsDetailVC.isEditing = false
                        nav.popToRootViewController(animated: true)
                        nav.pushViewController(communicationsDetailVC, animated: true)
                        print("[NC] Complete presenting communication view detail ")
                    
                    case .community_new_issue, .chat_message, .chat_message_file:
                        tabBarVC.selectedIndex = 1
                        guard let nav = tabBarVC.selectedViewController as? UINavigationController
                                else {
                                    print ("[NC!!] Error, current VC does not have NavigationController")
                                    return
                                }
                        let issuesDetailVC = storyboard.instantiateViewController(withIdentifier: "NCIssueDetailVCID") as! NCIssueDetailVC
                        // We update the  viewController model
                        var newIssue = NCIssue.init(dictionary: newDocDictionary)
                        newIssue?.docRef = doc.reference
                        guard newIssue?.docRef != nil else {return}
                        issuesDetailVC.issue = newIssue
                        issuesDetailVC.isEditing = false
                        nav.popToRootViewController(animated: true)
                        nav.pushViewController(issuesDetailVC, animated: true)
                        print("[NC] Complete presenting communication view detail ")
                        /*
                        case .communication_update:
                            tabBarVC.selectedIndex = 0
                            guard let nav = tabBarVC.selectedViewController as? UINavigationController
                                    else {
                                        print ("[NC!!] Error, current VC does not have NavigationController")
                                        return
                                    }
                            let communicationsDetailVC = storyboard.instantiateViewController(withIdentifier: "NCCommunicationDetailVCID") as! NCCommunicationDetailVC
                            // We update the  viewController model
                            communicationsDetailVC.communication = NCCommunication.init(dictionary: newDocDictionary)
                            nav.popToRootViewController(animated: true)
                            nav.pushViewController(communicationsDetailVC, animated: true)
                            print("[NC] Complete presenting communication view detail ")
                            
                        case .issue_update, .chat_message, .chat_message_file:
                            tabBarVC.selectedIndex = 1
                            guard let nav = tabBarVC.selectedViewController as? UINavigationController
                                    else {
                                        print ("[NC!!] Error, current VC does not have NavigationController")
                                        return
                                    }
                            let issuesDetailVC = storyboard.instantiateViewController(withIdentifier: "NCIssueDetailVCID") as! NCIssueDetailVC
                            // We update the  viewController model
                            issuesDetailVC.issue = NCIssue.init(dictionary: newDocDictionary)
                            nav.popToRootViewController(animated: true)
                            nav.pushViewController(issuesDetailVC, animated: true)
                            print("[NC] Complete presenting communication view detail ")
                         */
                    case .community_new_document:
                            tabBarVC.selectedIndex = 3
                }
            }
        })
    }
    
}


