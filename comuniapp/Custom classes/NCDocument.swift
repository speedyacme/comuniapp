//
//  NCDocument.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 20/8/18.
//  Copyright © 2018 Nutcoders. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

// [START codable_struct]
public struct NCDocument: Codable {
    
    @DocumentID var id: String?
    let docName: String
    let description: String?
    let type : Int
    let mimeType : String
    let creatorUserId : String
    let creatorNickname : String
    let uploadDate: Timestamp
    let fileStorageReferencePath: String
    var communityId: String
    var fileData : Data?
    
    // Esto creo que sólo es necesario si los nombres no coinciden...
    enum CodingKeys: String, CodingKey {
        case id
        case docName // Si no fuera el mismo nombre que la etiqueta del diccionario pondríamos = "docname"
        case description
        case type
        case mimeType
        case creatorUserId
        case creatorNickname
        case uploadDate
        case fileStorageReferencePath
        case communityId
    }

}

/*
 CÓDIGO ANTIGUO
 
struct NCDocument {
    
    var docRef: DocumentReference? // It has to be optional because when we create a new object, we don't know yet the reference. And is interesting to be able to create it locally when we don't have network access.
    var docRefIntern : String
    var docName : String
    var description: String = ""
    var documentType: Int
    var mimeType: String
    var fileStorageReferencePath : String
    var creatorUserID: String
    var creatorNickname : String
    var uploadDate: Timestamp
    var fileData : Data? = nil
    var communityId : String
    
    
    var dictionary:[String:Any] {
        return [
            constants.fireStore.documents.documentName: docName,
            constants.fireStore.documents.documentDescription :description,
            constants.fireStore.documents.documentType: documentType,
            constants.fireStore.documents.mimeType : mimeType,
            constants.fireStore.documents.fileStorageReferencePath: fileStorageReferencePath,
            constants.fireStore.documents.creatorUserId : creatorUserID,
            constants.fireStore.documents.creatorNickname: creatorNickname,
            constants.fireStore.documents.uploadDate : uploadDate,
            constants.fireStore.documents.documentRefIntern : docRefIntern,
            constants.fireStore.documents.communityId : communityId
        ]
    }
}

extension NCDocument : DocumentSerializable {
    init?(dictionary: [String : Any]) {
        let givenDocRefIntern = dictionary [constants.fireStore.documents.documentRefIntern] as? String ?? UUID().uuidString
        guard let docName = dictionary [constants.fireStore.documents.documentName] as? String,
        let description = dictionary[constants.fireStore.documents.documentDescription] as? String,
        let creatorNickname = dictionary [constants.fireStore.documents.creatorNickname] as? String,
        let fileStorageReferencePath = dictionary [constants.fireStore.documents.fileStorageReferencePath] as? String,
        let creatorUserID = dictionary [constants.fireStore.documents.creatorUserId] as? String,
        let documentTypeNumber = dictionary[constants.fireStore.documents.documentType] as? Int,
        let mimeTypeString = dictionary[constants.fireStore.documents.mimeType] as? String,
        let uploadDate = dictionary [constants.fireStore.documents.uploadDate] as? Timestamp,
        let docRef = dictionary [constants.fireStore.generic.documentReference] as? DocumentReference,
        let communityId = dictionary [constants.fireStore.documents.communityId] as? String
        else {
            print ("[NC!!] Trying to create  NCDocument from Firebase dictionary failed due to null values")
            return nil
        }
        self.init(docRef: docRef, docRefIntern: givenDocRefIntern, docName: docName, description: description, documentType: documentTypeNumber, mimeType: mimeTypeString, fileStorageReferencePath: fileStorageReferencePath, creatorUserID: creatorUserID, creatorNickname: creatorNickname, uploadDate: uploadDate, fileData: nil, communityId: communityId)
    }
}
*/




/*
// [START codable_struct]
public struct City: Codable {

    let name: String
    let state: String?
    let country: String?
    let isCapital: Bool?
    let population: Int64?

    enum CodingKeys: String, CodingKey {
        case name
        case state
        case country
        case isCapital = "capital"
        case population
    }

}
// [END codable_struct]
 
// The way to create the document is like this:
 do {
     try db.collection("cities").document("LA").setData(from: city)
 } catch let error {
     print("Error writing city to Firestore: \(error)")
 }
 ViewController.swift
 
 // Now we see how to convert firebase data to the custom struct
 
 let docRef = db.collection("cities").document("BJ")

 docRef.getDocument { (document, error) in
     // Construct a Result type to encapsulate deserialization errors or
     // successful deserialization. Note that if there is no error thrown
     // the value may still be `nil`, indicating a successful deserialization
     // of a value that does not exist.
     //
     // There are thus three cases to handle, which Swift lets us describe
     // nicely with built-in sum types:
     //
     //      Result
     //        /\
     //   Error  Optional<City>
     //               /\
     //            Nil  City
     let result = Result {
       try document?.data(as: City.self)
     }
     switch result {
     case .success(let city):
         if let city = city {
             // A `City` value was successfully initialized from the DocumentSnapshot.
             print("City: \(city)")
         } else {
             // A nil value was successfully initialized from the DocumentSnapshot,
             // or the DocumentSnapshot was nil.
             print("Document does not exist")
         }
     case .failure(let error):
         // A `City` value could not be initialized from the DocumentSnapshot.
         print("Error decoding city: \(error)")
     }
 }

*/


