//
//  NCGenericFunctions.swift
//  wwoww
//
//  Created by Tomás Brezmes Llecha on 10/1/17.
//  Copyright © 2017 Nutcoders. All rights reserved.
//

import Foundation
import UIKit

class NCGeneric: NSObject {
    
    class func openURLinExternalBrowser (urlAddress: String) {
        if let url = URL(string: urlAddress) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: {
                                            (success) in
                                            print("Open \(urlAddress): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(urlAddress): \(success)")
            }
        }
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
