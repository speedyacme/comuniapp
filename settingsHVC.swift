//
//  settingsHVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 13/9/22.
//  Copyright © 2022 Nutcoders. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

//Create a UIHostingController class that hosts your SwiftUI view
@available(iOS 13.0, *)
class SettingsHVC: UIHostingController<SettingsSwiftUIView> {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder, rootView: SettingsSwiftUIView())
    }
}

