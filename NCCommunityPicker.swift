//
//  NCCommunityPicker.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 12/4/21.
//  Copyright © 2021 Nutcoders. All rights reserved.
//

import Foundation
import UIKit


class NCCommunityPicker: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    // MARK: Picker View Delegate functions
    
    private var userCommunityArray : [NCCommunity] = []
    private var allCommunities : Bool = false


    init(frame: CGRect, userCommunityArray: [NCCommunity], withAllCommunities: Bool) {
        super.init(frame: frame)
        self.userCommunityArray = userCommunityArray
        self.allCommunities = withAllCommunities
        self.delegate = self
        self.dataSource = self
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let rows : Int
        switch allCommunities {
        case true: // Filtering communicties, it has "all communities" option plus each individual community
            rows = userCommunityArray.count + 1
        case false: // Selection of community for creating a new communication, it doesn't have "all communities" option
            rows = userCommunityArray.count
        }
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let title : String?
        
        switch allCommunities {
        case true: // Filtering communities, it has "all communities" option plus each individual community
            if row == userCommunityArray.count {
                title = NSLocalizedString("All communities", comment: "")
            } else {
                title = userCommunityArray[row].communityName
            }
        case false: // Selection of community for creating a new communication, it doesn't have "all communities" option
            title = userCommunityArray[row].communityName
        }
        return title
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print ("[NC] Row number \(row) selected")
        /*switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            self.pickerViewRolled = true
            if row == self.userCommunities.count {
                self.userCommunitiesIdsFiltered = self.userCommunitiesIds
            } else {
                self.userCommunitiesIdsFiltered = [userCommunities[row].communityId!]
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            self.newCommunicationCommunityId = userCommunities[row].communityId
            self.newCommunicationCommunityName = userCommunities[row].communityName
        default:
            return
        }*/
    }
}
