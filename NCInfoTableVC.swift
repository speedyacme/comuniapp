//
//  NCInfoTableVC.swift
//  comuniapp
//
//  Created by Tomás Brezmes Llecha on 09/09/2020.
//  Copyright © 2020 Nutcoders. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore


class NCInfoTableVC: UITableViewController {
    
    private var queryListeners = [ListenerRegistration]()
    let pullToRefreshControl = UIRefreshControl()
    var filterButton = UIBarButtonItem ()
    var db:Firestore! = Firestore.firestore()
    var userCommunities = [NCCommunity]()
    var userCommunitiesIds = [String]()
    var userCommunitiesIdsFiltered = [String]() {
        didSet { // We change the name of the navigation top bar based on community filtered
            if userCommunitiesIdsFiltered.count == 1{
                if let userCommunitySelected =  self.userCommunities.first(where: { $0.communityId == self.userCommunitiesIdsFiltered.first!})  {
                    self.navigationItem.prompt = userCommunitySelected.communityName
                    let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
                }
            } else {
                self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
                let navigationTitleFont = UIFont(name: "Lato", size: 20)!
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
            }
        }
    }
    var pickerViewRolled = false
    var infosByCommunity = [NCInfo]() {
        didSet
        {
            self.updateUI()
        }
    }
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    
    var newInfoCommunityId : String?
    var newInfoCommunityName : String?
    var admin = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Info", comment: "")
        super.viewDidLoad()
        self.userCommunities = NCModelAPI.sharedInstance.getUserCommunitiesFromLocalMemory()
        self.userCommunitiesIds = self.userCommunities.compactMap ({$0.communityId})
        self.userCommunitiesIdsFiltered = self.userCommunitiesIds
        let navigationTitleFont = UIFont(name: "Lato", size: 20)!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont]
        self.navigationItem.prompt = NSLocalizedString("All communities", comment: "")
        // Add Refresh Control to Table View
        pullToRefreshControl.tintColor = UIColor(named: "MainAppBlueColor")
        pullToRefreshControl.attributedTitle = NSAttributedString (string:NSLocalizedString("Loading info...", comment: ""))
        if #available(iOS 10.0, *) {
            tableView.refreshControl = pullToRefreshControl
        } else {
            tableView.addSubview(pullToRefreshControl)
        }
        // Configure Refresh Control
        pullToRefreshControl.addTarget(self, action: #selector(updateModel), for: .valueChanged)
        // end configuring refresh control
        self.tableView.isEditing = false // We only let edit it if the use is admin
        NCModelAPI.sharedInstance.isUserAdmin(blockAfterAdminInfo: { isAdmin, error in
            if let error = error {
               print("[NC] Couldn't get user Admin info \(error.localizedDescription)")
            } else {
                if isAdmin {
                    self.admin = true
                    let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPushed))
                    self.navigationItem.rightBarButtonItem = addButton
                }
            }
        })
        self.filterButton = UIBarButtonItem.init(image: UIImage(named: "icons8-filter"), landscapeImagePhone: UIImage(named: "icons8-filter"), style: .plain, target: self, action: #selector(self.filterButtonPushed))
        if self.userCommunities.count > 1 { self.navigationItem.leftBarButtonItem = filterButton } // If we have only one community, we don't need to show the filter button
        self.updateModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func stopObserving () {
        for listener in self.queryListeners {
            listener.remove()
        }
        self.queryListeners.removeAll() // We reset the queryListeners
    }
    
    func updateUI () {
        if (self.userCommunitiesIds.count != self.userCommunitiesIdsFiltered.count) && self.userCommunitiesIds.count > 1 {
            self.filterButton.image = UIImage (named: "icons8-filter_filled")
        } else {
            self.filterButton.image = UIImage (named: "icons8-filter")
        }
        self.tableView.reloadData()
    }
    
    func runActivityIndicator () {
        // See if it is animating.
        if activityIndicatorView.isAnimating {
            // If currently animating, stop it.
            activityIndicatorView.stopAnimating()
        }
        else {
            // Begin.
            activityIndicatorView.startAnimating()
        }
    }
    
    func stopActivityIndicator () {
        activityIndicatorView.stopAnimating()
    }
    
    @objc func addButtonPushed () {
        
        self.launchCommunityPickerforNewItem()
    }
    
    func launchCommunityPickerforNewItem () {
        print ("[NC--]******* Creating new info ********")
        print ("[NC--] ******************************")
        
        
        switch userCommunities.count {
        
            case let numberOfUserCommunities where numberOfUserCommunities == 1: // we don't need to show community picker
                newInfoCommunityId = userCommunities.first!.communityId
                newInfoCommunityName = userCommunities.first!.communityName
                guard let id = newInfoCommunityId else {
                    print ("[NC!!] Error creating communication, community id is nil")
                    return
                }
                self.performSegue(withIdentifier: "segueToInfoCreation", sender: id)
                
            
            case let numberOfUserCommunities where numberOfUserCommunities > 1 :  // we need to show communitiy picker
                
                let message = "\n\n\n\n\n\n"
                let alert = UIAlertController(title: "Please Select Community for publishing", message: message, preferredStyle: UIAlertController.Style.actionSheet)
                alert.isModalInPopover = true
                let screenWidth = self.view.frame.size.width
                let pickerFrame = NCCommunityPicker.init(frame: CGRect(x: 5, y: 20, width: screenWidth - 20, height: 140), userCommunityArray: self.userCommunities, withAllCommunities: false)
                         // CGRectMake(left, top, width, height) - left and top are like margins
                    //pickerFrame.tag = 1 // this will tell us if that this specific picker is to select one community to make a new info
                    //set the pickers datasource and delegate
                    //pickerFrame.delegate = self
                    
                //Add the picker to the alert controller
                alert.view.addSubview(pickerFrame)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    self.newInfoCommunityName = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityName
                    self.newInfoCommunityId = self.userCommunities[pickerFrame.selectedRow(inComponent: 0)].communityId
                    guard let id = self.newInfoCommunityId else {
                        print ("[NC!!] Error creating info, community id is nil")
                        return
                    }
                    self.newInfoCommunityId = id
                    self.performSegue(withIdentifier: "segueToInfoCreation", sender: id)

                })
                alert.addAction(okAction)
                let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                alert.addAction(cancelAction)
                self.parent!.present(alert, animated: true, completion: nil)
            
            default:
                NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities assigned", comment: ""), message: NSLocalizedString("You must be registered at least in one community", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.userCommunitiesIdsFiltered.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infosByCommunity.filter({$0.communityId == self.userCommunitiesIdsFiltered[section]}).count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "reusedInfoCellIdentifier", for: indexPath) as! NCInfoTableViewCell
        let info = self.infosByCommunity.filter({$0.communityId == self.userCommunitiesIdsFiltered[indexPath.section]})[indexPath.row]
        cell.descriptionLabel.text = info.description
        cell.phoneLabel.text = info.phone
        cell.callButton.buttonIndexPath = indexPath
        cell.callButton.addTarget(self, action: #selector(callButtonPushed(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NCModelAPI.sharedInstance.getCommunityNameForCommunityId(communityId: self.userCommunitiesIdsFiltered[section])
    }
    
    /*
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let phoneNumber = self.infosByCommunity[self.userCommunitiesIdsFiltered[indexPath.section]]?[indexPath.row].phone ?? ""
        callNumber(phoneNumber: phoneNumber)
    }*/
    
    // Override to support conditional editing of the table view.
   // override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
      //  return self.tableView.isEditing
    //}
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if self.admin && editingStyle == UITableViewCell.EditingStyle.delete {
            let infoToDelete = self.infosByCommunity.filter({$0.communityId == self.userCommunitiesIdsFiltered[indexPath.section]})[indexPath.row]
            guard let infoId = infoToDelete.id else {return}
            let path = db.collection(constants.fireStore.info.collectionName).document(infoId).path
            NCModelAPI.sharedInstance.deleteDocument(pathRef: path , block: { error in
                if error != nil {
                    print ("[NC!!] Error deleting info: \(error!.localizedDescription)")
                } else {
                    print ("[NC] Info deleted correctly)")
                    self.infosByCommunity.removeAll(where: {$0.id == infoToDelete.id})
                }
            })
        }
    }
    
    
    // MARK: - Model
    
    @objc func updateModel () {
        if self.userCommunitiesIdsFiltered.count != 0 {
            // Find infos in each community
            self.stopObserving()
            let newListener = NCModelAPI.sharedInstance.getCollectionGroupCodableDocumentsListener(collectionGroupId: "informations", communityIds: self.userCommunitiesIdsFiltered, orderField: constants.fireStore.issues.openingDate, descending: false, limit: 25, classType: NCInfo.self, blockAfterGettingDocuments: { informations, err in

                        if let err = err {
                            print("[NC!!] Error getting infos: \(err.localizedDescription)")
                            self.pullToRefreshControl.endRefreshing()
                            if let indicator = self.activityIndicatorView {
                                indicator.stopAnimating()
                            }
                            NCGUI.showErrorAlertOnCurrentVC(errorTitle: NSLocalizedString("Error downloading infos", comment: ""), error: err, vc: self)
                        } else {
                            self.pullToRefreshControl.endRefreshing()
                            if let indicator = self.activityIndicatorView {
                                indicator.stopAnimating()
                            }
                            guard let informations = informations
                                else { return }
                            self.infosByCommunity = informations
                        }
            })
            if newListener != nil {
                self.queryListeners.append(newListener!)
            }
                            
        } else {
            // User doesn't have any community set -> he must talk to administrator
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("No communities found", comment: ""), message: NSLocalizedString("Contact the community administrator to include you in the community", comment:""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
        }
    }
    
    
    @objc func filterButtonPushed () {
        if self.userCommunitiesIds.count > 1 { // show picker
            self.pickerViewRolled = false
            let message = "\n\n\n\n\n\n"
            let alert = UIAlertController(title: "Please Select Community for filter", message: message, preferredStyle: UIAlertController.Style.actionSheet)
            alert.isModalInPopover = true
            let screenWidth = self.view.frame.size.width
            let pickerFrame = NCCommunityPicker.init(frame: CGRect(x: 5, y: 30, width: screenWidth - 20, height: 175), userCommunityArray: self.userCommunities, withAllCommunities: true) // CGRectMake(left, top, width, height) - left and top are like margins
            // true will tell us if that this specific picker is to filter one specific community or select "all communities" option
            //set the pickers datasource and delegate
            
            
            //Add the picker to the alert controller
            alert.view.addSubview(pickerFrame)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                let selectedRowIndex = pickerFrame.selectedRow(inComponent: 0)
                if selectedRowIndex == self.userCommunitiesIds.count { // It has been selected "All communities"
                    self.userCommunitiesIdsFiltered = self.userCommunitiesIds
                } else {
                    self.userCommunitiesIdsFiltered = [self.userCommunitiesIds[selectedRowIndex]]
                }
                self.updateModel()
            })
            alert.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(cancelAction)
            self.parent!.present(alert, animated: true, completion: nil)
            
            if self.userCommunitiesIdsFiltered.count == 1 { // We have selected already a community, we show the current community
                if let indexCommunity = self.userCommunitiesIds.firstIndex(of: self.userCommunitiesIdsFiltered[0]) {
                pickerFrame.selectRow(indexCommunity, inComponent: 0, animated: true)
                }
            } else {
                pickerFrame.selectRow(self.userCommunitiesIds.count, inComponent: 0, animated: true)
            }
        } else {
            NCGUI.showAlertOnViewController(self, title: NSLocalizedString("Only one community assigned", comment: ""), message: NSLocalizedString("To filter communities you must be registered at least in two communities", comment: ""), cancelText: NSLocalizedString("Cancel", comment: ""), continueText: nil)
            return
        }
    }
    
    /*
    // MARK: Picker View Delegate functions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let rows : Int
        
        switch pickerView.tag {
        case 0: // Filtering communicties, it has "all communities" option plus each individual community
            rows = self.userCommunitiesIds.count + 1
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            rows = userCommunities.count
        default:
            rows = 0
        }
        return rows
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let title : String?
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            if row == self.userCommunities.count {
                title = NSLocalizedString("All communities", comment: "")
            } else {
                title = self.userCommunities[row].communityName
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            title = userCommunities[row].communityName
        default:
            title = nil
        }
        return title
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0: // Filtering communities, it has "all communities" option plus each individual community
            self.pickerViewRolled = true
            if row == self.userCommunities.count {
                self.userCommunitiesIdsFiltered = self.userCommunitiesIds
            } else {
                self.userCommunitiesIdsFiltered = [userCommunities[row].communityId!]
            }
        case 1: // Selection of community for creating a new communication, it doesn't have "all communities" option
            self.newInfoCommunityId = userCommunities[row].communityId
            self.newInfoCommunityName = userCommunities[row].communityName
        default:
            return
        }
    }*/
    
    @IBAction func callButtonPushed(_ sender: NCUIButtonWithIndexPath) {
        guard let indexP = sender.buttonIndexPath
        else { return }
        let info = self.infosByCommunity.filter({$0.communityId == self.userCommunitiesIdsFiltered[indexP.section]})[indexP.row]
        self.callNumber(phoneNumber: info.phone)
    }
    
   
    private func callNumber(phoneNumber: String) {
        
        guard let url = URL(string: "tel://\(phoneNumber)"),
            UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    
    // MARK: - Navigation
 
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if let identifier = segue.identifier
         {
             switch identifier
             {
             case "segueToInfoCreation":
                let destinationVC = segue.destination as! NCInfoCreationVC
                destinationVC.userCommunityId = sender as? String
             default: return
             }
         }
     }
   

}
